﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Order last in execution order to give accurate end of frame animations
public class Scr_Character_AnimatorVariableController : MonoBehaviour
{

    Scr_Character_Components character;
    Animator animator;

    public float RunAnimationModifier;

    private void Awake()
    {
        character = this.GetComponent<Scr_Character_Components>();
        animator = character.animator;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        UpdateMovementVariables();

    }

    void UpdateMovementVariables()
    {
        #region Run Speed
        //Vector3 FlattenedCharacterMoveSpeed = character.character_Movement.CurrentMoveSpeed;
        float RunSpeed = character.character_Movement.CurrentMoveSpeed.magnitude * RunAnimationModifier;
        if (!character.character_Movement.MovingForward)
        {
            RunSpeed *= -(1 + (1*character.character_Movement.BackwardMovementMultiplier));
        }
        animator.SetFloat("RunSpeed", RunSpeed);

        animator.SetFloat("RunSpeedPercent", RunSpeed / character.character_Movement.BaseRunSpeed);

        //Set the grounded state
        animator.SetBool("Grounded", character.character_Movement.Grounded);

        animator.SetFloat("VerticalSpeed", character.character_Movement.CurrentYVelocity);
        #endregion
    }

    public void TriggerThrow()
    {
        animator.SetTrigger("ThrowTrigger");
        SetCharging(false);   
    }

    public void TriggerJump()
    {
        animator.SetTrigger("Jump");
    }

    public void SetCharging(bool charging)
    {
        animator.SetBool("ThrowCharging", charging);
    }
}
