﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_TestMove : MonoBehaviour
{
    Vector3 InputVector;
	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        //Determine the input vector from unity input
        InputVector = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

        transform.position += InputVector * Time.deltaTime;
    }
}
