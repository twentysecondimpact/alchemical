﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_Checkpoint : MonoBehaviour
{
    public bool Triggered;
    public string CheckpointLevel;
    public int ObjectiveAtLevelStart;
    public int CrystalsAtLevelStart;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (!Triggered)
        {
            Scr_GameDirector.inst.CurrentCheckpointLevel = CheckpointLevel;
            //change the position of the level spaw
            Scr_GameDirector.inst.currentLevel.PlayerSpawn.transform.position = this.transform.parent.position;
            Scr_GameDirector.inst.currentLevel.PlayerSpawn.transform.rotation = this.transform.parent.rotation;
            Scr_GameDirector.inst.currentLevel.CheckPointObjective = ObjectiveAtLevelStart;
            Scr_GameDirector.inst.currentLevel.CheckPointCrystals = CrystalsAtLevelStart;
            Triggered = true;
        }
        
    }
}
