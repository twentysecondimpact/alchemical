﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_Character_IKController : MonoBehaviour
{

    Scr_Character_Components character;

    public Transform head;

    public Quaternion startingHeadRotation;

	// Use this for initialization
	void Start ()
    {
        character = Scr_Character_Components.inst;
        startingHeadRotation = head.rotation;
	}
	
	// Update is called once per frame
	void Update ()
    {
        //UpdateHeadDirection();
	}

    private void LateUpdate()
    {
        UpdateHeadDirection();
    }

    void UpdateHeadDirection()
    {
        if (character.character_Throw.PlayerThrowState == Scr_Character_Throw.ThrowState.Charging && character.character_Throw.FlaskIndex != 3)
        {
            Vector3 DeltaVector = head.position - (character.character_Throw.AimReticle.transform.position + new Vector3(0,1,0));
            Quaternion grabbedRotation = head.rotation;
            head.LookAt(this.transform.position - DeltaVector);
            head.rotation *= grabbedRotation;
            head.rotation *= Quaternion.Euler(new Vector3(character.character_Movement.CharacterModelTransformKeyboard.rotation.eulerAngles.y,0,0));
            //head.rotation *= Quaternion.Euler(new Vector3(character.character_Movement.CharacterModelTransformMouse.rotation.eulerAngles.y, 0, 0));
        }
    }
}
