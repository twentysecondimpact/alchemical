﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_Character_Components : MonoBehaviour
{
    public static Scr_Character_Components inst;

    public Scr_Character_Aggro character_Aggro;
    public Scr_Character_Movement character_Movement;
    public Scr_Character_Throw character_Throw;
    public Scr_Character_Health character_Health;
    public Scr_Character_AnimatorVariableController character_AnimVariables;
    public Scr_AnimEvents_Character character_AnimEvents;
    public Scr_Character_IKController character_IKController;

    public Animator animator;

    private void Awake()
    {
        inst = this;

        character_Aggro = GetComponent<Scr_Character_Aggro>();
        character_Movement = GetComponent<Scr_Character_Movement>();
        character_Throw = GetComponent<Scr_Character_Throw>();
        character_Health = GetComponent<Scr_Character_Health>();
        character_AnimVariables = GetComponent<Scr_Character_AnimatorVariableController>();
        //character_AnimEvents = GetComponent<Scr_AnimEvents_Character>();
        character_IKController = GetComponent<Scr_Character_IKController>();
    }

    // Use this for initialization
    void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ResetCharacter()
    {
        character_Throw.RestartCharacterFlasks();
        character_Health.RestartCharacterHealth();
        character_Movement.ResetCharacterMovement();
    }
}
