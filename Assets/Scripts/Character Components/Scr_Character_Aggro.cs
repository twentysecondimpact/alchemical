﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Scr_Character_Aggro : MonoBehaviour
{

    //public float AggroPulseRadius;
    public float AggroPulseTime;
    public float AggroPulseTimer;

    public GameObject AggroRangeProjectorPrefab;
    public GameObject AggroRangeProjector;

    public float AggroRadius;
    //Multiplies the actualy redius by the triggering aggro
    public float AggroLeeway;

    public TooltipObject AggroTooltip;

    // Use this for initialization
    void Start ()
    {
        //Spawn the range projector
        AggroRangeProjector = GameObject.Instantiate(AggroRangeProjectorPrefab, transform.position, transform.rotation);
        AggroRangeProjector.transform.parent = this.transform;
    }
	
	// Update is called once per frame
	void Update ()
    {
        UpdateAggroPulseTimer();


        AggroRangeProjector.transform.GetChild(0).GetComponent<Projector>().orthographicSize = AggroRadius;// + 0.1f;

        if (Input.GetKey(KeyCode.LeftShift))
        {
            AggroRangeProjector.SetActive(true);
        }
        else
        {
            AggroRangeProjector.SetActive(false);
        }
    }

    void UpdateAggroPulseTimer()
    {

        if (AggroPulseTimer <= 0)
        {
            AggroPulse();
        }
        else
        {
            AggroPulseTimer -= Time.deltaTime;
        }
    }

    public void AggroPulse()
    {
        //Reset the aggro pulse timer
        AggroPulseTimer = AggroPulseTime;

        foreach (GameObject enemy in GameObject.FindGameObjectsWithTag("Enemy"))
        {

            Scr_Enemy_Movement em = enemy.GetComponent<Scr_Enemy_Movement>();

            if(!enemy.GetComponent<Scr_Enemy_Attack_Contact>().Ranged)
            {

            }
            //Only attempt to aggro somethign that isnt already aggroed
            if (em.aggroState != Scr_Enemy_Movement.AggroState.Aggroed)
            {

                Vector3 YCanceledPlayer = this.transform.position;
                YCanceledPlayer.y = 0;
                Vector3 YCanceledEnemy = enemy.transform.position;
                YCanceledEnemy.y = 0;

                if (Vector3.Distance(YCanceledEnemy, YCanceledPlayer) < ((em.AggroPulseRadius * AggroLeeway) + (AggroRadius * AggroLeeway)))
                {
                    //if its ranged aggro straight away
                    if (em.GetComponent<Scr_Enemy_Attack_Contact>().Ranged)
                    {
                        enemy.GetComponent<Scr_Enemy_Movement>().SetStateAggro(Scr_GameDirector.inst.AggroDelay, Scr_GameDirector.inst.PassiveDelay, false);
                        AggroTooltip.Activate();
                    }
                    //if it is meele find a path first
                    else if(Scr_GameDirector.CharacterAttackable(em.Agent, Scr_Character_Components.inst.character_Movement.LastNavPosition))
                    {

                        enemy.GetComponent<Scr_Enemy_Movement>().SetStateAggro(Scr_GameDirector.inst.AggroDelay, Scr_GameDirector.inst.PassiveDelay, false);
                        AggroTooltip.Activate();
                        
                    }
                }
            }
        }
    }
}
