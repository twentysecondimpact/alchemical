﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Scr_Character_Movement : MonoBehaviour
{
    #region Variables

    #region Tracking Variables
    //The X/Y input vector from the Unity Input Manager
    public Vector3 InputVector;
    //The intended directoin the character wants to face
    public Quaternion TargetRotation;
    //The angle the kayborad model wants to face locally
    public Vector3 TargetKeyboardRotation;
    //The current progress from speed 0 to max
    public float CurrentAccelerationPercentage;
    public bool MovingForward = true;
    public Transform CharacterModelTransformMouse;
    public Transform CharacterModelTransformKeyboard;
    public Vector3 MouseRotationAngle;
    public float KeyboardRotationAngle;
    float ModelExtraRotationAngle;
    bool PreviousMovingForward;

    public float PlayerGravity;

    public bool Grounded;
    public bool PreviousGrounded;

    public float JumpForce;
    public float CurrentJumpForce;
    public float JumpForceGravity;

    public float JumpReductionMultiplier;
    public float JumpReductionMultiplierDecay;

    public float PreviousYPosition;
    public float CurrentYVelocity;

    public Vector3 CurrentMoveSpeed;

    public bool UseVariableJump;

    public Vector3 hitNormal;
    public float SlideFriction;
    public float HorizontalSlopeForce;
    public float VerticalSlopeForce;
    public Vector3 currentSlopeVelocity;



    public enum GroundType
    {
        Grass,
        Stone
    }

    public GroundType currentGroundType;
    #endregion

    #region Tweaking Variables
    //The time it takes to do a full 360 turn
    public float TurnTime;
    //The rate the player accelerates
    public float AccelerationSpeed;
    //The rate the player decelerates
    public float DecelerationSpeed;
    public float SlideDecelerationSpeed;
    //The max speed the player can reach
    public float RunSpeed;
    //The speed the play moves at while casting
    public float CastSpeed;
    //The base speed kept
    public float BaseRunSpeed;
    public float OriginalBaseRunSpeed;
    //The percentage speed you move at when moving backwards
    public float BackwardMovementMultiplier;
    public float TurnLerpSpeed;
    public Vector3 ChargeThrowDirection;
    public Vector3 LastGroundedPosition;
    public Vector3 LastNavPosition;
    #endregion

    #region External Component References
    public Transform CameraAnchor;
    public Transform LeftCollisionFeeler;
    public Transform RightCollisionFeeler;
    public Transform LeftReturnFeeler;
    public Transform RightReturnFeeler;
    public Transform LeftDropFeeler;
    public Transform RightDropFeeler;
    public GameObject Syl;
    #endregion

    #region Internal Component References
    #endregion

    #endregion

    #region Functions

    void Start ()
    {
        BaseRunSpeed = RunSpeed;
        OriginalBaseRunSpeed = BaseRunSpeed;

        //Required to initially set the directoin of the character
        TargetRotation = Quaternion.LookRotation(CameraAnchor.forward);

        //Detach the parent to rotate and move freely
        CharacterModelTransformMouse.parent = this.transform.parent;
        MouseRotationAngle = transform.rotation.eulerAngles;
        //CharacterModelTransformMouse.rotation = transform.rotation;
        PreviousYPosition = this.transform.position.y;

        //GetComponent<CharacterController>().enableOverlapRecovery = true;

    }

    void Update()
    {
        //Debug delta time log
        //Debug.Log(Time.deltaTime);

        if (Scr_GameDirector.inst.gameState == Scr_GameDirector.GameState.InGame)
        {
            //if(Scr_Character_Components.inst.character_Throw.PlayerThrowState == Scr_Character_Throw.ThrowState.Neutral)
            //{
            UpdateInput();
            UpdateDirection();
            //}
            
            UpdatePosition();

            UpdateModelFacingRotation();
        }
        else
        {
            CurrentMoveSpeed = Vector3.zero;
            //GetComponent<Scr_Character_AnimatorVariableController>().run
        }
    }

    private void LateUpdate()
    {
        //UpdateModelFacingRotation();
        PreviousMovingForward = MovingForward;

        //Store previous grounded
        PreviousGrounded = Grounded;
        //Set new grounded
        Grounded = GetComponent<CharacterController>().isGrounded;

        if (Grounded)
        {
            LastGroundedPosition = transform.position;
        }

        CurrentYVelocity = this.transform.position.y - PreviousYPosition;
        PreviousYPosition = this.transform.position.y;

        //Spherecasts downwards to see where youd hit the ground if you were on the ground
        Vector3 groundPosition = Vector3.zero;
        RaycastHit downHit;
        Ray downRay = new Ray(transform.position + new Vector3(0, 0.5f, 0), new Vector3(0, -1, 0));
        if (Physics.SphereCast(downRay, 0.5f, out downHit))
        {
            if(downHit.collider.gameObject.name == "WorldTerrain")
            {
                currentGroundType = GroundType.Grass;
            }
            else
            {
                currentGroundType = GroundType.Stone;
            }

            groundPosition = downHit.point;
        }

        //Determines the position of where would be if you were on the navmesh
        NavMeshHit navHit = new NavMeshHit();
        if(NavMesh.SamplePosition(groundPosition, out navHit, 1f, NavMesh.AllAreas))
        {
            //Debug.Log("Updating Nav Position");
            LastNavPosition = navHit.position;
        }

        Debug.DrawLine(transform.position, LastNavPosition, Color.blue);
        

    }

    #region Update Functions

    void UpdateInput()
    {
        if(Scr_GameDirector.inst.gameState == Scr_GameDirector.GameState.InGame)
        {

            //Determine the input vector from unity input
            if (Scr_FollowCamera.inst.TurnWithKeyboard)
            {
                InputVector = new Vector3(0, 0, Input.GetAxis("Vertical"));
            }
            else
            {
                InputVector = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            }
            

            

            //If the input vecotr is 0 and you are also moving with the mouse
            //If the player if holding down both mouse buttons, input a forward command
            if (Input.GetMouseButton(0) && Input.GetMouseButton(1))
            {
                InputVector.z = Mathf.Clamp01(InputVector.z + 1);
            }

            //If you are moving left or right you are moving forwad
            if(InputVector.x != 0)
            {
                MovingForward = true;
            }

            //If you are moving forward you are moving forward
            if(InputVector.z > 0)
            {
                MovingForward = true;
            }

            //If you are moving backwards you are not moving forwards
            if (InputVector.z < 0)
            {
                MovingForward = false;
            }
            //If you are not holding back but you are dragging, you are facing forward
            if(InputVector.z == 0 && Scr_FollowCamera.inst.RightClickDrag && Scr_FollowCamera.inst.MouseDragDelta.x != 0)
            { 
                //MovingForward = true;
            }

            ////If you have keyboard turning and dont have backwards input you are moving forwards
            //if (Scr_FollowCamera.inst.TurnWithKeyboard && InputVector.z == 0)
            //{
            //    MovingForward = true;
            //}

            //Jumping input
            if (Grounded)
            {
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    bool onSlope = !(Vector3.Angle(Vector3.up, hitNormal) <= GetComponent<CharacterController>().slopeLimit);

                    if (!onSlope)
                    {
                        StartCoroutine(DelayJump(0.04f));
                    }
                }
            }


        }
    }

    void UpdateDirection()
    {
        //If there is any direction input detected or if you are dragging the right mouse button
        if ((Scr_FollowCamera.inst.RightClickDrag) || Scr_FollowCamera.inst.TurnWithKeyboard) //InputVector.sqrMagnitude > 0 ||
        {
                //Face the camera
                TargetRotation = Quaternion.LookRotation(CameraAnchor.forward);
                MouseRotationAngle = TargetRotation.eulerAngles;
        }

        //if the player is inputinning keys adjust rotation
        if (InputVector.magnitude > 0)
        {
            KeyboardRotationAngle = Vector2.SignedAngle(new Vector2(0, 1), new Vector2(-InputVector.x, InputVector.z));
        }

        //ExtraRotationAngle = Vector2.SignedAngle(new Vector2(0, 1), new Vector2(-InputVector.x, InputVector.z));


        transform.rotation = Quaternion.Euler(TargetRotation.eulerAngles.x, MouseRotationAngle.y + KeyboardRotationAngle, TargetRotation.eulerAngles.z);
    }

    void UpdatePosition()
    {
        //Override grounded if need be
        bool onSlope = !(Vector3.Angle(Vector3.up, hitNormal) <= GetComponent<CharacterController>().slopeLimit);

        Vector3 StartingPosition = transform.position;

        #region Update Speed from cast state
            if(MovingForward)
            {
                RunSpeed = BaseRunSpeed;
            }
            else
            {
                RunSpeed = BaseRunSpeed * BackwardMovementMultiplier;
            }
        #endregion

        #region Update Acceleration
        //If there is any direction input detected -> Accelerate
        if (Scr_Character_Components.inst.character_Throw.PlayerThrowState == Scr_Character_Throw.ThrowState.Charging)
        {
            if (InputVector.sqrMagnitude > 0)
            {
                CurrentAccelerationPercentage = Mathf.Clamp01(CurrentAccelerationPercentage - ( SlideDecelerationSpeed * Time.deltaTime));
            }
            else
            {
                CurrentAccelerationPercentage = Mathf.Clamp01(CurrentAccelerationPercentage - (DecelerationSpeed * Time.deltaTime));
            }
                
        }
        else if (InputVector.sqrMagnitude > 0)
        {
            //Increase the acceleration percentage
            CurrentAccelerationPercentage = Mathf.Clamp01(CurrentAccelerationPercentage + (AccelerationSpeed * Time.deltaTime));
        }
        //If there is no direction input -> Decelerate
        else
        {
            //Reduce the acceleration
            CurrentAccelerationPercentage = Mathf.Clamp01(CurrentAccelerationPercentage - (DecelerationSpeed * Time.deltaTime));
        }
        #endregion

        #region Update Horizontal Position

        #region Collision Detection

     


        #endregion
        //Move the character forward based on rotation
        //transform.position += CollisionVelocity * RunSpeed * CurrentAccelerationPercentage * Time.deltaTime;
        Vector3 TargetMovementDelta = transform.forward * RunSpeed * CurrentAccelerationPercentage * Time.deltaTime;

        //Debug.Log(CollisionVelocity * RunSpeed * CurrentAccelerationPercentage * Time.deltaTime);

        #endregion

        #region Update Vertical Position


        if (onSlope && Grounded)
        {
            currentSlopeVelocity.x += (1f - hitNormal.y) * hitNormal.x * (1f - SlideFriction) * HorizontalSlopeForce * Time.deltaTime;// * SlideForce;
            currentSlopeVelocity.z += (1f - hitNormal.y) * hitNormal.z * (1f - SlideFriction) * HorizontalSlopeForce * Time.deltaTime;// * SlideForce;
            currentSlopeVelocity.y -= VerticalSlopeForce * Time.deltaTime;
        }
        else
        {
            if(currentSlopeVelocity.magnitude > 0)
            {
                //Debug.Log("Resetting Slope Gravity");
            }

            //currentSlopeVelocity = Vector3.Lerp(currentSlopeVelocity, Vector3.zero, 1 * Time.deltaTime);

            currentSlopeVelocity.x = Mathf.Clamp(currentSlopeVelocity.x - (HorizontalSlopeForce * Time.deltaTime) , 0,100);
            currentSlopeVelocity.z = Mathf.Clamp(currentSlopeVelocity.z - (HorizontalSlopeForce * Time.deltaTime) , 0, 100);
            currentSlopeVelocity.y = 0;// Mathf.Clamp(currentSlopeVelocity.y + (HorizontalSlopeForce * Time.deltaTime * 0.1f) , -100, 0);

            //currentSlopeVelocity = Vector3.zero;
        }


        GetComponent<CharacterController>().Move(TargetMovementDelta + currentSlopeVelocity);

            

        


        #region Jumping
        //Move up by jump force
        GetComponent<CharacterController>().Move(new Vector3(0, CurrentJumpForce, 0) * Time.deltaTime);
        //If jump force is higher than 0 reduce by jump gravity
        if(!Grounded)
        {
            //Debug.Log("Grounded: " + Grounded);

            if (UseVariableJump)
            {
                //If youre holding down, then it recuses over time
                if (Input.GetKey(KeyCode.Space))
                {
                    JumpReductionMultiplier = (Mathf.Clamp(JumpReductionMultiplier + (JumpReductionMultiplierDecay * Time.deltaTime), -0, 99));
                }
                //If for a moment you are nothing holding it, it goes straight to 1
                else
                {
                    JumpReductionMultiplier = 1;
                }
            }
            else
            {
                JumpReductionMultiplier = 1;
            }

            

            CurrentJumpForce = Mathf.Clamp(CurrentJumpForce - (JumpForceGravity * JumpReductionMultiplier * Time.deltaTime), -PlayerGravity, JumpForce + PlayerGravity);
        }

        #endregion

        #region Gravity

        //DetermineSlop
        
        


        GetComponent<CharacterController>().Move(new Vector3( 0, -PlayerGravity, 0) * Time.deltaTime);
        #endregion
        #endregion

        

        CurrentMoveSpeed = (transform.position - StartingPosition) / Time.deltaTime;
        CurrentMoveSpeed.y = 0;
    }

    public void StartCast()
    {
        StartCoroutine(ThrowTargetStanedingLeeway(0.25f));
    }

    IEnumerator ThrowTargetStanedingLeeway(float _Time)
    {
        for (float i = 0; i < _Time; i+= Time.deltaTime)
        {
            //Updates the aim target over a few frames
            Scr_Character_Components.inst.character_Movement.ChargeThrowDirection = Scr_Character_Components.inst.character_Throw.TargetPosition;
            yield return null;
        }
    }

    void UpdateModelFacingRotation()
    {
        //Updates the mouse model position
        CharacterModelTransformMouse.position = transform.position;

        //If you
        if (Scr_Character_Components.inst.character_Throw.PlayerThrowState == Scr_Character_Throw.ThrowState.Neutral && InputVector.magnitude > 0) // && InputVector.magnitude > 0
        {
            #region Mouse Model
            //Grab the mouse rotation
            Vector3 GrabbedRotation = MouseRotationAngle;

            //Apply it to the mouse rotation model
            CharacterModelTransformMouse.rotation = Quaternion.Euler(GrabbedRotation);
            #endregion

            #region Keyboard Model
            TargetKeyboardRotation = new Vector3(0, KeyboardRotationAngle, 0);

            //Adjust for backwards facing
            if (!MovingForward)
            {
                TargetKeyboardRotation.y -= 180;
            }

            #endregion
        }
        else if (Scr_Character_Components.inst.character_Throw.PlayerThrowState == Scr_Character_Throw.ThrowState.Charging)
        {

            //If you are sliding Update the throw direction to be the target position of the flask
            if (Scr_Character_Components.inst.character_Movement.CurrentAccelerationPercentage > 0)
            {
                Scr_Character_Components.inst.character_Movement.ChargeThrowDirection = Scr_Character_Components.inst.character_Throw.TargetPosition;

                if(Scr_Character_Components.inst.character_Movement.currentGroundType == GroundType.Grass)
                {
                    //If on grass
                    if (!Scr_Character_Components.inst.character_AnimEvents.GrassFootSlide_Particles.isPlaying)
                    {
                        
                        //If on grass
                        Scr_Character_Components.inst.character_AnimEvents.GrassFootSlide_Particles.Play();
                    }
                }
                else
                {
                    //If on grass
                    if (!Scr_Character_Components.inst.character_AnimEvents.StoneFootSlide_Particles.isPlaying)
                    {
                        //If on stone
                        Scr_Character_Components.inst.character_AnimEvents.StoneFootSlide_Particles.Play();
                    }
                }
            }
            else
            {
                //If on grass
                if (Scr_Character_Components.inst.character_AnimEvents.GrassFootSlide_Particles.isPlaying)
                {
                    //Debug.Log("Disabling grass slide");
                    Scr_Character_Components.inst.character_AnimEvents.GrassFootSlide_Particles.Stop();
                }

                //If on stone
                if (Scr_Character_Components.inst.character_AnimEvents.StoneFootSlide_Particles.isPlaying)
                {
                    //Debug.Log("Disabling stone slide");
                    Scr_Character_Components.inst.character_AnimEvents.StoneFootSlide_Particles.Stop();
                }
            }

            //Determine the rotation that would be looking at the flask target point
            Vector3 FlaskTargetRotation = Quaternion.LookRotation(ChargeThrowDirection - CharacterModelTransformMouse.position).eulerAngles;
            FlaskTargetRotation.x = 0;
            FlaskTargetRotation.z = 0;
            //Vector3 FlaskYShavedRotation = FlaskTargetRotation.eulerAngles;
            //FlaskYShavedRotation.y = 0;
            //FlaskTargetRotation = Quaternion.Euler(FlaskYShavedRotation);

            #region Mouse Model
            //Apply make the mouse model face the flask point, slerping cause you cannot control with mouse drag
            CharacterModelTransformMouse.rotation = Quaternion.SlerpUnclamped(CharacterModelTransformMouse.rotation, Quaternion.Euler(FlaskTargetRotation), TurnLerpSpeed * Time.deltaTime);
            //CharacterModelTransformMouse.rotation = 
            #endregion

            //Set the target keyboard rotation to 0
            TargetKeyboardRotation.y = 0;

        }



        //Lerp the Keyborad target rotation
        CharacterModelTransformKeyboard.localRotation = Quaternion.SlerpUnclamped(CharacterModelTransformKeyboard.localRotation, Quaternion.Euler(TargetKeyboardRotation), TurnLerpSpeed * Time.deltaTime);
    }

    #region Single Functions

    public void Jump()
    {

            CurrentJumpForce = JumpForce + PlayerGravity;
            JumpReductionMultiplier = 0;


        //Scr_Character_Components.inst.character_AnimVariables.TriggerJump();
    }

    public void ResetCharacterMovement()
    {
        MouseRotationAngle = Vector3.zero;
        KeyboardRotationAngle = 0;
        TargetKeyboardRotation = Vector3.zero;
        Syl.GetComponent<Scr_Syl_Movement>().TargetEnemies.Clear();
        Syl.GetComponent<Scr_Syl_Movement>().TargetPickups.Clear();
    }

    IEnumerator DelayJump(float _Delay)
    {
        Scr_Character_Components.inst.character_AnimVariables.TriggerJump();
        yield return new WaitForSeconds(_Delay);
        Jump();
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawSphere(LastNavPosition, 0.5f);
    }

    public void PauseCharacterMovement()
    {


        //Enable the cursor when you step pause character movement
        if (Scr_FollowCamera.inst.DraggingCamera == true)
        {
            Scr_FollowCamera.inst.RightClickDrag = false;
            Scr_FollowCamera.inst.StopDrag();
        }
    }

    public void UnpauseCharacterMovement()
    {
        CurrentAccelerationPercentage = 0;
    }

    void OnControllerColliderHit(ControllerColliderHit hit)
    {

        hitNormal = hit.normal;

    }
    #endregion

    #endregion
    #endregion
}
