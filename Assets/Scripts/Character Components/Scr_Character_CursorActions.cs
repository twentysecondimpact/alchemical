﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_Character_CursorActions : MonoBehaviour
{

    public LayerMask EnemyLayerMask;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        CheckAggroRadius();	
	}

    void CheckAggroRadius()
    {
        if(Input.GetKey(KeyCode.LeftShift))
        {

            if(Scr_FollowCamera.inst.DraggingCamera == false)
            {
                //Debug.Log("Shooting Ray");
                //Only check radius if you are not aiming something
                //if (GetComponent<Scr_Character_Throw>().PlayerThrowState != Scr_Character_Throw.ThrowState.Charging)
                //{
                Ray CameraRay = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit Hit = new RaycastHit();


                if (Physics.Raycast(CameraRay, out Hit, 99, EnemyLayerMask))
                {
                    //Debug.Log(Hit.transform.gameObject.name);
                    Hit.transform.gameObject.GetComponent<Scr_Enemy_AggroDisplay>().selected = true;
                }
                //}
            }


        }
    }
}
