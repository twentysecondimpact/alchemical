﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_Character_Throw : MonoBehaviour
{
    #region Variables

    #region Tracking Variables
    //The position the player wishes to throw the flask
    public Vector3 TargetPosition;
    //The possible states connected with the throw
    public enum ThrowState
    {
        Neutral,
        Charging
    }
    //Tracks the players throw state
    public ThrowState PlayerThrowState = new ThrowState();
    //the current Index of the flask being throw
    public int FlaskIndex;
    //The 4 cooldowns used to determine if the player can throw flasks or not
    public float[] FlaskCooldowns;
    public float[] SavedFlaskCooldowns;
    //Use to track the charges of Cooldowns
    public int[] FlaskCharges;
    public int[] SavedFlaskCharges;
    //The cast timers for throwing flasks
    public float CastTimer;
    //The possible CastBarStates
    public enum CastState
    {
        NotReady,
        Ready
    }
    public CastState castState;
    float GlobalCoolDownTimer;

    public bool EasingInToThrow;
    #endregion

    #region Tweaking Variables
    public float ArkHeight;
    public float MinArkHeight;
    public float ArkHeightDistanceFactor;
    public float gravity;
    public Vector3 AimReticleRotaton;
    public KeyCode[] FlaskKeybinds;
    public int CurrentFlask;
    public float CharacterMaxRange;
    public float MaxRange;
    public LayerMask terrainLayermask;
    public LayerMask projectionMeshMask;
    public LayerMask throwMeshMask;
    public LayerMask pureTerrainLayerMask;
    public float PerfectThrowLeeway;
    public float GlobalCoolDown;
    public float MinimumCastTime;
    public float ThrowDelay;
    public bool KeyUpToThrow;
    #endregion

    #region External Component References
    public GameObject[] FlaskPrefab;
    public GameObject[] SavedFlaskPrefabs;
    public GameObject[] FlaskProps;
    public Rigidbody Flask;
    public int FlaskPropIndex;
    public LineRenderer FlaskArcRenderer;
    public TrailRenderer FlaskTrailRenderer;
    public GameObject AimReticle;
    public Collider ThrowProjectionCollider;
    public Transform ThrowingHandBone;
    public TooltipObject CooldownTooltip;
    //public AudioEffectSO AESO_Charge;
    #endregion

    #region Internal Component References

    #endregion

    #endregion

    #region Functions

    void Start()
    {
        SaveFlaskPrefabs();
        //Sets cast starte to ready so you can always throw
        castState = CastState.Ready;
    }

    void Update()
    {
        UpdateInput();
        
        switch (PlayerThrowState)
        {
            case ThrowState.Neutral:
                UpdateThrowStateNeutral();
                //UpdatePassiveAimReticle();
                break;
            case ThrowState.Charging:
                UpdateThrowStateCharging();
                if(Flask != null)
                {
                    UpdateThrowArcVisual();
                }
                
                break;
        }

        UpdateTargetPosition();


        UpdateCooldowns();
        UpdateGlobalCoolDown();
    }

    #region Update Functions

    void UpdateInput()
    {
        //If the game is currently active take inputs
        if (Scr_GameDirector.inst.gameState == Scr_GameDirector.GameState.InGame)
        {
            //Canceling with movement key
            if (PlayerThrowState == ThrowState.Charging)
            {
                if(Scr_FollowCamera.inst.TurnWithKeyboard == false)
                {
                    if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.D))
                    {
                        CancelThrow();
                    }
                }
                
            }

            //Canceling with right click
            if (Input.GetMouseButtonDown(1))
            {
                if (PlayerThrowState == ThrowState.Charging)
                {
                    CancelThrow();
                }
            }

            //Cancel when you jump
            if(Input.GetKeyDown(KeyCode.Space))
            {
                if (PlayerThrowState == ThrowState.Charging)
                {
                    CancelThrow();
                }
            }

            //Charging with keybinds
            for (int i = 0; i < FlaskKeybinds.Length; i++)
            {
                if (Input.GetKeyDown(FlaskKeybinds[i]))
                {
                    if (PlayerThrowState == ThrowState.Neutral && GetComponent<Scr_Character_Movement>().Grounded)
                    {
                        if(FlaskPrefab[i] != null)
                        {
                            //Check if the ability isnt on cooldown
                            if (FlaskCharges[i] > 0)
                            {
                                FlaskIndex = i;
                                StartThrowCharge();
                            }
                            else
                            {
                                CooldownTooltip.Activate();
                            }
                        }
                    }
                    else if(PlayerThrowState == ThrowState.Charging)
                    {
                        if (FlaskPrefab[i] != null)
                        {
                            //Check if the ability is the same as youre chargin
                            if (i  == FlaskIndex)
                            {
                                StartCoroutine(DelayedThrow(ThrowDelay));
                                //StartThrow();
                            }
                            else
                            {

                                //cancel the current charge
                                CancelThrow();

                                if (FlaskCharges[i] > 0)
                                {
                                    //Start a new one
                                    FlaskIndex = i;
                                    StartThrowCharge();
                                }
                                else
                                {
                                    //Play cooldown tooltip
                                    CooldownTooltip.Activate();
                                }
                            }
                        }
                    }
                }
            }

            
            //Throwing with mouse up with keybinds
            if(KeyUpToThrow)
            {
                if (PlayerThrowState == ThrowState.Charging)
                {
                    for (int i = 0; i < FlaskKeybinds.Length; i++)
                    {
                        if (Input.GetKeyUp(FlaskKeybinds[i]))
                        {
                            if (FlaskPrefab[i] != null)
                            {
                                //Check if the ability is the same as youre chargin
                                if (i == FlaskIndex)
                                {
                                    StartCoroutine(DelayedThrow(ThrowDelay));
                                    //StartThrow();
                                }
                            }
                        }

                    }
                }
            }
            

            //Throwing on mouse release
            if (Input.GetMouseButtonUp(0))
            {
                if (PlayerThrowState == ThrowState.Charging && castState == CastState.Ready)
                {
                    StartCoroutine(DelayedThrow(ThrowDelay));
                    //StartThrow();
                }
            }

            //Throwing on mouse release
            if (Input.GetMouseButtonDown(0))
            {
                if (PlayerThrowState == ThrowState.Charging && castState == CastState.Ready)
                {
                    StartCoroutine(DelayedThrow(ThrowDelay));
                    //StartThrow();
                }
            }


        }
    }

    void UpdateTargetPosition()
    {
        if(Scr_FollowCamera.inst.DraggingCamera == false)
        {
            Ray CameraRay = new Ray();

            //if (Scr_FollowCamera.inst.DraggingCamera)
            //{
            //    CameraRay = Camera.main.ScreenPointToRay(new Vector2(Scr_FollowCamera.inst.SavedMousePosition.X, Scr_FollowCamera.inst.SavedMousePosition.Y));
            //}
            //else
            //{
            //    CameraRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            //}

            CameraRay = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit Hit = new RaycastHit();

            LayerMask toggledLayerMask = new LayerMask();

            if (Input.GetKey(KeyCode.LeftControl))
            {
                toggledLayerMask = terrainLayermask;
            }
            else
            {
                toggledLayerMask = pureTerrainLayerMask;
            }

            if (Physics.Raycast(CameraRay, out Hit, 9999, toggledLayerMask))
            {

                //Debug.DrawLine(CameraRay.origin, CameraRay.direction.normalized * 9999, Color.red);

                #region Range Based Clamping

                //If the mouse hit point is within range of the maximum, set TargetPosition
                if (Vector3.Distance(Hit.point, transform.position) <= MaxRange)
                {

                    if (Hit.collider.gameObject.tag == "Enemy")
                    {
                        //Ray cast down on terrain layer
                        Ray TerrainDownRay = new Ray(Hit.point, new Vector3(0, -1, 0));
                        Debug.DrawRay(Hit.point, new Vector3(0, -1, 0), Color.red);

                        RaycastHit TerrainHit = new RaycastHit();
                        if(Physics.Raycast(TerrainDownRay, out TerrainHit, 10, pureTerrainLayerMask))
                        {
                            //Debug.Log("Hit target hit ground");
                            Debug.Log(TerrainHit.point);
                            Debug.Log(TerrainHit.collider.gameObject.name);
                            TargetPosition = Hit.collider.gameObject.transform.position;
                            //TargetPosition = TerrainHit.point;
                        }
                        else
                        {
                            Debug.Log("Hit target not hit ground");
                        }
                    }
                    else
                    {
                        //Debug.Log("Not hit");
                        TargetPosition = Hit.point;
                    }
                    //Debug.Log("Origin: " + CameraRay.origin);
                    //Debug.Log("Direction: " + CameraRay.direction);
                    //Debug.Log("Object Hit: " + Hit.collider.gameObject);
                    
                    //Debug.Log("Target Position: " + TargetPosition);
                }
                //If its ouside max range
                else
                {
                    //Normalize the vector and make it the max range
                    Vector3 ClampedTargetPosition = transform.position + (Hit.point - transform.position).normalized * MaxRange;

                    //Set the y to the hight of the camera, ready to raytrace down
                    ClampedTargetPosition.y = Camera.main.transform.position.y;

                    //Cast a ray downwards to hit the highest available object
                    RaycastHit DownHit = new RaycastHit();
                    if (Physics.Raycast(new Ray(ClampedTargetPosition, new Vector3(0, -1, 0)), out DownHit, 9999, toggledLayerMask))
                    {
                        //Debug.Log("Updating due to nothing hit");
                        TargetPosition = DownHit.point;
                    }
                }

                //Snapping within foward boundry
                if(EasingInToThrow == false)
                {
                    if (!Physics.CheckSphere(TargetPosition, 0.0001f, projectionMeshMask))
                    {
                        //TargetPosition = ThrowProjectionCollider.ClosestPoint(TargetPosition);

                        //Debug.Log()

                        //snaps target poition to 0
                        TargetPosition.y = 0;

                        //Normalize the vector and make it the max range
                        Vector3 ClampedTargetPosition = ThrowProjectionCollider.ClosestPoint(TargetPosition);

                        //Set the y to the hight of the camera, ready to raytrace down
                        ClampedTargetPosition.y = Camera.main.transform.position.y;

                        //Cast a ray downwards to hit the highest available object
                        RaycastHit DownHit = new RaycastHit();
                        if (Physics.Raycast(new Ray(ClampedTargetPosition + new Vector3(0, 0.1f, 0), new Vector3(0, -1, 0)), out DownHit, 9999, toggledLayerMask))
                        {
                            TargetPosition = DownHit.point;
                        }

                    }
                }
                

                //Debug.Log("Final Target Position: " + TargetPosition);

                #endregion
            }
        }
        
    }

    void UpdateThrowStateNeutral()
    {

    }

    void UpdateThrowStateCharging()
    {
        //Update the cast time counter
        if(CastTimer > 0)
        {
            CastTimer -= Time.deltaTime;
            //castState = CastState.NotReady;
        }
        else
        {
            if(CastTimer <= 0)
            {
                //castState = CastState.Ready;
            }
        }

        float PercentDistance = Vector3.Distance(TargetPosition, Flask.position) / CharacterMaxRange;
        //Debug.Log("Flask Y: " + Flask.transform.position.y);


        //Determiens if the height of the ark is based on player position of target position

        //Start off with the arc being at player height
        float CalculatedArkHeight = transform.position.y;

        //if the
        if(TargetPosition.y > CalculatedArkHeight)
        {
            CalculatedArkHeight = TargetPosition.y;
        }

        ArkHeight = Mathf.Clamp((CalculatedArkHeight) + (ArkHeightDistanceFactor * PercentDistance) - (Flask.transform.position.y - 1), MinArkHeight, CalculatedArkHeight + ArkHeightDistanceFactor);


    }

    void UpdateThrowArcVisual()
    {
        #region Arc Renderer
        LaunchData launchData = CalculateLaunchData(TargetPosition, Flask.position);
        Vector3 previousDrawPoint = Flask.position;

        //Set the fidelity of the line renderer
        int resolution = 30;
        FlaskArcRenderer.positionCount = resolution;

        //Set the position of each of the nodes
        for (int i = 0; i < resolution; i++)
        {
            float simulationTime = i / (float)resolution * launchData.timeToTarget;
            Vector3 displacement = launchData.initialVelocity * simulationTime + Vector3.up * gravity * simulationTime * simulationTime / 2f;
            Vector3 drawPoint = Flask.position + displacement;
            FlaskArcRenderer.SetPosition(i, drawPoint);
            previousDrawPoint = drawPoint;
        }
        #endregion

        #region Aim Reticle
        //Update the target reticle positoin to the mouse position
        
        //update the reticle rotation


        

        if(FlaskIndex == 3)
        {
            AimReticle.transform.Rotate(AimReticleRotaton * Time.deltaTime);

            AimReticle.transform.position = transform.position;
        }
        else
        {
            AimReticle.transform.position = TargetPosition;

            Quaternion calculatedRotation = Quaternion.LookRotation(AimReticle.transform.position - this.transform.position) * Quaternion.Euler(0, 45, 0);
            calculatedRotation = Quaternion.Euler(0, calculatedRotation.eulerAngles.y, 0);
            AimReticle.transform.rotation = calculatedRotation;// Quaternion.LookRotation(AimReticle.transform.position - Flask.transform.position) * Quaternion.Euler(0, 45, 0);
        }


        //Update the highlight of enemies
        
        Collider[] collidersInRange = Physics.OverlapSphere(TargetPosition, Flask.gameObject.GetComponent<Scr_Flask>().radius * 0.9f);
        foreach (Collider colliderInRange in collidersInRange)
        {
            //Debug.Log(collidersInRange.Length + " Colliders in range");
            if (colliderInRange.tag == "Enemy")
            {
                //Debug.Log("Enemy in Range");
                colliderInRange.GetComponent<Scr_Enemy>().ApplyWhiteHightlightColor();
            }
        }
        

        #endregion
    }

    void UpdateCooldowns()
    {
        
        for (int i = 0; i < FlaskPrefab.Length; i++)
        {
            //If you have at least 1 flask unlocked
            if (FlaskPrefab[i] != null)
            {
                if(!FlaskPrefab[i].GetComponent<Scr_Flask>().Ultimate)
                {
                    //If its larger than - keep counting
                    if (FlaskCooldowns[i] > 0)
                    {
                        FlaskCooldowns[i] -= Time.deltaTime;
                    }
                    //If it has reached 0 but you arent at full charges yet, add a charge and reset the cooldown to max again
                    else if (FlaskCooldowns[i] <= 0 && FlaskCharges[i] < FlaskPrefab[i].GetComponent<Scr_Flask>().MaxCharges)
                    {
                        FlaskCharges[i] += 1;

                        //Check if you are at max charges now, if not reset CD
                        if (FlaskCharges[i] < FlaskPrefab[i].GetComponent<Scr_Flask>().MaxCharges)
                        {
                            FlaskCooldowns[i] = FlaskPrefab[i].GetComponent<Scr_Flask>().cooldown;
                        }
                    }
                }
            }
        }
        
    }

    void UpdateGlobalCoolDown()
    {
        if(GlobalCoolDownTimer > 0)
        {
            GlobalCoolDownTimer -= Time.deltaTime;
        }
        else
        {
            GlobalCoolDownTimer = 0;
        }
    }

    #endregion

    #region Single Functions

    public void ToggleKeyUpThrow()
    {
        KeyUpToThrow = !KeyUpToThrow;
    }

    void StartThrowCharge()
    {
        if(GlobalCoolDownTimer <= 0)
        {
            //If the camera is dragging, stop it

            //Reset the cursor immediately
            //Scr_FollowCamera.inst.InstantResetCamera();


            //Debug.Log("Start Charge");
            GetComponent<Scr_Character_AudioController>().PlayHowlendFlaskChargeStart();
            GetComponent<Scr_Character_AudioController>().PlayChargeAudio();

            if (Scr_FollowCamera.inst.DraggingCamera)
            {
                //TODO this waits till the end of the frame to reset the camera position
                Scr_FollowCamera.inst.StopDrag();
                //Debug.Log("Mouse Position: " + Input.mousePosition);
            }


            PlayerThrowState = ThrowState.Charging;
            //Create the flask
            GameObject NewFlask = GameObject.Instantiate(FlaskPrefab[FlaskIndex]);
            //Link its rigid body to the characters reference
            Flask = NewFlask.GetComponent<Rigidbody>();
            //Unparent the Flask
            Flask.transform.parent = ThrowingHandBone;
            //Set its position
            Flask.transform.localPosition = Vector3.zero;
            //Set its rotation
            Flask.transform.localRotation = Quaternion.identity;
            //Set the flask to not use gravity yet
            Flask.GetComponent<Scr_Flask>().GravityActive = false;
            //Sets the line renderer to the flasks renderer
            FlaskArcRenderer = Flask.gameObject.GetComponent<LineRenderer>();
            //Sets the flask trail renderer
            //Updates the arc renderers color
            Flask.GetComponent<Scr_Flask>().UpdateArcColor();

            Flask.gameObject.GetComponent<TrailRenderer>().enabled = false;



            //Enable the aim reticle
            AimReticle.GetComponent<Scr_AimReticle>().EnableReady();
            //Change the scale of the Reticle to match the radius of the flask
            if (Flask.GetComponent<Scr_Flask>().StartsAtRadius0)
            {
                AimReticle.transform.localScale = Vector3.zero;// Vector3.one * 0.0001f;
            }
            else
            {
                AimReticle.transform.localScale = Vector3.one * Flask.gameObject.GetComponent<Scr_Flask>().radius;
            }

            //Update the color of the reticle
            MaxRange = Flask.GetComponent<Scr_Flask>().maxRange;

            //Runs the target position update to stop flickering on 1st frame
            UpdateTargetPosition();

            

            //Deparent the reticle
            AimReticle.transform.parent = null;
            //Set the cast timer to the flasks cast time
            CastTimer = Flask.GetComponent<Scr_Flask>().castTime;

            //Updates the character throw with the location of the target position
            StartCoroutine(EaseInThrowDirection(0.1f));
            

            //Face direction of throw
            //Ray CameraRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            //RaycastHit Hit = new RaycastHit();

            //if (Physics.Raycast(CameraRay, out Hit, 9999, terrainLayermask))
            //{
            //    //Scr_Character_Components.inst.character_Movement.MovingForward = true;
            //    //transform.LookAt(new Vector3(Hit.point.x, transform.position.y, Hit.point.z));
            //    Scr_Character_Components.inst.character_Movement.ChargeThrowDirection = new Vector3(Hit.point.x, transform.position.y, Hit.point.z);
            //    //Scr_Character_Components.inst.character_Movement.CharacterModelTransform.LookAt(new Vector3(Hit.point.x, transform.position.y, Hit.point.z));
            //}

            //Hides the current prop
            HideFlaskProps();

            //Update the animator
            Scr_Character_Components.inst.character_AnimVariables.SetCharging(true);
        }
    }

    void StartThrow()
    {
        //makes the cursor visible
        //Cursor.visible = true;
        if((Flask.GetComponent<Scr_Flask>().castTime - CastTimer) > MinimumCastTime)
        {
            //Debug.Log(Flask.GetComponent<Scr_Flask>().castTime - CastTimer);

            

            PlayerThrowState = ThrowState.Neutral;
            //Turn on the throw prediction
            FlaskArcRenderer.enabled = false;
            //Turn on the trail renderer
            //FlaskTrailRenderer.enabled = true;
            //Detaches the line renderer reference
            FlaskArcRenderer = null;
            //Unparent the Flask
            Flask.transform.parent = null;
            //Sebastion Throw Code
            Flask.GetComponent<Scr_Flask>().StartThrow(gravity, TargetPosition);
            //Sets the initial velocity for the flask when thrown
            Flask.velocity = CalculateLaunchData(TargetPosition, Flask.position).initialVelocity;
            //Parent the reticle
            //AimReticle.transform.parent = this.transform;
            //Disable the aim reticle
            //AimReticle.SetActive(false);
            AimReticle.GetComponent<Scr_AimReticle>().DisableReticle();
            AimReticle.transform.position = this.gameObject.transform.position;

            //If youre at max charges start the cooldown 
            if (FlaskCharges[FlaskIndex] == FlaskPrefab[FlaskIndex].GetComponent<Scr_Flask>().MaxCharges)
            {
                FlaskCooldowns[FlaskIndex] = Flask.gameObject.GetComponent<Scr_Flask>().cooldown;
            }

            //Reduce the flask charge by 1
            FlaskCharges[FlaskIndex] -= 1;

            //Checks to see if you hit a perfect throw and gives you the bonus
            CheckPerfectThrow();

            //Turn of the flasks collider (after a delay)
            //Flask.GetComponent<Collider>().enabled = true;
            Flask.GetComponent<Scr_Flask>().ActivateCollider();


            //Detach flask reference
            Flask = null;

            //Increase the amount of flasks used this level
            Scr_GameDirector.inst.FlasksUsed++;

            //Update the animator
            Scr_Character_Components.inst.character_AnimVariables.TriggerThrow();

            //Trigger global cooldown
            GlobalCoolDownTimer = GlobalCoolDown;


        }

        
    }

    public void CancelThrow()
    {
        GetComponent<Scr_Character_AudioController>().StopChargeAudio();
        //if(Input)
        //makes the cursor visible
        //Cursor.visible = true;
        //Set the throw state
        PlayerThrowState = ThrowState.Neutral;
        //Parent the reticle
        //AimReticle.transform.parent = this.transform;
        //Disable the aim reticle
        AimReticle.GetComponent<Scr_AimReticle>().DisableReticle();
        AimReticle.transform.position = this.gameObject.transform.position;
        //Destroy the flask that was created
        if(Flask != null)
        {
            Destroy(Flask.gameObject);
        }
        
        //Show the flask prop
        ShowFlaskProp(FlaskIndex);

        //Update the animator
        Scr_Character_Components.inst.character_AnimVariables.SetCharging(false);
    }

    LaunchData CalculateLaunchData(Vector3 _targetPosition, Vector3 _flaskPosition)
    {
        float displacementY = _targetPosition.y - _flaskPosition.y;

        Vector3 displacementXZ = new Vector3(_targetPosition.x - _flaskPosition.x, 0, _targetPosition.z - _flaskPosition.z);
        float time = Mathf.Sqrt(-2 * ArkHeight / gravity) + Mathf.Sqrt(2 * (displacementY + -ArkHeight) / gravity);
        Vector3 velocityY = Vector3.up * Mathf.Sqrt(-2 * gravity * ArkHeight);
        Vector3 velocityXZ = displacementXZ / time;

        return new LaunchData(velocityXZ + velocityY * -Mathf.Sign(gravity), time);
    }

    public void UnlockFlask(int _Index, GameObject _FlaskPrefab)
    {
        FlaskPrefab[_Index] = _FlaskPrefab;
        //Resets the charges to max when you aquire it
        FlaskCharges[_Index] = _FlaskPrefab.GetComponent<Scr_Flask>().MaxCharges;
    }

    public void ResetCooldowns()
    {
        for (int i = 0; i < FlaskPrefab.Length; i++)
        {
            if(FlaskPrefab[i] != null)
            {
                if (!FlaskPrefab[i].GetComponent<Scr_Flask>().Ultimate)
                {
                    FlaskCharges[i] = FlaskPrefab[i].GetComponent<Scr_Flask>().MaxCharges;
                    FlaskCooldowns[i] = 0;
                }
                
            }
        }
    }

    public void SaveFlaskPrefabs()
    {

        Debug.Log("Saving Flasks");
        for (int i = 0; i < FlaskPrefab.Length; i++)
        {
            if (FlaskPrefab[i] != null)
            {
                SavedFlaskPrefabs[i] = FlaskPrefab[i];

                SavedFlaskCooldowns[i] = FlaskCooldowns[i];
                SavedFlaskCharges[i] = FlaskCharges[i];
            }
            else
            {
                SavedFlaskPrefabs[i] = null;
            }

            
            if(i == 3)
            {
                //Only save the state of the ulti
                SavedFlaskCooldowns[i] = FlaskCooldowns[i];
                SavedFlaskCharges[i] = FlaskCharges[i];
            }
        }

        
    }

    public void LoadFlaskPrefabs()
    {
        for (int i = 0; i < 4; i++)
        {
            if (SavedFlaskPrefabs[i] != null)
            {
                FlaskPrefab[i] = SavedFlaskPrefabs[i];
                if (i < 3)
                {
                    FlaskCooldowns[i] = 0;// SavedFlaskCooldowns[i];
                    FlaskCharges[i] = 1;// SavedFlaskCharges[i];
                }
                else
                {
                    Debug.Log("Setting ulti cooldown to " + SavedFlaskCooldowns[i]);
                    FlaskCooldowns[i] = SavedFlaskCooldowns[i];
                    Debug.Log("Setting Ulti Charges to " + SavedFlaskCharges[i]);
                    FlaskCharges[i] = SavedFlaskCharges[i];
                }
            }
            else
            {
                FlaskPrefab[i] = null;
            }


            
        }
    }

    public bool CheckPerfectThrow()
    {

        Scr_Flask _flask = Flask.GetComponent<Scr_Flask>();
        float PerfectTime = _flask.castTime * _flask.BonusCastPosition;
        float PerfectLeeway = (_flask.castTime * _flask.BonusCastDuration) / 2;

        float PerfectTimeMin = PerfectTime - PerfectLeeway - PerfectThrowLeeway;
        float PerfectTimeMax = PerfectTime + PerfectLeeway + PerfectThrowLeeway;


        //Debug.Log("Current Time: " + (_flask.castTime - CastTimer));
        //Debug.Log("Perfect Time Min: " + PerfectTimeMin);
        //Debug.Log("Perfect Time Max: " + PerfectTimeMax);

        if ((_flask.castTime - CastTimer) >= PerfectTimeMin && (_flask.castTime - CastTimer) <= PerfectTimeMax)
        {
            //Reduce the cooldown if its part of the effect
            ReduceCooldowns(FlaskIndex, FlaskPrefab[FlaskIndex].GetComponent<Scr_Flask>().cooldown * FlaskPrefab[FlaskIndex].GetComponent<Scr_Flask>().PerfectBonus);
            //Also Trigger the bonus effect
            Flask.GetComponent<Scr_Flask>().ActivatePerfectShot();
            return true;
        }
        else
        {
            return false;
            //Debug.Log("Missed");
        }
    }

    public void UpdateUltimateCooldowns(float _Reduction)
    {
        for (int i = 0; i < FlaskPrefab.Length; i++)
        {
            //If you have at least 1 flask unlocked
            if (FlaskPrefab[i] != null)
            {
                if (FlaskPrefab[i].GetComponent<Scr_Flask>().Ultimate)
                {
                    //If its larger than - keep counting
                    if (FlaskCooldowns[i] > 0)
                    {
                        FlaskCooldowns[i] -= _Reduction;
                    }
                    //If it has reached 0 but you arent at full charges yet, add a charge and reset the cooldown to max again
                    if (FlaskCooldowns[i] <= 0 && FlaskCharges[i] < FlaskPrefab[i].GetComponent<Scr_Flask>().MaxCharges)
                    {
                        FlaskCharges[i] += 1;

                        //Check if you are at max charges now, if not reset CD
                        if (FlaskCharges[i] < FlaskPrefab[i].GetComponent<Scr_Flask>().MaxCharges)
                        {
                            FlaskCooldowns[i] = FlaskPrefab[i].GetComponent<Scr_Flask>().cooldown;
                        }
                    }
                }
            }
        }
    }

    public void ReduceCooldowns(int _flaskIndex, float _time)
    {
        //Take the whole time off the current cooldown
        FlaskCooldowns[_flaskIndex] -= _time;

        //Check if thats the cooldown is now less than 0
        if (FlaskCooldowns[_flaskIndex] < 0)
        {
            //If so increase the charges by 1
            FlaskCharges[_flaskIndex] += 1;

            //Check if the charges are at max
            if(FlaskCharges[_flaskIndex] < FlaskPrefab[_flaskIndex].GetComponent<Scr_Flask>().MaxCharges)
            {
                //If not set the cooldown to the max cooldown minus the exta bit left over (that was stored as a negative in the cooldown)
                FlaskCooldowns[_flaskIndex] = FlaskPrefab[_flaskIndex].GetComponent<Scr_Flask>().cooldown - Mathf.Abs(FlaskCooldowns[_flaskIndex]);
            }
            else
            {
                //Otherwise set cooldown to 0
                FlaskCooldowns[_flaskIndex] = 0;
            }
        }

        //If there is leftover, reduce the charge by 1, set the new cooldown to base cooldown - left over

    }

    public void RestartCharacterFlasks()
    {
        ResetCooldowns();
    }

    public void ShowFlaskProp(int FlaskID)
    {
        FlaskProps[FlaskID].SetActive(true);
    }

    public void HideFlaskProps()
    {
        for (int i = 0; i < FlaskProps.Length; i++)
        {
            FlaskProps[i].SetActive(false);
        }
        
    }

    IEnumerator DelayedThrow(float _Delay)
    {
        
        //StartThrow();

        if ((Flask.GetComponent<Scr_Flask>().castTime - CastTimer) > MinimumCastTime)
        {
            //Debug.Log(Flask.GetComponent<Scr_Flask>().castTime - CastTimer);
            GetComponent<Scr_Character_AudioController>().StopChargeAudio();
            PlayerThrowState = ThrowState.Neutral;
            //Turn on the throw prediction
            FlaskArcRenderer.enabled = false;
            //Turn on the trail renderer
            //FlaskTrailRenderer.enabled = true;
            //Detaches the line renderer reference
            FlaskArcRenderer = null;
            //Unparent the Flask
            //Parent the reticle
            //AimReticle.transform.parent = this.transform;
            //Disable the aim reticle
            //AimReticle.SetActive(false);
            AimReticle.GetComponent<Scr_AimReticle>().DisableReticle();
            AimReticle.transform.position = this.gameObject.transform.position;


            //If youre at max charges start the cooldown 
            if (FlaskCharges[FlaskIndex] == FlaskPrefab[FlaskIndex].GetComponent<Scr_Flask>().MaxCharges)
            {
                FlaskCooldowns[FlaskIndex] = Flask.gameObject.GetComponent<Scr_Flask>().cooldown;
            }

            //Reduce the flask charge by 1
            FlaskCharges[FlaskIndex] -= 1;

            //Checks to see if you hit a perfect throw and gives you the bonus
            bool perfectState = CheckPerfectThrow();

            //play the audio
            GetComponent<Scr_Character_AudioController>().PlayerHowlendFlaskThrow(perfectState);


            //Turn of the flasks collider (after a delay)





            //Increase the amount of flasks used this level
            Scr_GameDirector.inst.FlasksUsed++;

            //Update the animator
            Scr_Character_Components.inst.character_AnimVariables.TriggerThrow();

            //Trigger global cooldown
            GlobalCoolDownTimer = GlobalCoolDown;

            //Remember the grabbed flask before losing reference to it
            GameObject GrabbedFlask = Flask.gameObject;

            //Detach flask reference
            Flask = null;

            Vector3 GrabbedTargetPosition = TargetPosition;

            //Acivate the trail renderer
            //SGrabbedFlask.GetComponent<TrailRenderer>().enabled = true;

            yield return new WaitForSeconds(_Delay);

            //Null incase flask was destroyed before delay ends
            if(GrabbedFlask != null)
            {
                //Detach the parent
                GrabbedFlask.transform.parent = null;

                if(GrabbedFlask.GetComponent<Scr_Flask>() != null)
                {
                    //Sebastion Throw Code
                    GrabbedFlask.GetComponent<Scr_Flask>().StartThrow(gravity, TargetPosition);
                }
                
                //Sets the initial velocity for the flask when thrown
                GrabbedFlask.GetComponent<Rigidbody>().velocity = CalculateLaunchData(GrabbedTargetPosition, GrabbedFlask.transform.position).initialVelocity;
                //Flask.GetComponent<Collider>().enabled = true;
                GrabbedFlask.GetComponent<Scr_Flask>().ActivateCollider();
                //Acivate the trail renderer
                GrabbedFlask.GetComponent<TrailRenderer>().enabled = true;
            }

            
        }
    }

    //Used to update the throw direction over a few frames, fix for a but that makes the rotation not fully update in 1 frame
    IEnumerator EaseInThrowDirection(float _easeTime)
    {

        Scr_Character_Components.inst.character_Movement.StartCast();

        EasingInToThrow = true;
        for (float i = 0; i < _easeTime; i += Time.deltaTime)
        {
            //if(Scr_Character_Components.inst.character_Movement.MovingForward)
            //{
            //    Scr_Character_Components.inst.character_Movement.ChargeThrowDirection = TargetPosition;
            //}
            //else
            //{
            //    Scr_Character_Components.inst.character_Movement.ChargeThrowDirection = -TargetPosition;
            //}

            Scr_Character_Components.inst.character_Movement.ChargeThrowDirection = TargetPosition;

            

            yield return null;
        }
        EasingInToThrow = false;
        //Debug.Log(TargetPosition);
    }
    #endregion

    #endregion

    #region Structs
    struct LaunchData
    {
        public readonly Vector3 initialVelocity;
        public readonly float timeToTarget;

        public LaunchData(Vector3 initialVelocity, float timeToTarget)
        {
            this.initialVelocity = initialVelocity;
            this.timeToTarget = timeToTarget;
        }

    }
    #endregion

    #region Gizmo Functions
    private void OnDrawGizmos()
    {
        //Debug draw the target posiion
        //Gizmos.DrawCube(TargetPosition, Vector3.one);
    }
    #endregion

}
