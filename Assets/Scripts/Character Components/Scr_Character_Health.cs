﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_Character_Health : MonoBehaviour
{
    public int BaseHealth;
    public int Health;
    public int MaxHealth;
    public int Lives;

    public int SavedHealth;
    public int SavedLives;

    public bool Immune;

    //public int CurrentHealthOrbs;
    //public int HealthOrbsToHeart;

    public float ImmuneTime;
    float ImmuneTimer;

    public AudioEffectSO TakeDamage_AESO;

    public TooltipObject TTSO_TakeDamage;

    public CameraShakeSO CHSO_TakeDamage;

	// Use this for initialization
	void Start ()
    {
        Health = BaseHealth;
        SavedHealth = BaseHealth;
        Immune = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (Immune)
        {
            UpdateImmuneTimer();
        }
	}

    void UpdateImmuneTimer()
    {
        //Check if higher
        if(ImmuneTimer > 0)
        {
            //Reduce variable
            ImmuneTimer -= Time.deltaTime;
        }
        else
        {
            Immune = false;
            ImmuneTimer = 0;
        }
    }

    void StartImmunity()
    {
        ImmuneTimer = ImmuneTime;
        Immune = true;
    }

    public void TakeDamage(int _Damage)
    {
        if(Scr_GameDirector.inst.gameState == Scr_GameDirector.GameState.InGame || Scr_GameDirector.inst.gameState == Scr_GameDirector.GameState.Dialogue)
        {
            //Debug.Log("Got Hit");

            //Only player the damage sound if you take damage, not recieve it
            if( _Damage > 0)
            {
                TakeDamage_AESO.Play();
            }
            

            if (!Immune)
            {
                //Debug.Log("Took Damage");

                StartImmunity();

                Health -= _Damage;
                TTSO_TakeDamage.Activate();

                if (Health <= 0)
                {
                    if(Lives == 0)
                    {
                        
                        Die();
                    }
                    else
                    {
                        Lives -= 1;
                        Health = MaxHealth + Health;
                    }
                }

                //Shake Camera
                CHSO_TakeDamage.Shakeonce(1, 1, 1, 1);

                //Update Health UI
                Scr_HealthDisplay.inst.UpdateHealth();

            }
        }
        
    }

    public void Die()
    {
        //Destroy(this.gameObject);
        Debug.Log("Player died, resetting health");
        //GetComponent<Scr_Character_Movement>().RunSpeed = 0;

        //Cancels castign if you are casting
        if( Scr_Character_Components.inst.character_Throw.PlayerThrowState == Scr_Character_Throw.ThrowState.Charging)
        {
            Scr_Character_Components.inst.character_Throw.CancelThrow();
        }

        Scr_GameDirector.inst.Deaths++;

        Scr_GameDirector.inst.StartRestartAtCheckpoint();
    }

    public void AddHealth()
    {
        Health += 1;

        if(Health > MaxHealth)
        {
            //Health = 1;
            //Lives += 1;

            Health = MaxHealth;
        }

        Scr_HealthDisplay.inst.UpdateHealth();
    }

    public void RestartCharacterHealth()
    {
        Health = SavedHealth;
        Lives = SavedLives;
    }

    public void SaveCharacterHealth()
    {
        SavedHealth = Health;
        SavedLives = Lives;
    }

    private void OnDisable()
    {
        
    }
}
