﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_PlayerObjects : MonoBehaviour {

    public Transform PlayerCamera;
    public Transform PlayerCharacter;
    public Transform PlayerSyl;

    public Vector3 PlayerCameraPosition;
    public Vector3 PlayerCharacterPosition;
    public Vector3 PlayerSylPosition;

    public Quaternion PlayerCameraRotation;
    public Quaternion PlayerCharacterRotation;
    public Quaternion PlayerSylRotation;

    void Awake()
    {
        PlayerCameraPosition = PlayerCamera.localPosition;
        PlayerCharacterPosition = PlayerCharacter.localPosition;
        PlayerSylPosition = PlayerSyl.localPosition;

        PlayerCameraRotation = PlayerCamera.localRotation;
        PlayerCharacterRotation = PlayerCharacter.localRotation;
        PlayerSylRotation = PlayerSyl.localRotation;
}

    // Use this for initialization
    void Start () {
        PlayerCameraPosition = PlayerCamera.localPosition;

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ResetCharacterObjects()
    {
        PlayerCamera.localPosition = PlayerCameraPosition;
        PlayerCharacter.localPosition = PlayerCharacterPosition;
        PlayerSyl.localPosition = PlayerSylPosition;

        PlayerCamera.localRotation = PlayerCameraRotation;
        PlayerCharacter.localRotation = PlayerCharacterRotation;
        PlayerSyl.localRotation = PlayerSylRotation;
    }
}
