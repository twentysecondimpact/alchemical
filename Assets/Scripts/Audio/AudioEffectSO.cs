﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class AudioEffectSO : ScriptableObject
{
    public AudioClip[] clips;
    [Range(0.0f, 1.0f)]
    public float minVol = 1, maxVol = 1;
    [Range(-3.0f, 3.0f)]
    public float minPitch = 1, maxPitch = 1;

    public void Play(AudioSource source, bool oneShot = true)
    {
        if (source == null || clips == null || clips.Length == 0)
            return; //bail out not possible

        var clip = clips[Random.Range(0, clips.Length)];
        var curVol = Random.Range(minVol, maxVol);
        var curPitch = Random.Range(minPitch, maxPitch);

        if (oneShot)
        {
            source.PlayOneShot(clip);
        }
        else
        {
            source.clip = clip;
            source.pitch = curPitch;
            source.volume = curVol;
            source.Play();
        }
    }

    public void Play()
    {
        var s = AudioSourcePool.GetSource();
        Play(s, false);
        AudioSourcePool.ReturnSourceWhenDone(s);
    }
}