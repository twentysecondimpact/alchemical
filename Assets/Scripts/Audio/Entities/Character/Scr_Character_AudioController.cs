﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_Character_AudioController : MonoBehaviour
{
    public AudioEffectSO AESO_HowlendFootStep;
    public AudioEffectSO AESO_HowlendFlaskThrow;
    public AudioEffectSO AESO_HowlendFlaskThrowPerfect;
    public AudioEffectSO AESO_HowlendFlaskStartCharge;
    public AudioEffectSO AESO_HowlendCharging;
    public AudioSource ChargeSource;

    bool ExitCharging = false;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    #region Howlend Audio

    public void PlayHowlendFootStep()
    {
        AESO_HowlendFootStep.Play();
    }

    public void PlayChargeAudio()
    {
        AESO_HowlendCharging.Play(ChargeSource, false);
    }

    public void StopChargeAudio()
    {
        Debug.Log("Stopping Audio");
        //ChargeSource.Stop();
        StartCoroutine(AudioFade(ChargeSource, 0.2f));
    }

    IEnumerator AudioFade(AudioSource _source, float _duration)
    {
        float StartingVolume = _source.volume;

        for (float i = 0; i < _duration; i+= Time.deltaTime)
        {
            if (GetComponent<Scr_Character_Throw>().PlayerThrowState == Scr_Character_Throw.ThrowState.Charging)
            {
                break;
            }
            else
            {
                _source.volume = Mathf.Lerp(StartingVolume, 0, i / _duration);
            }
            yield return null;
        }
        _source.Stop();
    }

    public void PlayerHowlendFlaskThrow(bool _PerfectShot)
    {
        if (_PerfectShot)
        {
            AESO_HowlendFlaskThrowPerfect.Play();
        }
        else
        {
            //Debug.Log("Play howlend flask throw");
            AESO_HowlendFlaskThrow.Play();
        }
        

    }

    public void PlayHowlendFlaskChargeStart()
    {
        AESO_HowlendFlaskStartCharge.Play();
    }

    #endregion
}
