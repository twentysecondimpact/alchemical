﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_AudioManager : MonoBehaviour
{
    public static Scr_AudioManager inst;

    public AudioSource CurrentBGMSource;

    public float MaxBGMVolume;
    public float FadeInSpeed;
    public float FadeOutSpeed;

    private void Awake()
    {
        inst = this;
    }

    // Use this for initialization
    void Start () {
        CurrentBGMSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
