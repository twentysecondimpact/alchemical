﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_BGM_Audio_Trigger : MonoBehaviour
{
    public AudioSource Source;
    public bool RestartsOnTrigger;

    private void Awake()
    {
        Source = GetComponent<AudioSource>();
    }

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Scr_AudioManager.inst.CurrentBGMSource == Source)
        {
            Source.volume = Mathf.Clamp(Source.volume + (Scr_AudioManager.inst.FadeInSpeed * Time.deltaTime), 0, Scr_AudioManager.inst.MaxBGMVolume);
        }
        else
        {
            Source.volume = Mathf.Clamp(Source.volume - (Scr_AudioManager.inst.FadeOutSpeed * Time.deltaTime), 0, Scr_AudioManager.inst.MaxBGMVolume);
        }
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            if(Scr_AudioManager.inst.CurrentBGMSource != Source)
            {
                if (RestartsOnTrigger)
                {
                    Source.Stop();
                    Source.Play();
                }


                Scr_AudioManager.inst.CurrentBGMSource = Source;
            }
            
            
            
        }
    }
}
