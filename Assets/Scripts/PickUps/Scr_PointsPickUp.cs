﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_PointsPickUp : MonoBehaviour
{
    public Rigidbody rb;

    public AudioEffectSO PickUpAudio;

    public GameObject ParticleEffect;

    public float GravityRange;
    public float GravityForce;
    public AnimationCurve GravityCurve;

    public bool Triggered;
    public float TimeTriggered;

    public float VerticleAnchorOffset;

    public Vector3 MinimumVelocity;
    public Vector3 MaximumVelocity;

    public float extraGravity;

    public float LaunchForce;

    public Vector3 Velocity;

    public float ImmuneTime;
    public float ImmuneTimer;

    public bool SpawnedFromEnemy;

    public float GroundedHeight;

    public bool UseRigidBody;

    public TooltipObject Tooltip;

    // Use this for initialization
    void Start ()
    {
        rb = transform.GetComponent<Rigidbody>();

        if(!SpawnedFromEnemy)
        {
            UseRigidBody = false;
        }
        else
        {
            //if you were spwaned by enemy, allpy playr created tag
            this.gameObject.tag = "PlayerMade";


            //When spawned from enemy, add right to syls list
            if (!Scr_Character_Components.inst.character_Movement.Syl.GetComponent<Scr_Syl_Movement>().TargetPickups.Contains(this.gameObject) && Triggered == false)
            {
                Triggered = true;
                Scr_Character_Components.inst.character_Movement.Syl.GetComponent<Scr_Syl_Movement>().AddTargetPickUp(this.gameObject);
            }
        }

        if (UseRigidBody)
        {
            //Apply initial velocity
            rb.velocity += new Vector3(Random.Range(MinimumVelocity.x, MaximumVelocity.x), Random.Range(MinimumVelocity.y, MaximumVelocity.y), Random.Range(MinimumVelocity.z, MaximumVelocity.z));
        }
        else
        {
            rb.isKinematic = true;
            if(SpawnedFromEnemy)
            {
                //Add the initial velocity
                //Velocity += new Vector3(Random.Range(MinimumVelocity.x, MaximumVelocity.x), Random.Range(MinimumVelocity.y, MaximumVelocity.y), Random.Range(MinimumVelocity.z, MaximumVelocity.z));

            }

        }

        ImmuneTimer = ImmuneTime;
    }
	
	// Update is called once per frame
	void Update ()
    {
        //Get direction vector
        Vector3 Direction = transform.position - new Vector3(Scr_Character_Components.inst.transform.position.x, Scr_Character_Components.inst.transform.position.y + VerticleAnchorOffset, Scr_Character_Components.inst.transform.position.z);

        //Get absolute distance
        float distance = Mathf.Abs(Direction.magnitude);

        //f distance is within ramnge
        if(distance < Scr_Character_Components.inst.character_Movement.Syl.GetComponent<Scr_Syl_Movement>().PickUpRange && Triggered == false)
        {
            Triggered = true;
            Scr_Character_Components.inst.character_Movement.Syl.GetComponent<Scr_Syl_Movement>().AddTargetPickUp(this.gameObject);
        }

        if (Triggered)
        {
            float distanceFromSyl = Vector3.Distance(this.transform.position, Scr_Character_Components.inst.character_Movement.Syl.transform.position);
            if (distanceFromSyl < GravityRange)
            {
                transform.position += (Scr_Character_Components.inst.character_Movement.Syl.transform.position - transform.position) * GravityCurve.Evaluate(distanceFromSyl / GravityRange) * GravityForce * Time.deltaTime;
            }
        }
        
        //
    }

    public void LaunchEye()
    {
        rb = transform.GetComponent<Rigidbody>();
        rb.velocity += new Vector3(Random.Range(MinimumVelocity.x, MaximumVelocity.x), Random.Range(MinimumVelocity.y, MaximumVelocity.y), Random.Range(MinimumVelocity.z, MaximumVelocity.z)) * LaunchForce;
    }

    public void Activate()
    {

        //Notify the player to reduce ulti Cooldown
        Scr_Character_Components.inst.character_Throw.UpdateUltimateCooldowns(1);

        if (Scr_Character_Components.inst.character_Movement.Syl.GetComponent<Scr_Syl_Movement>().TargetPickups.Contains(this.gameObject))
        {
            Scr_Character_Components.inst.character_Movement.Syl.GetComponent<Scr_Syl_Movement>().TargetPickups.Remove(this.gameObject);
        }

        PickUpAudio.Play();

        Scr_GameDirector.inst.scoreSystem.GainPoints(1);
        Scr_Character_Components.inst.character_Health.AddHealth();

        GameObject PointPickUpParticles = GameObject.Instantiate(ParticleEffect, this.transform.position, Quaternion.identity);
        //Scr_GameDirector.inst.scoreSystem.co
        //Scr_GameDirector.inst.PointPickUpsCollected++;

        if(SpawnedFromEnemy)
        {
            Destroy(this.gameObject);
        }
        else
        {
            gameObject.SetActive(false);// Destroy(this.gameObject);
        }

        Tooltip.Activate();


    }

    private void FixedUpdate()
    {
        rb.velocity += new Vector3(0, -extraGravity, 0);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == 11)
        {
            //Grounded = true;
        }
        if (other.tag == "Player")
        {
            //Activate();
        }
        //else if(other.name == "Syl")
        //{
        //    Activate();
        //}
    }

    private void OnEnable()
    {
        Triggered = false;
    }
}
