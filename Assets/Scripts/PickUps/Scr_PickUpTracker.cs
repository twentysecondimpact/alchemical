﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Scr_PickUpTracker : MonoBehaviour {

    public List<PickupListEntry> PointPickups;

    public static Scr_PickUpTracker inst;

    private void Awake()
    {
        inst = this;
    }

    // Use this for initialization
    void Start()
    {
        CreateInitialList();

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void CreateInitialList()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).gameObject.activeSelf)
            {
                PointPickups.Add(new PickupListEntry(transform.GetChild(i).gameObject));
            }
        }
    }

    public void UpdatePickupList()
    {
        for (int i = 0; i < PointPickups.Count; i++)
        {
            //If the enemy is disabeled upon saving, it stays that way
            if (!PointPickups[i].PickupObject.activeSelf)
            {
                PointPickups[i].Alive = false;
            }
        }
    }

    public void LoadPickupsFromList()
    {
        for (int i = 0; i < PointPickups.Count; i++)
        {
            //If the enemy has been destroyed but its state hasnt been saved, spawn another one
            if (PointPickups[i].Alive && PointPickups[i].PickupObject.activeSelf == false)
            {
                PointPickups[i].Reset();
            }
        }
    }

    [Serializable]
    public class PickupListEntry
    {
        public GameObject PickupObject;
        public Vector3 SpawningPosition;
        public Vector3 SpawningScale;
        public Quaternion SpawningRotation;

        public bool Alive;


        public PickupListEntry(GameObject _pickupObject)
        {
            PickupObject = _pickupObject;
            SpawningPosition = _pickupObject.transform.position;
            SpawningScale = _pickupObject.transform.localScale;
            SpawningRotation = _pickupObject.transform.rotation;
            Alive = true;
        }

        public void Reset()
        {
            PickupObject.SetActive(true);
            PickupObject.transform.position = SpawningPosition;
            PickupObject.transform.localScale = SpawningScale;
            PickupObject.transform.rotation = SpawningRotation;
        }
    }
}
