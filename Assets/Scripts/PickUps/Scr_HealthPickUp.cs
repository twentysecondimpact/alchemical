﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_HealthPickUp : MonoBehaviour
{
    public AudioEffectSO PickUpAudio;

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    void Activate()
    {
        PickUpAudio.Play();
        Scr_Character_Components.inst.character_Health.TakeDamage(-2);
        Destroy(this.gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Hit something");
        if (other.tag == "Player")
        {
            //Debug.Log("Hit Player");
            if(Scr_Character_Components.inst.character_Health.Health < Scr_Character_Components.inst.character_Health.BaseHealth)
            {
                //Debug.Log("Activating HP");
                Activate();
            }
        }
    }
}
