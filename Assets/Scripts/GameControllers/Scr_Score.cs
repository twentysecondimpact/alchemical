﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_Score : MonoBehaviour
{

    //Your current score form combos
    public int CurrentScore;
    //Used to track your current COmbo
    public int ComboPool;
    //Your current combo multiplier
    public int ComboMultiplier;
    //The amount of points given per hit of damage dealt
    public int DamageToScoreFactor;

    //The amount of time you have before the combo consolidates
    public float ComboTimeLength;
    //Tracks current combo time
    public float ComboTimer;

    //The starting amount of points you get from time
    public float TimeBonusBase;
    //How many points are removed per second
    public float TimeBonusFactor;

    //The starting amount of point you get from efficiency
    public float EfficiencyBonusBase;
    //How many points are taken off per flask used
    public float EfficiencyBonusFactor;

    public int SavedScore;

    //The amount of points 
    //public int BonusPickUpMultiplier;



    

	// Use this for initialization
	void Start ()
    {
    }
	
	// Update is called once per frame
	void Update ()
    {
        if(ComboTimer > 0)
        {
            ComboTimer -= Time.deltaTime;
        }
        

        if(ComboTimer <= 0 && ComboPool > 0)
        {
            EndCombo();
        }
	}

    public void SaveScore()
    {
        //Ends the combo so it can be saved
        EndCombo();
        SavedScore = CurrentScore;
    }

    public void LoadScore()
    {
        //Ends the combo so it can be overridden
        EndCombo();
        CurrentScore = SavedScore;
    }

    public void EndCombo()
    {
        if (ComboMultiplier == 0)
        {
            ComboMultiplier = 1;
        }

        CurrentScore += ComboPool * ComboMultiplier;
        ComboMultiplier = 0;
        ComboPool = 0;
    }

    public void GainPoints(int _Points)
    {
        ComboPool += _Points * DamageToScoreFactor;
        ComboTimer = ComboTimeLength;
    }

    public void IncreaseMultiplier()
    {
        ComboMultiplier += 1;
    }

    public void ResetScore()
    {
        CurrentScore = 0;
        ComboPool = 0;
        ComboMultiplier = 0;
        ComboTimer = 0;
    }
}
