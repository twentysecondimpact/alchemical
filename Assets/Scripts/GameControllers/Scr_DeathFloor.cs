﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_DeathFloor : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            Scr_Character_Components.inst.character_Health.Die();
        }
    }
}
