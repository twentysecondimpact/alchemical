﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Scr_SceneManager : MonoBehaviour
{
    public string startingLevel;

    public bool DebugLoading;
    public List<string> DebugLoadScenes;

	// Use this for initialization
	void Start ()
    {
        if(startingLevel != "0")
        {
            LoadLevel(startingLevel);
        }

        //Debug level Loading
        if (DebugLoading)
        {
            for (int i = 0; i < DebugLoadScenes.Count; i++)
            {
                SceneManager.LoadSceneAsync(DebugLoadScenes[i], LoadSceneMode.Additive);
            }
            
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void LoadLevel(string _LevelName)
    {
        //If the current scene is already loaded, unload it
        if(SceneManager.GetSceneByName(_LevelName).isLoaded)
        {
            SceneManager.UnloadSceneAsync(_LevelName);
        }

        //The load it again
        SceneManager.LoadSceneAsync(_LevelName,LoadSceneMode.Additive);
    }

    public void UnloadLevel(string _LevelName)
    {
        //If the current scene is already loaded, unload it
        if (SceneManager.GetSceneByName(_LevelName).isLoaded)
        {
            SceneManager.UnloadSceneAsync(_LevelName);
        }
    }

    public void ReloadCurrentLevel()
    {
        //string LevelName = Scr_GameDirector.inst.currentLevel.LevelSceneName;
        //SceneManager.UnloadSceneAsync(Scr_GameDirector.inst.currentLevel.LevelSceneName);
        //SceneManager.LoadSceneAsync(LevelName, LoadSceneMode.Additive);

        SceneManager.LoadScene("GameplayScene");
    }

    public void LoadScene(string _SceneName)
    {
        SceneManager.LoadScene(_SceneName);
    }
}
