﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_MenuManager : MonoBehaviour
{
    public List<GameObject> MenuPanels;

    public AudioEffectSO AESO_ButtonClick;
    public AudioEffectSO AESO_MenuToggle;

    public enum Menu
    {
        Main = 0,
        Pause = 1,
        LevelOver = 2,
        LevelSelect = 3,
        GameOver = 4
    }

    public Menu menu;

    GameObject currentMenu;

    public Scr_HintToolTip HintToolTip;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void LoadMenu(Menu _menu)
    {
        if(currentMenu != null)
        {
            currentMenu.SetActive(false);
        }

        currentMenu = MenuPanels[(int)_menu];

        MenuPanels[(int)_menu].SetActive(true);

    }

    public void UnloadMenu()
    {
        if (currentMenu != null)
        {
            currentMenu.SetActive(false);
        }
    }

    public void PlayClickAudio()
    {
        AESO_ButtonClick.Play();
    }

    public void PlayMenuToggleAudio()
    {
        AESO_MenuToggle.Play();
    }

    public void ToggleLeftDrag()
    {
        Scr_FollowCamera.inst.LeftClickDrag = !Scr_FollowCamera.inst.LeftClickDrag;
    }
}
