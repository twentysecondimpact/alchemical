﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.AI;
using UnityEngine.UI;

public class Scr_GameDirector : MonoBehaviour
{
    public static Scr_GameDirector inst;

    public Texture2D MouseUpCursor;
    public Texture2D MouseDownCursor;

    public bool InCombat;
    public List<Scr_Enemy> EnemiesInCombat;

    public Scr_Score scoreSystem;

    public Scr_SceneManager sceneManager;
    public Scr_LevelManager currentLevel;
    public Scr_MenuManager menuManager;

    public int Deaths;

    public Image IntroImage;
    public Image OutroImage;

    public enum GameState
    {
        InGame,
        PauseMenu,
        GameOver,
        Dialogue
    }

    public GameState gameState;

    public float LevelTimer;

    public int FlasksUsed;

    //Super fast placeholder for game over screen
    public GameObject EndGamePanel;
    //The end level panel
    public GameObject EndLevelPanel;

    public int CrystalsCollected;

    public static bool CharacterAttackable(NavMeshAgent _Agent, Vector3 _CharPos)
    {
        if (_Agent.gameObject.GetComponent<Scr_Enemy_Health>().Alive)
        {
            NavMeshPath newPath = new NavMeshPath();
            _Agent.CalculatePath(_CharPos, newPath);
            //If there is a path
            if (newPath.status == NavMeshPathStatus.PathComplete)
            {
                return true;
            }
        }

        return false;
    }

    public int PointPickUpsCollected;

    public string CurrentCheckpointLevel;

    public float AggroDelay;
    public float PassiveDelay;

    public float TimeLerpRate;
    public float TargetTimeScale;

    public float TimeSlowedSpeed;

    public float FadeInDelay;
    public Scr_UI_Fader fader;

    public TooltipObject TTSO_StartingToolTip;

    public bool ContinueFromStartImage;
    public bool ContinueFromEndImage;

    private void Awake()
    {
        inst = this;
        sceneManager = GetComponent<Scr_SceneManager>();
        menuManager = GetComponent<Scr_MenuManager>();
 
        StartCoroutine(DelayFadeIn(FadeInDelay));

        Physics.IgnoreLayerCollision(2, 15, true);
        //Enemies and pickups
        Physics.IgnoreLayerCollision(10, 16, true);
        //Ignore raycast and Pickups
        Physics.IgnoreLayerCollision(2, 16, true);
    }

    // Use this for initialization
    void Start ()
    {
        InCombat = false;

        gameState = GameState.Dialogue;

        //Set the cursor image
        Cursor.SetCursor(MouseUpCursor, new Vector2(0, 0), CursorMode.Auto);

        
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (gameState == GameState.InGame)
            {
                PauseGame();
            }
            else if(gameState == GameState.PauseMenu)
            {
                UnpauseGame();
            }
            
        }

        if(Input.GetMouseButtonDown(0))
        {
            //Debug.Log("Change cursor to down");
            //Set the cursor image
            Cursor.SetCursor(MouseDownCursor, new Vector2(0, 0), CursorMode.Auto);
        }

        if (Input.GetMouseButtonUp(0))
        {
            //Debug.Log("Change cursor to up");
            //Set the cursor image
            Cursor.SetCursor(MouseUpCursor, new Vector2(0, 0), CursorMode.Auto);
        }

        if(gameState == GameState.InGame)
        {
            LevelTimer += Time.deltaTime;
        }

        if (Input.GetKeyDown(KeyCode.O))
        {
            //SaveGame();
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            //StartRestartAtCheckpoint();
        }

        //Warps time based on state
        Time.timeScale = Mathf.Lerp(Time.timeScale, TargetTimeScale, TimeLerpRate * Time.unscaledDeltaTime);
    }
    private void LateUpdate()
    {
        //if(StartingToolTip.ShowToolTip)
        //{
        //    StartCoroutine(StartingToolTipDisplay());
        //    StartingToolTip.ShowToolTip = false;
        //}

    }

    public void EndGame()
    {
        //Unlocks the camera if you were dragging
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        //Sets stats
        gameState = GameState.GameOver;

        //Loads end game screen
        menuManager.LoadMenu(Scr_MenuManager.Menu.GameOver);
       
        //EndGamePanel.SetActive(true);
    }

    public void PauseGame()
    {
        //Scr_HintToolTip.inst.Deactive();
        gameState = GameState.PauseMenu;
        menuManager.LoadMenu(Scr_MenuManager.Menu.Pause);
        menuManager.PlayMenuToggleAudio();
    }

    public void UnpauseGame()
    {
        gameState = GameState.InGame;
        menuManager.UnloadMenu();
        menuManager.PlayMenuToggleAudio();
    }

    public void EndLevel()
    {
        StartCoroutine(EndGameSequence());




        ////Loads end level screen
        //menuManager.LoadMenu(Scr_MenuManager.Menu.LevelOver);

        ////Generate the score;
        //EndLevelPanel.GetComponent<Scr_UI_LevelOverPanel>().CalculateLevelScore();
    }

    public IEnumerator EndGameSequence()
    {
        //Fade to black


        //Unlocks the camera if you were dragging
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        //Sets stats
        gameState = GameState.GameOver;

        fader.Activate(1);

        yield return new WaitForSeconds(2);

        //Load end image
        OutroImage.gameObject.SetActive(true);

        //Fade in to reveal image
        fader.Activate(-1);

        //Wait for continue
        while(ContinueFromEndImage == false)
        {
            yield return null;
        }

        //Fade out
        fader.Activate(1);

        yield return new WaitForSeconds(2);

        ContinueFromEndImage = false;

        LoadMainMenu();
    }

    public void StartRestartGame()
    {
        if (!fader.Active)
        {
            StartCoroutine(FadeRestartGame());
        }
    }

    public void RestartGame()
    {
        gameState = GameState.InGame;

        //Reset the Level Scene, the level resets the character position
        sceneManager.ReloadCurrentLevel();

        //Reset the Character
        Scr_Character_Components.inst.ResetCharacter();

        //reset the current level
        currentLevel.SetUpLevel();

        //Reset the score
        scoreSystem.ResetScore();

       

        //Reset the UI
        menuManager.UnloadMenu();

        EndGamePanel.SetActive(false);
    }

    public void StartRestartAtCheckpoint()
    {
        if (!fader.Active)
        {
            //Sets stats
            gameState = GameState.GameOver;
            Scr_HintToolTip.inst.Deactive();
            StartCoroutine(FadeRestartAtCheckpoint());
        }
        else
        {
            Debug.Log("Fader is disabeled you dunce, change this shitty system already");
        }
    }

    public void RestartAtCheckpoint()
    {

        Debug.Log("Laoding Game");

        gameState = GameState.InGame;

        //Reset the Level Scene, the level resets the character position
        //sceneManager.LoadLevel(CurrentCheckpointLevel);

        //Reset the Character
        Scr_Character_Components.inst.ResetCharacter();

        //reset the current level
        currentLevel.SetUpLevel();

        //Reset the score
        scoreSystem.ResetScore();

        //Reset the UI
        menuManager.UnloadMenu();

        //Load the flasks
        Scr_Character_Components.inst.character_Throw.LoadFlaskPrefabs();

        //Load the enemies
        Scr_EnemyTracker.inst.LoadEnemiesFromList();

        //Load flask pickups
        Scr_FlaskTracker.inst.LoadFlasksFromList();

        //Load pickups
        Scr_PickUpTracker.inst.LoadPickupsFromList();

        //Load the score
        scoreSystem.LoadScore();

        EndGamePanel.SetActive(false);
        
    }

    public void StartLoadMainMenu()
    {
        if (!fader.Active)
        {
            StartCoroutine(FadeMainMenu());
        }
    }

    public void LoadMainMenu()
    {
        //menuManager.LoadMenu(Scr_MenuManager.Menu.Main);
        sceneManager.LoadScene("MainMenu");
    }

    public void SaveGame()
    {
        //Save the player position
        //update the position of the spawn point
        Scr_GameDirector.inst.currentLevel.PlayerSpawn.position = Scr_Character_Components.inst.transform.position;// transform.position;
        //Rotate 180 from the tomb stone, makes you face the stone
        Scr_GameDirector.inst.currentLevel.PlayerSpawn.localRotation = Scr_Character_Components.inst.transform.rotation;// transform.localRotation * Quaternion.Euler(0, 180, 0);


        //Save the enemies state
        Scr_EnemyTracker.inst.UpdateEnemyList();

        //Save the flask state
        Scr_Character_Components.inst.character_Throw.SaveFlaskPrefabs();

        //Save the pickup state
        Scr_PickUpTracker.inst.UpdatePickupList();

        //Save the flask pickup state
        Scr_FlaskTracker.inst.UpdateFlaskList();

        //Save the score
        scoreSystem.SaveScore();

        //Save player Health
        Scr_Character_Components.inst.character_Health.SaveCharacterHealth();
    }

    public void UpdateEnemiesInCombat(bool _remove, Scr_Enemy _enemy)
    {
        //int StartingCount = EnemiesInCombat.Count;
        bool StartingCombat = InCombat;

        if (_remove == true)
        {
            if (EnemiesInCombat.Contains(_enemy))
            {
                EnemiesInCombat.Remove(_enemy);
            }
        }
        else
        {
            if(EnemiesInCombat.Contains(_enemy) == false)
            {
                EnemiesInCombat.Add(_enemy);
            }
            
        }

        

        Scr_Enemy[] allEnemies = GameObject.FindObjectsOfType<Scr_Enemy>();
        int EnemiesDisabeled = 0;

        foreach(Scr_Enemy enemy in allEnemies)
        {
            if(enemy.DisabledTimer > 0)
            {
                EnemiesDisabeled++;
            }
        }

        if(EnemiesDisabeled > 0)
        {
            InCombat = true;
        }
        else if (EnemiesInCombat.Count > 0)
        {
            InCombat = true;
        }
        else
        {
            InCombat = false;
        }




        //int ModifiedCount = EnemiesInCombat.Count;

        //if (StartingCount == 0 && ModifiedCount > 0)
        //{
        //    Debug.Log("Entered Combat");
        //}

        //if (StartingCount > 0 && ModifiedCount == 0)
        //{
        //    Debug.Log("Exiting Combat");
        //    Scr_Character_Components.inst.character_Throw.ResetCooldowns();
        //}


        if (StartingCombat == false && InCombat == true)
        {
            //Debug.Log("Entered Combat");
        }

        if (StartingCombat == true && InCombat == false)
        {
            //Debug.Log("Exiting Combat");
            //Scr_Character_Components.inst.character_Throw.ResetCooldowns();
        }
    }

    public void LoadLevel(string _LevelSceneName)
    {
        sceneManager.LoadLevel(_LevelSceneName);
    }

    public void LoadLevelSelect()
    {
        menuManager.LoadMenu(Scr_MenuManager.Menu.LevelSelect);
    }

    IEnumerator DelayFadeIn(float _delay)
    {
        //Initial delay after the object loads in
        for (float i = 0; i < _delay; i+= Time.deltaTime)
        {
            yield return null;
        }

        //Fades outthe fader revealing the intro image
        fader.Activate(-1);

        //Waits for the player to continue past the 
        while(ContinueFromStartImage == false)
        {
            yield return null;
        }

        ContinueFromStartImage = false;

        //Fades in the fader
        fader.Activate(1);

        yield return new WaitForSeconds(2);
        IntroImage.gameObject.SetActive(false);

        fader.Activate(-1);

        gameState = GameState.InGame;

        TTSO_StartingToolTip.Activate();
    }

    IEnumerator FadeMainMenu()
    {
        //Set the fader to fade out
        fader.Activate(1);

        for (int i = 0; i < 10000; i++)
        {
            //Wait till the fader has hit 1
            if (fader.Active == false)
            {
                break;
            }
            yield return null;
        }

        //elay after full fade
        for (float i = 0; i < 0.2f; i += Time.deltaTime)
        {
            yield return null;
        }
        //Load the game
        LoadMainMenu();
        //SceneManager.LoadScene("GameplayScene");
    }

    IEnumerator FadeRestartGame()
    {
        //Set the fader to fade out
        fader.Activate(1);

        for (int i = 0; i < 10000; i++)
        {
            //Wait till the fader has hit 1
            if (fader.Active == false)
            {
                break;
            }
            yield return null;
        }

        //elay after full fade
        for (float i = 0; i < 0.2f; i += Time.deltaTime)
        {
            yield return null;
        }

        RestartGame();
        //Load the game
        //SceneManager.LoadScene("GameplayScene");
    }

    IEnumerator FadeRestartAtCheckpoint()
    {
        //Set the fader to fade out
        fader.Activate(1);

        for (int i = 0; i < 10000; i++)
        {
            //Wait till the fader has hit 1
            if (fader.Active == false)
            {
                break;
            }
            yield return null;
        }
        //elay after full fade
        for (float i = 0; i < 0.2f; i += Time.deltaTime)
        {
            yield return null;
        }

        RestartAtCheckpoint();
        //Load the game
        //SceneManager.LoadScene("GameplayScene");
    }

    IEnumerator StartingToolTipDisplay()
    {
        yield return new WaitForSeconds(0.2f);
        //Send out the initial tooltip
        //Scr_HintToolTip.inst.Activate(StartingToolTip);
    }

    public void StopTime()
    {
        TargetTimeScale = TimeSlowedSpeed;
    }

    public void StartTime()
    {
        TargetTimeScale = 1;
    }

    public void FadeOutIntroPanel()
    {
        ContinueFromStartImage = true;
    }

    public void FadeOutOutroPanel()
    {
        ContinueFromEndImage = true;
    }
}
