﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_TerrainPlayTrigger : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
        if(Scr_GameDirector.inst != null)
        {
            Scr_GameDirector.inst.RestartAtCheckpoint();
        }
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
