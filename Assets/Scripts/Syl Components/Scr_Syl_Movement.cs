﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_Syl_Movement : MonoBehaviour
{
    public Transform SylAnchor;
    public float FollowLerpSpeed;
    public float TurnLerpSpeed;

    public Vector3 PreviousPosition;
    public Vector3 Velocity;

    public Vector3 TargetPosition;

    public List<GameObject> TargetPickups;
    public List<GameObject> TargetEnemies;

    public float TurnVelocityThreshold;

    public float SylFloatNoise;

    public GameObject SylModel;

    public float CurrentSpeed;
    public float Acceleration;
    public float Deceleration;



    public float TurnSpeed;
    public float BaseTurnSpeed;
    public float TurnSpeedAcceleration;

    public float CurrentAttackingSpeed;
    public float AttackingTurnSpeed;
    public float AttackingBaseTurnSpeed;
    public float AttackingTurnSpeedAcceleration;

    public bool FollowingCharacter;

    public float AnchorRadius;

    public float PickUpRange;


    public float PickUpRadius;
    public float BasePickUpRadius;
    public float PickUpRadiusIncrease;

    public bool Attacking;

    public float KnockUpForce;
    public Vector3 KnockUpTorque;

    public float HitForce;

    public float KnockUpDampening;

    public float SylAccelerationMultiplier;
    public float SylMaximumAccelerationDistance;
    public float DistancePercentage;
    // Use this for initialization
    void Start ()
    {
        PickUpRadius = BasePickUpRadius;
        TurnSpeed = BaseTurnSpeed;
        AttackingTurnSpeed = AttackingBaseTurnSpeed;
        PreviousPosition = transform.position;
    }
	
	// Update is called once per frame
	void Update ()
    {

    }

    private void FixedUpdate()
    {
        
    }

    public void AddTargetPickUp(GameObject _TargetPickUp)
    {

        TargetPickups.Add(_TargetPickUp);
    }

    public void StartAttack()
    {
        //Debug.Log("Starting attack");

        for (int i = 0; i < TargetEnemies.Count; i++)
        {
            //Debug.Log(TargetEnemies[i].name);

            Scr_Enemy_Health enemy = TargetEnemies[i].GetComponent<Scr_Enemy_Health>();

            enemy.DisableInvulnerable();

            enemy.Alive = false;
            //Debug.Log(gameObject.name + " Die");

            //Remove the disable before dying
            enemy.enemyController.DisabledTimer = 0;

            //Turn off the collider
            enemy.GetComponent<Collider>().enabled = false;

            //Turns off the movement component
            enemy.GetComponent<Scr_Enemy_Movement>().enabled = false;

            

            Scr_EnemyHover hover = enemy.gameObject.AddComponent<Scr_EnemyHover>();
            hover.KnockUp(KnockUpForce, KnockUpTorque, KnockUpDampening);

            
        }

        Attacking = true;

    }

    private void LateUpdate()
    {
        //Bobbing
        SylModel.transform.localPosition += new Vector3(Mathf.Sin(Time.time), Mathf.Sin(Time.time * 1.8f), 0) * SylFloatNoise * Time.deltaTime;

        //Also turn on the trail rendered
        transform.GetChild(0).GetChild(0).GetComponent<TrailRenderer>().enabled = true;

        if (Attacking)
        {
            if (TargetEnemies.Count != 0)
            {
                FollowingCharacter = false;
                TargetPosition = TargetEnemies[0].transform.position;
                transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(TargetPosition - transform.position), AttackingTurnSpeed * Time.deltaTime);
                transform.position += transform.forward * CurrentAttackingSpeed * Time.deltaTime;
                AttackingTurnSpeed += AttackingTurnSpeedAcceleration * Time.deltaTime;
                PickUpRadius += PickUpRadiusIncrease * Time.deltaTime;

                if (Vector3.Distance(TargetPosition, transform.position) < PickUpRadius)
                {
                    TargetEnemies[0].GetComponent<Scr_Enemy_Health>().DisableInvulnerable();
                    TargetEnemies[0].GetComponent<Scr_Enemy_Health>().TakeDamage(1);
                    PickUpRadius = BasePickUpRadius;

                    if(TargetEnemies[0].GetComponent<Scr_Enemy_Health>().Health > 0)
                    {
                        GameObject enemyToReturn = TargetEnemies[0];
                        TargetEnemies.Remove(TargetEnemies[0]);
                        TargetEnemies.Add(enemyToReturn);

                        //enemyToReturn.GetComponent<Rigidbody>().AddForceAtPosition(this.transform.forward * HitForce, this.transform.position);

                        //enemyToReturn.GetComponent<Rigidbody>().AddRelativeTorque(new Vector3(0, HitForce, 0));

                        //enemyToReturn.GetComponent<Rigidbody>().AddTorque(new Vector3(Random.Range(-KnockUpTorque.x, KnockUpTorque.x), Random.Range(-KnockUpTorque.y, KnockUpTorque.y), Random.Range(-KnockUpTorque.z, KnockUpTorque.z)));
                    }
                    else
                    {
                        TargetEnemies.Remove(TargetEnemies[0]);
                    }
                    
                }
            }
            else
            {
                Attacking = false;
                AttackingTurnSpeed = AttackingBaseTurnSpeed;
            }

            if (TargetPickups.Count != 0)
            {
                TargetPickups[0].GetComponent<Scr_PointsPickUp>().Activate();
            }

        }
        else
        {

            #region UpdatePosition
            //TargetPosition 
            if (TargetPickups.Count != 0)
            {

                DistancePercentage = Mathf.Clamp01((TargetPosition - transform.position).magnitude / SylMaximumAccelerationDistance);

                FollowingCharacter = false;
                TargetPosition = TargetPickups[0].transform.position;
                transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(TargetPosition - transform.position), TurnSpeed * Time.deltaTime);

                transform.position += transform.forward * CurrentSpeed * Time.deltaTime * ((1 + (DistancePercentage * SylAccelerationMultiplier)));

                TurnSpeed += TurnSpeedAcceleration * Time.deltaTime;
                PickUpRadius += PickUpRadiusIncrease * Time.deltaTime;

                if (Vector3.Distance(TargetPosition, transform.position) < PickUpRadius)
                {
                    TargetPickups[0].GetComponent<Scr_PointsPickUp>().Activate();
                    PickUpRadius = BasePickUpRadius;
                }
            }
            else
            {
                TargetPosition = SylAnchor.position;


                if (Vector3.Distance(transform.position, SylAnchor.position) <= AnchorRadius)
                {
                    FollowingCharacter = true;
                }

                //If sly hits the anchhor switch back to following
                if (FollowingCharacter)
                {
                    TurnSpeed = BaseTurnSpeed;
                    PickUpRadius = BasePickUpRadius;
                    transform.position = Vector3.LerpUnclamped(transform.position, TargetPosition, FollowLerpSpeed * Time.deltaTime);
                }
                else
                {
                    //Follow position
                    transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(TargetPosition - transform.position), TurnSpeed * Time.deltaTime);
                    transform.position += transform.forward * CurrentSpeed * Time.deltaTime;
                }

            }
            #endregion

            #region UpdateRotation

            //If Sly is chasing orbs
            if (TargetPickups.Count != 0)
            {

            }
            //If Syl is following the player
            else
            {
                //if the character isnt moving, recenter syl
                Velocity = transform.position - PreviousPosition;


                if (Velocity.magnitude <= TurnVelocityThreshold)// TurnVelocityThreshold)
                {
                    //Also turn off the trail rendered
                    transform.GetChild(0).GetChild(0).GetComponent<TrailRenderer>().enabled = false;

                    transform.rotation = Quaternion.Slerp(transform.rotation, Scr_Character_Components.inst.character_Movement.CharacterModelTransformKeyboard.transform.rotation, TurnLerpSpeed * Time.deltaTime);// Quaternion.LookRotation(Velocity);
                }
                //If you are moving quickly
                else
                {
                    transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(Velocity), 1);// TurnLerpSpeed * Time.deltaTime);
                }
            }

            #endregion



        }



        PreviousPosition = transform.position;
    }
}
