﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_PathNode : MonoBehaviour
{

    public Scr_PathNode[] NextNodes;

    public float MinWaitTime;
    public float MaxWaitTime;

    public int CurrentIndex;
	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Enemy")
        {
            if(other.GetComponent<Scr_Enemy_Movement>().aggroState == Scr_Enemy_Movement.AggroState.Passive)
            {
                float WaitTime = Random.Range((float)MinWaitTime, (float)MaxWaitTime);//Debug.Log("Enemy Hit");
                Debug.Log(WaitTime);
                StartCoroutine(SendToNext(WaitTime, other.GetComponent<Scr_Enemy_Movement>()));
            }
            
        }
    }

    IEnumerator SendToNext(float _WaitTime, Scr_Enemy_Movement _Enemy)
    {
        if(_WaitTime > 0)
        {
            _Enemy.ChangeSpeed(0);
        }
        
        //Debug.Log("starting coroutine");
        //Wait the instructed time
        yield return new WaitForSeconds(_WaitTime);


        if (_WaitTime > 0)
        {
            _Enemy.ChangeTurn(120, 900);
        }
        else
        {
            _Enemy.ChangeTurn(1, 40);
        }

        //int RandomNodeIndex = Random.Range(0, NextNodes.Length - 1);
        //Debug.Log("Sending unity away");

        if(_Enemy != null)
        {
            _Enemy.Patrolling = true;

            //Chenge the enemy target position
            _Enemy.ChangeSpeed(_Enemy.PatrollingSpeed);
            _Enemy.ChargeTargetPosition(NextNodes[CurrentIndex].transform.position);

            //Increase and loop index
            CurrentIndex++;
            if(CurrentIndex >= NextNodes.Length)
            {
                CurrentIndex = 0;
            }
        }
        
    }

    private void OnDrawGizmos()
    {
        if(NextNodes != null)
        {
            for (int i = 0; i < NextNodes.Length; i++)
            {
                if (NextNodes[i] != null)
                {
                    Gizmos.DrawLine(transform.position, NextNodes[i].transform.position);
                }
            }
        }
        
        
    }
}
