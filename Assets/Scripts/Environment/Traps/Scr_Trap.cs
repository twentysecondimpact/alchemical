﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_Trap : MonoBehaviour
{
    //The time it takes the trap to deactivate, it gives this info to the rune that activated it as well
    public float ResetTime;
    public float ResetTimer;
    public bool Activated;
    public bool StartActivated;

    // Use this for initialization
    protected virtual void Start()
    {
        if (StartActivated)
        {
            Activate();
        }
    }

    

    // Update is called once per frame
    protected virtual void Update()
    {
        if (Activated)
        {
            UpdateResetTimer();
        }

    }

    public void UpdateResetTimer()
    {
        if (ResetTimer > 0)
        {
            ResetTimer -= Time.deltaTime;
        }
        else
        {
            Deactivate();
        }
    }

    public virtual void Activate()
    {
        Activated = true;
        ResetTimer = ResetTime;
    }

    public virtual void Deactivate()
    {
        Activated = false;
    }


}
