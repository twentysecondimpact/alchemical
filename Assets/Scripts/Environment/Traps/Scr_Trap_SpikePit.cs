﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_Trap_SpikePit : Scr_Trap
{
    public Vector3 DeactivedPosition;
    public Vector3 ActivatedPositionDelta;

    public Vector3 TargetPosition;

    public float LerpRate;

    bool LateActivation = false;

    // Use this for initialization
    protected override void Start ()
    {
        DeactivedPosition = transform.position;

        base.Start();
        Deactivate();
        
	}
	
	// Update is called once per frame
	protected override void Update ()
    {
        base.Update();

        transform.position = Vector3.Lerp(transform.position, TargetPosition, LerpRate * Time.deltaTime);
        //Lerp between current and target position
	}

    public override void Activate()
    {
        base.Activate();
        TargetPosition = DeactivedPosition + ActivatedPositionDelta;
        //transform.position = TargetPosition;// Vector3.Lerp(transform.position, TargetPosition, LerpRate * Time.deltaTime);
        //Debug.Log("")

        LateActivation = true;

        //Checking if anythign is in range
        Collider[] collidersInRange = Physics.OverlapBox(transform.position + GetComponent<BoxCollider>().center, GetComponent<BoxCollider>().bounds.extents);
        foreach (Collider colliderInRange in collidersInRange)
        {
            Debug.Log("Collided with: " + colliderInRange.gameObject.name);
            if (colliderInRange.tag == "Enemy")
            {
                colliderInRange.GetComponent<Scr_Enemy_Health>().TakeDamage(99);
            }

            if (colliderInRange.tag == "Player")
            {
                colliderInRange.GetComponent<Scr_Character_Health>().TakeDamage(99);
            }

        }
    }

    public override void Deactivate()
    {
        base.Deactivate();
        TargetPosition = DeactivedPosition;
    }

    public void LateUpdate()
    {
        //if (LateActivation)
        //{
        //    //Checking if anythign is in range
        //    Collider[] collidersInRange = Physics.OverlapBox(this.transform.position, GetComponent<BoxCollider>().bounds.extents);
        //    foreach (Collider colliderInRange in collidersInRange)
        //    {
        //        Debug.Log("Collided with: " + colliderInRange.gameObject.name);
        //        if (colliderInRange.tag == "Enemy")
        //        {
        //            colliderInRange.GetComponent<Scr_Enemy_Health>().TakeDamage(99);
        //        }

        //    }

        //    LateActivation = false;
        //}
        
    }
}
