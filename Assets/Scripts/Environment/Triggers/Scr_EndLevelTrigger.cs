﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_EndLevelTrigger : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            Scr_GameDirector.inst.EndLevel();
        }
        
    }
}
