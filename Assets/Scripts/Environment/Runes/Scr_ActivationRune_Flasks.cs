﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_ActivationRune_Flasks : Scr_ActivationRune
{

    

	// Use this for initialization
	protected override void Start ()
    {
        base.Start();	
	}
	
	// Update is called once per frame
	protected override void Update ()
    {
        base.Update();
	}

    public override void Activate()
    {
        ShowToolTip = true;
        bool HasAllFlasks = true;

        GameObject[] PlayerFlasks = Scr_Character_Components.inst.character_Throw.FlaskPrefab;

        for (int i = 0; i < PlayerFlasks.Length; i++)
        {
            if(PlayerFlasks[i] == null)
            {
                HasAllFlasks = false;
            }
        }

        if (HasAllFlasks)
        {
            ShowToolTip = false;
            base.Activate();
        }
        else
        {
            Debug.Log("Not enough flasks");
        }
        
    }
}
