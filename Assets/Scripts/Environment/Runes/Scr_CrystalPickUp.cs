﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_CrystalPickUp : MonoBehaviour {

    public GameObject DisabledOnTrigger;
    public GameObject EnabledOnTrigger;

    public AudioEffectSO AESO_PickUp;

    public Vector3 RotationSpeed;
    public float HoverSpeed;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(RotationSpeed * Time.deltaTime);
        transform.position += new Vector3(0, Mathf.Sin(Time.deltaTime * HoverSpeed), 0);
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            AESO_PickUp.Play();

            Scr_GameDirector.inst.CrystalsCollected++;

            //Increments the objective tracker
            Scr_ObjectiveTracker.inst.UpdateObjectText(1);

            if(Scr_GameDirector.inst.CrystalsCollected >= 0)
            {
                if(EnabledOnTrigger != null)
                {
                    EnabledOnTrigger.SetActive(true);
                }
                
                if(DisabledOnTrigger != null)
                {
                    DisabledOnTrigger.SetActive(false);
                }
                
            }

            Destroy(this.gameObject);
        }
    }
}
