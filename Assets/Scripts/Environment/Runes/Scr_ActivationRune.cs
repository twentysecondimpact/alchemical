﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_ActivationRune : MonoBehaviour
{
    public string LevelSceneToLoad;
    public string LevelSceneToUnload;


    public bool Activated = false;

    public Scr_Trap[] ActivatedTraps;

    //Tooltip variables
    public bool ShowToolTip;
    public string ToolTipText;
    public Scr_HintToolTip Tooltip;
    public bool Active;

    public float FadePercent;
    public float FadeInSpeed;
    public float FadeOutSpeed;

    public AudioEffectSO AESO_TriggerAudio;

    //The time it takes for the rune to deactive, it grabs this from the trap it is activating
    public float ResetTime;

	// Use this for initialization
	protected virtual void Start ()
    {
        //Needs to go somewhere else but eh
        Tooltip = Scr_GameDirector.inst.menuManager.HintToolTip;
    }
	
	// Update is called once per frame
	protected virtual void Update ()
    {
        UpdateFadePercent();
    }

    public virtual void Activate()
    {
        if(Activated == false)
        {
            AESO_TriggerAudio.Play();

            foreach (Scr_Trap trap in ActivatedTraps)
            {
                trap.Activate();
                ResetTime = trap.ResetTime;
            }

            if (LevelSceneToLoad != "")
            {
                Scr_GameDirector.inst.sceneManager.LoadLevel(LevelSceneToLoad);
            }

            if (LevelSceneToUnload != "")
            {
                Scr_GameDirector.inst.sceneManager.UnloadLevel(LevelSceneToUnload);
            }

            //Increment the objective tracker
            Scr_ObjectiveTracker.inst.UpdateObjectText(1);
            Activated = true;
        }
        

    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Hit Something");
        if(other.tag == "Player")
        {
            //Debug.Log("Hit Player");
            Activate();
        }

        if (ShowToolTip)
        {
            if (other.tag == "Player")
            {
                Tooltip.TextMesh.text = ToolTipText;
                Tooltip.GetComponent<Scr_HintToolTip>().Active = true;
                Active = true;
            }
        }
        
    }

    private void OnTriggerExit(Collider other)
    {
        if (ShowToolTip)
        {
            if (other.tag == "Player")
            {
                Tooltip.GetComponent<Scr_HintToolTip>().Active = false;
                Active = false;
            }
        }
        
    }

    public void UpdateFadePercent()
    {
        if (Active)
        {
            FadePercent = Mathf.Clamp01(FadePercent + (FadeInSpeed * Time.deltaTime));
        }
        else
        {
            FadePercent = Mathf.Clamp01(FadePercent - (FadeOutSpeed * Time.deltaTime));
        }

        //Color GrabbedColor = TipGhost.GetComponent<MeshRenderer>().material.color;
        //GrabbedColor.a = FadePercent;
        //TipGhost.GetComponent<MeshRenderer>().material.color = GrabbedColor;
    }
}
