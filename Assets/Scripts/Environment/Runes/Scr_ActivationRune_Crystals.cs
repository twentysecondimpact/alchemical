﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_ActivationRune_Crystals : Scr_ActivationRune
{

    public int CrystalsRequired;

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        ShowToolTip = true;
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();

        if (Scr_GameDirector.inst.CrystalsCollected >= CrystalsRequired)
        {
            ShowToolTip = false;
        }

    }

    public override void Activate()
    {
        if (Scr_GameDirector.inst.CrystalsCollected >= CrystalsRequired)
        {
            
            base.Activate();
        }
        else
        {
            Debug.Log("Not enough crystals");
        }

    }
}
