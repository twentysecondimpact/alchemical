﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_FlaskUnlock : MonoBehaviour
{
    public GameObject FlaskPrefab;

    public Vector3 RotationPerSecond;

    public int FlaskIndex;

    public AudioEffectSO AESO_Unlock;

    public TooltipObject ToolTip;

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.Rotate(RotationPerSecond * Time.deltaTime);
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            AESO_Unlock.Play();

            other.GetComponent<Scr_Character_Throw>().UnlockFlask(FlaskIndex, FlaskPrefab);

            #region Check to update objective tracker
            bool HasAllFlasks = true;
            //Check if you have all the flasks
            for (int i = 0; i < other.GetComponent<Scr_Character_Throw>().FlaskPrefab.Length; i++)
            {
                if (other.GetComponent<Scr_Character_Throw>().FlaskPrefab[i] == null)
                {
                    HasAllFlasks = false;
                }
            }
            if(HasAllFlasks)
            {
                //Scr_ObjectiveTracker.inst.UpdateObjectText(1);
            }
            #endregion

            gameObject.SetActive(false);// Destroy(this.gameObject);

            if(ToolTip != null)
            {
                ToolTip.Activate();
            }
            

            //Scr_GameDirector.inst.SaveGame();
        }
    }
}
