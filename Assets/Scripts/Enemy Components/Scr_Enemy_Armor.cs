﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_Enemy_Armor : MonoBehaviour
{
    public List<GameObject> ArmorLayers;

	// Use this for initialization
	void Start ()
    {
        //Loop through all children and grab the objects Called Armor
        for (int i = 0; i < transform.childCount; i++)
        {
            if(transform.GetChild(i).name.Contains("Armor"))
            {
                ArmorLayers.Add(transform.GetChild(i).gameObject);
            }
        }
	}
	
    public void TakeDamage(int _Damage)
    {
        if(_Damage <= ArmorLayers.Count)
        {
            if (ArmorLayers.Count > 0)
            {
                //Debug.Log("Took armor damage");
                for (int i = 0; i < _Damage; i++)
                {
                    if (ArmorLayers.Count >= i)
                    {
                        //Destroy(ArmorLayers[i]);
                        ArmorLayers.RemoveAt(i);
                    }
                }
            }
        }
        
        
        
    }

	// Update is called once per frame
	void Update ()
    {
		
	}
}
