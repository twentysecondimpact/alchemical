﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_PathNodeTracker : MonoBehaviour
{
    public static Scr_PathNodeTracker inst;
    public List<Transform> PathNodes;

    private void Awake()
    {
        inst = this; 
    }

    // Use this for initialization
    void Start () {
        for (int i = 0; i < transform.childCount; i++)
        {
            for (int c = 0; c < transform.GetChild(i).transform.childCount; c++)
            {
                PathNodes.Add(transform.GetChild(i).transform.GetChild(c));
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
