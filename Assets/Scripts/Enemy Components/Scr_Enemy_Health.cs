﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_Enemy_Health : MonoBehaviour
{
    public Scr_Enemy enemyController;

    public int BaseHealth;
    public int Health;

    public float UltimateCooldownReduction;

    public GameObject ExplosionEffect;
    public GameObject EyeExplosionEffect;
    public GameObject ScoreOrb;

    public bool Alive;

    public List<GameObject> Eyes;

    public int eyeIndex;

    public float DeathDelayMin;
    public float DeathDelayMax;

    public Vector3 DeathBloatSpeed;

    public float eyeExplosionScale;

    public AudioEffectSO AESO_TakeDamage;
    public AudioEffectSO AESO_Explode;

    public List<GameObject> DestructableEyes;

    Vector3 BaseScale;

    public bool ExplodesOnDeath;
    public GameObject ExplosionPoolPrefab;

    public bool invulnerable;
    public GameObject invulnerableEffectPrefab;
    public GameObject invulnerableEffect;

    public CameraShakeSO CHSO_EnemyDeath;

    public GameObject ExplodedShroomPrefab;

    private void Awake()
    {
        enemyController = GetComponent<Scr_Enemy>();
        Health = BaseHealth;
        Alive = true;
        BaseScale = transform.localScale;
    }

    // Use this for initialization
    void Start ()
    {

	}
	
	// Update is called once per frame
	void Update ()
    {

	}

    public void ActiveInvulnerable()
    {
        invulnerable = true;

        if(invulnerableEffect == null)
        {
            invulnerableEffect = GameObject.Instantiate(invulnerableEffectPrefab, this.transform);
            invulnerableEffect.transform.localPosition = Vector3.zero;
            //invulnerableEffect.transform.localScale = new Vector3(transform.localScale.x / 2.5f, transform.localScale.z / 2.5f, 1);
            ParticleSystem ps = invulnerableEffect.GetComponent<ParticleSystem>();
            var sh = ps.shape;
            sh.radius = transform.localScale.x / 2.5f;
        }
        else
        {
            if(invulnerableEffect.activeSelf == false)
            {
                invulnerableEffect.SetActive(true);
            }
        }
    }

    public void DisableInvulnerable()
    {
        invulnerable = false;

        if (invulnerableEffect != null)
        {
            invulnerableEffect.SetActive(false);
        }
    }

    public void TakeDamage(int _Damage)
    {



        //When taking damage, break CC
        if(enemyController.enemyState == Scr_Enemy.EnemyState.Disabeled)
        {
            if(enemyController.DisabledStacks > 1)
            {
                enemyController.DisabledStacks -= 1;
            }
            else
            {
                enemyController.Enable();
            }
            
        }

        
        //Removes any overkill
        int DamageToSend = _Damage;
        if(DamageToSend > Health)
        {
            DamageToSend = Health;
        }
        DamageToSend = Mathf.Clamp(DamageToSend, 0, 99);

        //Debug.Log(DamageToSend);
        //Scr_GameDirector.inst.scoreSystem.GainPoints(DamageToSend);
        //Spawn a points orb


        //Vector3 RandomSpawnPosition
        //scoreOrb.transform.localScale = this.transform.localScale;

        //If the enemy will die
        if((Health - DamageToSend) <= 0)
        {
            if(this.gameObject.GetComponent<Scr_Enemy_Movement>().enabled == true)
            {
                //Send out an arrgo pulse
                this.gameObject.GetComponent<Scr_Enemy_Movement>().AggroPulse();// (0, Scr_GameDirector.inst.PassiveDelay, true);
            }
            
        }
        //If the enemy will still be alive
        else
        {
            if (this.gameObject.GetComponent<Scr_Enemy_Movement>().enabled == true)
            {
                //Send out an arrgo pulse
                this.gameObject.GetComponent<Scr_Enemy_Movement>().SetStateAggro(Scr_GameDirector.inst.AggroDelay, Scr_GameDirector.inst.PassiveDelay, true);
            }

            //Create the eplxosion effect
            GameObject scoreOrb = GameObject.Instantiate(ScoreOrb, Eyes[eyeIndex].transform.position, Quaternion.identity);
            scoreOrb.GetComponent<Scr_PointsPickUp>().SpawnedFromEnemy = true;
            scoreOrb.GetComponent<Scr_PointsPickUp>().LaunchEye();
            

            //Create the eplxosion effect from the eye
            GameObject explosionEffect = GameObject.Instantiate(EyeExplosionEffect, Eyes[eyeIndex].transform.position, Quaternion.identity);
            explosionEffect.transform.localScale = this.transform.localScale * eyeExplosionScale;

            eyeIndex++;
        }



        //if(!invulnerable)
        //{
        //Debug.Log("Took damage: " + _Damage);

        //}



        //Check if the enemy has armor and if so, tell it to trigger armor damage
        //if (this.gameObject.GetComponent < Scr_Enemy_Armor>() != null)
        //{
        //    this.gameObject.GetComponent<Scr_Enemy_Armor>().TakeDamage(_Damage);
        //}
        if (!invulnerable)
        {
            Health -= _Damage;

            if (Health <= 0)
            {
                StartCoroutine(Die());
            }
            else
            {

                //Turn off the eye based on how much hp you have
                if (DestructableEyes[Health - 1].activeSelf)
                {
                    DestructableEyes[Health - 1].SetActive(false);
                }

                AESO_TakeDamage.Play();
            }
        }
        
        
    }

    public IEnumerator Die()
    {

        Alive = false;
        //Debug.Log(gameObject.name + " Die");

        //Remove the disable before dying
        enemyController.DisabledTimer = 0;

        //Increases the current multiplier
        Scr_GameDirector.inst.scoreSystem.IncreaseMultiplier();

        //Notify the game director of aggro stat change
        Scr_GameDirector.inst.UpdateEnemiesInCombat(true, enemyController);

        //Turn off the collider
        //GetComponent<Collider>().enabled = false;

        //Delay before the final epxlosion
        float Delay = Random.Range(DeathDelayMin, DeathDelayMax);

        GetComponent<Scr_Enemy_Movement>().enabled = false;

        Vector3 expansionRate = new Vector3(Random.Range(0, DeathBloatSpeed.x), Random.Range(0, DeathBloatSpeed.y), Random.Range(0, DeathBloatSpeed.z));

        //Delay while the enemy expands
        for (float i = 0; i < Delay; i+= Time.deltaTime)
        {
            transform.localScale += DeathBloatSpeed * Time.deltaTime;
            yield return null;
        }

        AESO_Explode.Play();

        //Create the eplxosion effect
        GameObject scoreOrb = GameObject.Instantiate(ScoreOrb, Eyes[eyeIndex].transform.position, Quaternion.identity);
        scoreOrb.GetComponent<Scr_PointsPickUp>().SpawnedFromEnemy = true;
        scoreOrb.GetComponent<Scr_PointsPickUp>().LaunchForce *= 2;
        scoreOrb.GetComponent<Scr_PointsPickUp>().LaunchEye();

        //Create the eplxosion effect
        GameObject explosionEffect = GameObject.Instantiate(ExplosionEffect, transform.position, Quaternion.identity);
        explosionEffect.transform.localScale = this.transform.localScale;


        //Create the explosion Pool
        if(ExplodesOnDeath)
        {
            GameObject explosionPool = GameObject.Instantiate(ExplosionPoolPrefab, this.transform.position, Quaternion.identity);
        }


        CHSO_EnemyDeath.Shakeonce(1,1,1,1);

        Scr_UI_Score.inst.UpdateShroomCounter();

        GameObject ExplodedDummy = GameObject.Instantiate(ExplodedShroomPrefab);
        ExplodedDummy.transform.position = this.transform.position;
        ExplodedDummy.transform.localRotation = this.transform.rotation;
        ExplodedDummy.transform.localScale = this.transform.localScale;

        this.gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        Alive = false;
    }

    //When you reenable/respawn an enemy
    private void OnEnable()
    {
        Alive = true;

        //Turn on the collider
        GetComponent<Collider>().enabled = true;

        //Reenable the movement component
        GetComponent<Scr_Enemy_Movement>().enabled = true;

        //Reset the base scale
        transform.localScale = BaseScale;

        

        //enable the navagent
        //GetComponent<Scr_Enemy_Movement>().
    }
}
