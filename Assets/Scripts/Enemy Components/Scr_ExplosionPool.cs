﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_ExplosionPool : MonoBehaviour {

    public float duration;
    public float durationTimer;
    public float SpeedPercentReduction;
    public float Radius;
    public Projector projector;
    public float ExpandRate;

    public Material NormalMaterial;

    public float FadeTime;

    public float TickTimer;
    public float TickTime;

    public int DamagePerTick;

    // Use this for initialization
    void Start()
    {
        durationTimer = duration;

        //Destroy after the time runs out
        Destroy(this.gameObject, duration);
        projector.orthographicSize = 0.001f;


        GetComponent<CapsuleCollider>().radius = Radius;

        TickTimer = TickTime;
    }

    // Update is called once per frame
    void Update()
    {
        if (durationTimer <= 0)
        {
            Destroy(this.gameObject);
        }
        else
        {
            durationTimer -= Time.deltaTime;
        }
        //Make the sap look larger than it is, to have player not feel jipped
        projector.orthographicSize = Mathf.Lerp(projector.orthographicSize, Radius * 1.2f, Time.deltaTime * ExpandRate);
    }

    //Start a fresh tick when you enter
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Debug.Log("Dealing damage to character initial");
            Debug.Log(other.gameObject.name);
            Scr_Character_Components.inst.character_Health.TakeDamage(DamagePerTick);
            TickTimer = TickTime;
        }

        if(other.gameObject.tag == "Enemy")
        {
            other.gameObject.GetComponent<Scr_Enemy_Movement>().SetStateAggro(Scr_GameDirector.inst.AggroDelay, Scr_GameDirector.inst.PassiveDelay, true);
        }
    }


    //Get hit eve X seconds after initial tick
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if(TickTimer > 0)
            {
                TickTimer -= Time.deltaTime;
            }
            else
            {
                Debug.Log("Dealing damage to character");
                Scr_Character_Components.inst.character_Health.TakeDamage(DamagePerTick);
                TickTimer = TickTime;
            }
            //Debug.Log("Sapping player");
            //other.gameObject.GetComponent<Scr_Character_Movement>().BaseRunSpeed = other.gameObject.GetComponent<Scr_Character_Movement>().OriginalBaseRunSpeed * SpeedPercentReduction;
        }
    }
}
