﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_Enemy_AggroDisplay : MonoBehaviour
{

    //public GameObject AggroTriggerProjectorPrefab;
    //public GameObject AggroTriggerProjector;

    public GameObject AggroRangeProjectorPrefab;
    public GameObject AggroRangeProjector;

    public Material RangeMaterialWhite;
    public Material RangeMaterialRed;

    public bool selected;
    public bool previousSelected;

    public bool triggered;
    public bool previousTriggered;

    public float CharacterAggroRange;

    public bool InRange;
    //public 

    public float AggroTriggerProjectorSize;

    //public bool DisplayEnabled;

	// Use this for initialization
	void Start ()
    {
        //Spawn the trigger projector
        //AggroTriggerProjector = GameObject.Instantiate(AggroTriggerProjectorPrefab, transform.position, transform.rotation);
        //AggroTriggerProjector.transform.parent = this.transform;

        //Spawn the range projector
        AggroRangeProjector = GameObject.Instantiate(AggroRangeProjectorPrefab, transform.position, transform.rotation);
        AggroRangeProjector.transform.parent = this.transform;
        AggroRangeProjector.transform.GetChild(0).GetComponent<Projector>().material = RangeMaterialWhite;

        //rangeMaterial = AggroRangeProjector.transform.GetChild(0).GetComponent<Material>().
    }

    // Update is called once per frame
    void Update()
    {


        if (Input.GetKey(KeyCode.LeftShift))
        {
            //TODO Turn RED
            if (!GetComponent<Scr_Enemy_Attack_Contact>().Ranged)
            {
                //If youre withing range of the player
                float DistanceFromPlayer = Vector3.Distance(transform.position, Scr_Character_Components.inst.transform.position);
                if (DistanceFromPlayer < CharacterAggroRange)
                {
                    AggroRangeProjector.SetActive(true);
                    InRange = true;
                }
                else
                {
                    AggroRangeProjector.SetActive(false);
                    InRange = false;
                }
            }
        }
        else
        {
            InRange = false;
            AggroRangeProjector.SetActive(false);
        }

        if(Input.GetKeyUp(KeyCode.LeftShift))
        {
            //GetComponent<Scr_Enemy>().ApplyStarndardMaterial();
        }
    }
    public void EnableAggroDisplay()
    {

        if (InRange)
        {
            //Debug.Log("Enable display");
            AggroRangeProjector.transform.GetChild(0).GetComponent<Projector>().material = RangeMaterialRed;
            
            //AggroTriggerProjector.SetActive(true);
            //AggroRangeProjector.SetActive(true);
            //TriggerPulse();
        }


    }

    public void DisableAggroDisplay()
    {
        //TODO Turn White
            //Debug.Log("DisableDisplay");
        AggroRangeProjector.transform.GetChild(0).GetComponent<Projector>().material = RangeMaterialWhite;

    }

    public void UpdateAggroProjector()
    {

        //Debug.Log("Updating Aggro projector");
        //Set its radius
        //AggroTriggerProjector.transform.GetChild(0).GetComponent<Projector>().orthographicSize = AggroTriggerProjectorSize;// GetComponent<Scr_Enemy_Movement>().AggroPulseRadius;
        AggroRangeProjector.transform.GetChild(0).GetComponent<Projector>().orthographicSize = GetComponent<Scr_Enemy_Movement>().AggroPulseRadius;


        if (selected)
        {
            TriggerPulse();
        }

        if (selected && !previousSelected)
        {
            EnableAggroDisplay();
        }
        else if (!selected && previousSelected)
        {
            DisableAggroDisplay();
        }


        if (!selected)
        {
            if (triggered)
            {
                if (InRange)
                {
                    //TODO Turn RED
                    AggroRangeProjector.transform.GetChild(0).GetComponent<Projector>().material = RangeMaterialRed;

                    //Debug.Log("Turning red from aggro display");

                    
                    //AggroTriggerProjector.SetActive(true);
                }

            }
            else
            {
                if (InRange)
                {
                    //TODO Turn White
                    AggroRangeProjector.transform.GetChild(0).GetComponent<Projector>().material = RangeMaterialWhite;
                    //AggroTriggerProjector.SetActive(false);
                }

            }
        }


    }

    void UpdateHighlightMaterial()
    {
        if (AggroRangeProjector.activeSelf)
        {
            if (AggroRangeProjector.transform.GetChild(0).GetComponent<Projector>().material == RangeMaterialRed)
            {
                GetComponent<Scr_Enemy>().ApplyRedHighlightColor();
            }
        }
        
    }

    public void TriggerPulse()
    {
        triggered = true;

        //Debug.Log("Triggering Pulse");
        //Loop through every enemy triggereing any in range
        foreach (GameObject enemy in GameObject.FindGameObjectsWithTag("Enemy"))
        {
            float leeway = Scr_Character_Components.inst.character_Aggro.AggroLeeway;

            //Debug.Log("Hit another enemy");
            if (Vector3.Distance(this.transform.position, enemy.transform.position) <= ((GetComponent<Scr_Enemy_Movement>().AggroPulseRadius * leeway) + (enemy.GetComponent<Scr_Enemy_Movement>().AggroPulseRadius * leeway)))
            {
                //GetComponent<Scr_Enemy_AggroDisplay>().triggered = true;
                //Debug.Log("Enemy in Range: " + enemy.transform.gameObject.name);
                if (enemy.GetComponent<Scr_Enemy_AggroDisplay>().triggered == false && enemy.GetComponent<Scr_Enemy_AggroDisplay>().selected == false && enemy.GetComponent<Scr_Enemy>().DisabledTimer <= 0 && GetComponent<Scr_Enemy_Attack_Contact>().Ranged == false)
                {
                    //Debug.Log("Enemy in Range: " + enemy.transform.gameObject.name);
                    enemy.GetComponent<Scr_Enemy_AggroDisplay>().triggered = true;
                    enemy.GetComponent<Scr_Enemy_AggroDisplay>().TriggerPulse();
                }
            }
        }
    }

    private void LateUpdate()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            UpdateAggroProjector();
            UpdateHighlightMaterial();
        }

        previousSelected = selected;
        selected = false;

        previousTriggered = triggered;
        triggered = false;

    }
}
