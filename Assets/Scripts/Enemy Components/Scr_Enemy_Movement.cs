﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Scr_Enemy_Movement : MonoBehaviour
{
    public Scr_Enemy enemyController;

    public NavMeshAgent Agent;
    public Transform Player;

    public Vector3 TargetPosition;
    public Vector3 StartingPosition;

    public float PatrollingSpeed;
    public float AggroSpeed;
    public float BaseSpeed;
    public float Speed;

    public bool AggroStateAlert = false;
    public bool DeAggroStateAlert = false;

    GameObject AggroAlertDisplayObject;
    GameObject PassiveAlertDisplayObject;

    public AudioEffectSO AESO_AggroTrigger;
    public AudioEffectSO AESO_DeAggroTrigger;
    public AudioEffectSO AESO_FootSteps;

    public Scr_Enemy_AggroDisplay aggroDisplay;

    public bool CancelAggroDelays;

    public Vector3 PreviousPosition;
    public Vector3 Velocity;

    public enum AggroState
    {
        Passive,
        Aggroed,
        Returning
    }
    public AggroState aggroState;

    public float AggroPulseRadius;

    public float AggroPulseTime;
    public float AggroPulseTimer;

    public GameObject AggroAlert;
    public GameObject DeAggroAlert;

    public TooltipObject TTSO_ChainAggro;

    public bool StartPatrolling;
    public bool Patrolling;

    public List<Transform> StartingPathNodes;

    void Awake()
    {
        enemyController = GetComponent<Scr_Enemy>();
    }

	// Use this for initialization
	void Start ()
    {
        Speed = BaseSpeed;

        StartingPosition = transform.position;

        Player = Scr_Character_Components.inst.gameObject.transform;
        TargetPosition = Player.position;
        SetStatePassive(0);

        aggroDisplay = GetComponent<Scr_Enemy_AggroDisplay>();

        if (StartPatrolling)
        {
            FindClosestPathNode();
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        ////Debug Aggro Button
        //if (Input.GetKeyDown(KeyCode.Space))
        //{
        //    SetStateAggro();
        //}

        //Needs to be active to update
        if(enemyController.enemyState == Scr_Enemy.EnemyState.Active)
        {
            UpdateMovement();
            if (GetComponent<Scr_Enemy_Attack_Contact>().Ranged)
            {
                UpdateRangedMovement();
            }
            

            if (aggroState == AggroState.Aggroed)
            {
                UpdateAggroPulseTimer();
            }
        }
	}

    private void LateUpdate()
    {
        Velocity = transform.position - PreviousPosition;
        PreviousPosition = transform.position;
    }

    void UpdateMovement()
    {
        switch (aggroState)
        {
            case AggroState.Passive:
                break;
            case AggroState.Aggroed:
                TargetPosition = Scr_Character_Components.inst.character_Movement.LastNavPosition;

                //Debug.Log(PlaceOnNavMesh);

                //Ranged aggro rules
                if (GetComponent<Scr_Enemy_Attack_Contact>().Ranged)
                {
                    //Update the position if they are in an area that can attack, otherwise just keep doing what you do
                    if (Scr_GameDirector.CharacterAttackable(Agent, TargetPosition))
                    {
                        Agent.destination = TargetPosition;
                    }
                }
                //Meele aggro rules
                else
                {
                    if (Scr_GameDirector.CharacterAttackable(Agent, TargetPosition))
                    {
                        Agent.destination = TargetPosition;
                    }
                    else
                    {
                        if (Scr_Character_Components.inst.character_Movement.Grounded)
                        {
                            SetStatePassive(Scr_GameDirector.inst.PassiveDelay);
                        }
                    }
                }
                break;
        }
    }

    void UpdateRangedMovement()
    {
        if(aggroState == AggroState.Aggroed)
        {
            if(Vector3.Distance(transform.position, Scr_Character_Components.inst.transform.position) < GetComponent<Scr_Enemy_Attack_Contact>().FollowRange)
            {
                ChangeSpeed(0);
            }
            else
            {
                if(GetComponent<Scr_Enemy>().DisabledStacks <= 0)
                {
                    ChangeSpeed(AggroSpeed);
                }
            }
        }
    }

    public void SetStatePassive(float _Delay)
    {
        if (enemyController.GetComponent<Scr_Enemy_Health>().Alive)
        {
            //Debug.Log(gameObject.name + " Passive");

            if (!(AggroStateAlert || DeAggroStateAlert))
            {
                TargetPosition = StartingPosition;
                Agent.destination = TargetPosition;
                aggroState = AggroState.Passive;

                if (_Delay > 0)
                {
                    GameObject deAggroAllert = GameObject.Instantiate(DeAggroAlert, this.transform);
                    deAggroAllert.transform.localPosition = new Vector3(0, 1.7f, 0);
                    deAggroAllert.transform.localRotation = Quaternion.identity;
                    PassiveAlertDisplayObject = deAggroAllert;

                    if (AggroAlertDisplayObject != null)
                    {
                        Destroy(AggroAlertDisplayObject);
                    }

                    AESO_DeAggroTrigger.Play();
                }

                //Recalculate aggro
                Scr_GameDirector.inst.UpdateEnemiesInCombat(true, enemyController);

                StartCoroutine(DeAggroDelay(Random.Range(_Delay, _Delay)));
            }
        }
        

            


    }

    public void SetStateAggro(float _Delay, float _PassiveDelay, bool _FromDamage)
    {
        if(!(AggroStateAlert || DeAggroStateAlert))
        {
            if (GetComponent<Scr_Enemy_Health>().Alive == true)
            {
                //Only aggro when active
                if (enemyController.enemyState == Scr_Enemy.EnemyState.Active)
                {

                    if (aggroState != AggroState.Aggroed)
                    {
                        if (Scr_GameDirector.CharacterAttackable(Agent, Player.GetComponent<Scr_Character_Movement>().LastNavPosition) || GetComponent<Scr_Enemy_Attack_Contact>().Ranged)
                        {
                            aggroState = AggroState.Aggroed;
                            //Debug.Log("Setting aggro state to aggroed");

                            TargetPosition = Player.position;
                            //Agent.speed = AggroSpeed;
                            //Notify the game director of aggro stat change
                            Scr_GameDirector.inst.UpdateEnemiesInCombat(false, enemyController);

                            AESO_AggroTrigger.Play();

                            GameObject aggroAllert = GameObject.Instantiate(AggroAlert, this.transform);
                            aggroAllert.transform.localPosition = new Vector3(0, 1.7f, 0);
                            aggroAllert.transform.localRotation = Quaternion.identity;
                            AggroAlertDisplayObject = aggroAllert;

                            //If the passive one is still up get rid of it
                            if(PassiveAlertDisplayObject != null)
                            {
                                Destroy(PassiveAlertDisplayObject);
                            }

                            //Aggro animation trigger
                            GetComponent<Scr_EnemyAnimationVariables>().TriggerAggroFromGround();

                            StartCoroutine(AggroDelay(Random.Range(_Delay, _Delay), _FromDamage));
                        }
                        else
                        {
                            if (_FromDamage)
                            {
                                SetStatePassive(_PassiveDelay);
                            }
                        }
                    }
                }
            }
            else
            {

                //Attempts to aggro everything else in a radius 
                AggroPulse();
            }

        }
    }

    public void FinalizeStateAggro(bool _FromDamage)
    {
        //if (enemyController.GetComponent<Scr_Enemy_Health>().Alive)
        //{
        //    bool ConfirmAggro = false;

        //    //If the aggro was cause by proximity and not damage, check if they are still in range
        //    if (_FromDamage == false)
        //    {
        //        //If the characer is in range
        //        if (Vector3.Distance(transform.position, Scr_Character_Components.inst.transform.position) < ((AggroPulseRadius* Scr_Character_Components.inst.character_Aggro.AggroLeeway) + (Scr_Character_Components.inst.character_Aggro.AggroRadius * Scr_Character_Components.inst.character_Aggro.AggroLeeway)))
        //        {
        //            //If there is a viable path
        //            NavMeshPath newPath = new NavMeshPath();
        //            Agent.CalculatePath(transform.position, newPath);
        //            if (newPath.status == NavMeshPathStatus.PathComplete)
        //            {
        //                ConfirmAggro = true;
        //            }
        //        }
        //    }
        //    else
        //    {
        //        ConfirmAggro = true;
        //    }

        //    if (ConfirmAggro)
        //    {
        //        //Trigger the final aggro state
        //        //Attempts to aggro everything else in a radius
        //        AggroPulse();
        //        //Agent.speed = AggroSpeed;
        //        ChangeSpeed(AggroSpeed);
        //    }
        //    else
        //    {
        //        SetStatePassive(Scr_GameDirector.inst.PassiveDelay);
        //    }

        //}

        AggroPulse();
        //Agent.speed = AggroSpeed;
        ChangeSpeed(AggroSpeed);

    }

    public void FinalizePassiveState()
    {
        if (Patrolling)
        {
            ChangeSpeed(PatrollingSpeed);
        }
        else
        {
            ChangeSpeed(0);
        }
        
    }

    public void FindClosestPathNode()
    {


        //_Enemy.Patrolling = true;

        //Find the closest node
        Vector3 ClosestNode = Vector3.zero;
        float ClosestDistance = 9999;
        foreach(Transform node in StartingPathNodes)
        {
            float Distance = Vector3.Distance(node.position, transform.position);
            if (Distance < ClosestDistance)
            {
                ClosestDistance = Distance;
                ClosestNode = node.position;
            }
        }



        //Chenge the enemy target position
        ChangeSpeed(PatrollingSpeed);
        //ChargeTargetPosition(ClosestNode);

        TargetPosition = ClosestNode;
        Agent.destination = TargetPosition;

        Patrolling = true;
        
    }

    IEnumerator AggroDelay(float _DelayTime, bool _FromDamage)
    {
        AggroStateAlert = true;
        for (float i = 0; i < _DelayTime; i += Time.deltaTime)
        {

            ChangeSpeed(0);
            yield return null;
        }
        AggroStateAlert = false;

        if (CancelAggroDelays)
        {
            CancelAggroDelays = false;
        }
        else
        {
            FinalizeStateAggro(_FromDamage);
        }
        

        
        
    }

    IEnumerator DeAggroDelay(float _DelayTime)
    {
        DeAggroStateAlert = true;
        //Wait a while and keep at speed 0
        for (float i = 0; i < _DelayTime; i += Time.deltaTime)
        {
            ChangeSpeed(0);

            //If you get aggroed then just cancel this
            if(aggroState == AggroState.Aggroed)
            {
                break;
            }

            yield return null;
        }

        DeAggroStateAlert = false;
        FinalizePassiveState();
    }

    void UpdateAggroPulseTimer()
    {

        if(AggroPulseTimer <= 0)
        {
            //AggroPulse();
        }
        else
        {
            AggroPulseTimer -= Time.deltaTime;
        }
    }

    public void AggroPulse()
    {
        //Dont aggro pulse if you are ranged
        if (!GetComponent<Scr_Enemy_Attack_Contact>().Ranged)
        {
            //Reset the aggro pulse timer
            AggroPulseTimer = AggroPulseTime;

            foreach (GameObject enemy in GameObject.FindGameObjectsWithTag("Enemy"))
            {
                Scr_Enemy_Movement em = enemy.GetComponent<Scr_Enemy_Movement>();

                //Only attempt to aggro somethign that isnt already aggroed
                if (em.aggroState != AggroState.Aggroed)
                {
                    float leeway = Scr_Character_Components.inst.character_Aggro.AggroLeeway;
                    if (Vector3.Distance(enemy.transform.position, this.transform.position) < ((AggroPulseRadius * leeway) + (em.AggroPulseRadius * leeway)))
                    {
                        if (enemy.GetComponent<Scr_Enemy_Health>().Alive == true && enemy.GetComponent<Scr_Enemy_Attack_Contact>().Ranged == false)
                        {
                            if(enemy != this.gameObject)
                            {
                                //Debug.Log("Chain enemy");
                                TTSO_ChainAggro.Activate();
                            }



                            //a triggered pulse from another enemy still counts as damage as the delay time has by definition already been used
                            enemy.GetComponent<Scr_Enemy_Movement>().SetStateAggro(Scr_GameDirector.inst.AggroDelay, Scr_GameDirector.inst.PassiveDelay, true);
                            
                        }
                    }
                }
            }
        }
        
    }

    public void ChangeSpeed(float _Speed)
    {
        if (GetComponent<Scr_Enemy_Health>().Alive)
        {
            Speed = _Speed;
            Agent.speed = Speed;

            if(_Speed == AggroSpeed)
            {
                ChangeTurn(120, 900);
            }
        }
    }

    public void ChangeTurn(float _Accel, float _AngSpeed)
    {
        Agent.angularSpeed = _AngSpeed;
        Agent.acceleration = _Accel;

    }

    public void ChargeTargetPosition(Vector3 _TargetPosition)
    {
        if (GetComponent<Scr_Enemy_Health>().Alive)
        {
            TargetPosition = _TargetPosition;
            Agent.destination = TargetPosition;
        }

    }

    private void OnDisable()
    {
        if(AggroAlertDisplayObject != null)
        {
            AggroAlertDisplayObject.SetActive(false);
        }
        
        if(PassiveAlertDisplayObject != null)
        {
            PassiveAlertDisplayObject.SetActive(false);
        }

        ChangeSpeed(0);

        StopAllCoroutines();

        Agent.enabled = false;
    }

    private void OnEnable()
    {
        

        //if it has a hover component, get rid of it
        if (GetComponent<Scr_EnemyHover>() != null)
        {
            Destroy(GetComponent<Scr_EnemyHover>());
        }


        //Debug.Log("Being Enableled");
        ChangeSpeed(0);

        Agent.enabled = true;

        //Debug.Log("Enabling movement");

        AggroStateAlert = false;

        ChangeSpeed(0);

        if(StartPatrolling)
        {

            FindClosestPathNode();

        }
        else
        {
            Agent.SetDestination(transform.position);

        }
        

        //Set state as passive
        SetStatePassive(0);


    }
    //public Vector3 
}
