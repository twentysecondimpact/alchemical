﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_Enemy : MonoBehaviour
{
    public Scr_Enemy_Movement enemy_Movement;
    public int DisabledStacks;

    Material StandardMaterial;
    public Material HighlightedMaterialRed;
    public Material HighlightedMaterialWhite;

    bool MaterialSet = false;

    public SkinnedMeshRenderer meshRenderer;

    public AudioSource audioSource;

    public enum EnemyState
    {
        Active,
        Disabeled
    }
    public EnemyState enemyState;

    public float DisabledTimer;

    public void Awake()
    {
        enemy_Movement = GetComponent<Scr_Enemy_Movement>();
        //meshRenderer = transform.GetChild(0).GetChild(0).GetComponent<MeshRenderer>();
        StandardMaterial = meshRenderer.material;
    }

    // Use this for initialization
    void Start () {
        enemyState = EnemyState.Active;
	}
	
	// Update is called once per frame
	void Update () {
		if(enemyState == EnemyState.Disabeled)
        {
            UpdateDisabeledTimer();
        }
	}

    public void Disable(float _Duration, int _DisabledStacks)
    {
        DisabledStacks = _DisabledStacks;
        enemyState = EnemyState.Disabeled;
        DisabledTimer = _Duration;
        //enemy_Movement.ChangeSpeed(0);
        GetComponent<Scr_Enemy_Movement>().Agent.isStopped = true;
    }

    public void Enable()
    {
        enemyState = EnemyState.Active;

        //Search children for a disabledEffect and delete it
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).gameObject.name.Contains("DisabledParticleEffect"))
            {
                transform.GetChild(i).gameObject.SetActive(false);
            }
        }

        if (GetComponent<Scr_Enemy_Health>().Alive)
        {
            //enemy_Movement.ChangeSpeed(enemy_Movement.BaseSpeed);
            GetComponent<Scr_Enemy_Movement>().Agent.isStopped = false;
        }

    }

    void UpdateDisabeledTimer()
    {
        if(DisabledTimer > 0)
        {
            DisabledTimer -= Time.deltaTime;
        }
        else
        {
            Enable();
        }
    }

    public void ApplyRedHighlightColor()
    {
        if(meshRenderer.material != HighlightedMaterialRed)
        {
            meshRenderer.material = HighlightedMaterialRed;
            //Debug.Log("Applying Red Material");
        }
        //Debug.Log("Material Set To Red");
        MaterialSet = true;
    }

    public void ApplyWhiteHightlightColor()
    {
        if(meshRenderer.material != HighlightedMaterialRed)
        {
            meshRenderer.material = HighlightedMaterialWhite;
            //Debug.Log("Applying White Material");
        }
        
        MaterialSet = true;
        
    }

    public void ApplyStarndardMaterial()
    {
        if(meshRenderer.material != StandardMaterial)
        {
            //Debug.Log("Applying Standard material");
            meshRenderer.material = StandardMaterial;
        }

    }

    private void LateUpdate()
    {
        if (MaterialSet)
        {
            //Keep the material till the next frame
        }
        else
        {
            //otherwise go back to normal
            ApplyStarndardMaterial();
        }

        MaterialSet = false;
    }
}
