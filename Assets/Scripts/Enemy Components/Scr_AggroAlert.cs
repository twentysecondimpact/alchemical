﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_AggroAlert : MonoBehaviour
{

    public float duration;
    public float PeakDuration;

    float timer;

    public AnimationCurve curve;

    Vector3 StartingScale;

	// Use this for initialization
	void Start ()
    {
        timer = 0;
        StartingScale = this.transform.localScale;
        transform.localScale = StartingScale * curve.Evaluate(timer / duration);
        
	}
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;

        transform.localScale = StartingScale * curve.Evaluate(timer / duration);
        transform.LookAt(Camera.main.transform.position);

        if(timer>= PeakDuration)
        {
            //Scr_Character_Components.inst.character_Aggro.AggroTooltip.Activate();
        }

        if(timer >= duration)
        {
            Destroy(this);
        }
	}
}
