﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_CaptainBuff : MonoBehaviour
{

    public List<Scr_Enemy_Health> EnemiesInRange;

    public float AOERange;

    public CapsuleCollider BuffAOECollider;

    public Scr_Enemy_Health health;
    public Scr_Enemy enemy;

    public GameObject AOEBuffEffect;

    public bool BuffActive;

    // Use this for initialization
    void Start ()
    {
        health = GetComponent<Scr_Enemy_Health>();
        enemy = GetComponent<Scr_Enemy>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        
        //if(enemy.enemyState == Scr_Enemy.EnemyState.Disabeled)
        //{
        //    BuffActive = false;
        //    AOEBuffEffect.SetActive(false);

        //    if(EnemiesInRange.Count > 0)
        //    {
        //        for (int i = 0; i < EnemiesInRange.Count; i++)
        //        {
        //            EnemiesInRange[i].DisableInvulnerable();

        //        }
        //    }

        //    EnemiesInRange.Clear();
            


        //}
        //else
        //{
        //    BuffActive = true;
        //    AOEBuffEffect.SetActive(true);
        //}


        foreach (GameObject enemy in GameObject.FindGameObjectsWithTag("Enemy"))
        {

            Scr_Enemy_Health em = enemy.GetComponent<Scr_Enemy_Health>();

            if(em != GetComponent<Scr_Enemy_Health>())
            {
                //If youre not in the list
                if (!EnemiesInRange.Contains(em))
                {
                    Vector3 YCanceledPlayer = this.transform.position;
                    YCanceledPlayer.y = 0;
                    Vector3 YCanceledEnemy = enemy.transform.position;
                    YCanceledEnemy.y = 0;

                    if (Vector3.Distance(YCanceledEnemy, YCanceledPlayer) <= (AOERange))
                    {
                        //If the buff is also active
                        if (BuffActive)
                        {
                            EnemiesInRange.Add(em);
                            em.ActiveInvulnerable();
                        }
                        //If the buff isnt acive, remove them from the list
                        else
                        {
                            EnemiesInRange.Remove(em);
                            em.DisableInvulnerable();
                        }
                        
                    }
                }
                else
                {
                    Vector3 YCanceledPlayer = this.transform.position;
                    YCanceledPlayer.y = 0;
                    Vector3 YCanceledEnemy = enemy.transform.position;
                    YCanceledEnemy.y = 0;

                    if (Vector3.Distance(YCanceledEnemy, YCanceledPlayer) > (AOERange))
                    {
                        EnemiesInRange.Remove(em);
                        em.DisableInvulnerable();
                    }
                }
            }
        }

        if(EnemiesInRange.Count == 0)
        {
            health.DisableInvulnerable();
        }
        else
        {
            health.ActiveInvulnerable();
        }
    }
}
