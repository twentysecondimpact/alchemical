﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_EnemyAnimationVariables : MonoBehaviour
{

    Scr_Enemy enemy;
    public Animator animator;

    public float RunAnimationModifier;


    public float AggroAnimationDelay;
    public float PatrolAggroAnimationDelay;

    bool PreviousPatrolling;

    public float DisableDistance;

    private void Awake()
    {
        enemy = this.GetComponent<Scr_Enemy>();
    }

    // Use this for initialization
    void Start()
    {
        animator.SetFloat("PatrolTimeScale", 1);
        
    }

    // Update is called once per frame
    void Update()
    {
        UpdateMovementVariables();
        UpdateAnimationLoD();
    }

    void UpdateWalkingAudio()
    {

    }

    public void TriggerPatrolling()
    {
        animator.SetTrigger("StartPatrolling");
        animator.SetFloat("PatrolTimeScale", 1);
    }

    void UpdateMovementVariables()
    {
        if (PreviousPatrolling == false && enemy.enemy_Movement.Patrolling)
        {
            animator.SetTrigger("StartPatrolling");
            animator.SetFloat("PatrolTimeScale", 1);
        }
        PreviousPatrolling = enemy.enemy_Movement.Patrolling;

        animator.SetFloat("MoveSpeed", enemy.enemy_Movement.Agent.speed);

        if(enemy.enemy_Movement.enemyController.enemyState == Scr_Enemy.EnemyState.Disabeled)
        {
            animator.SetBool("Disabled", true);
        }
        else
        {
            animator.SetBool("Disabled", false);
        }
    }

    public void ResetAnimations()
    {
        animator.SetTrigger("ResetAnimations");
    }

    void UpdateAnimationLoD()
    {
        //if(Vector3.Distance( Scr_Character_Components.inst.transform.position,transform.position) < DisableDistance)
        //{
        //    animator.enabled = true;
        //}
        //else
        //{
        //    animator.enabled = false;
        //}
    }

    public void TriggerAggroFromGround()
    {
        if(enemy.GetComponent<Scr_Enemy_Attack_Contact>().Ranged == false)
        {
            if (enemy.enemy_Movement.Patrolling)
            {
                StartCoroutine(DelayPatrolAggro(PatrolAggroAnimationDelay));
            }
            else
            {
                StartCoroutine(DelayAggro(AggroAnimationDelay));
            }
        }
        
        
    }



    IEnumerator DelayPatrolAggro(float _Delay)
    {
        animator.SetFloat("PatrolTimeScale", 0);
        yield return new WaitForSeconds(_Delay);
        animator.SetFloat("PatrolTimeScale", 1);
        animator.SetTrigger("Aggro");
    }

    IEnumerator DelayAggro(float _Delay)
    {
        yield return new WaitForSeconds(_Delay);

        Debug.Log("Aggro Animation");
        animator.SetTrigger("Aggro");
    }
}
