﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Scr_EnemyTracker : MonoBehaviour
{

    public List<EnemyListEntry> Enemies;

    public static Scr_EnemyTracker inst;

    private void Awake()
    {
        inst = this;
    }

    // Use this for initialization
    void Start ()
    {
        CreateInitialList();

    }
	
	// Update is called once per frame
	void Update ()
    {

	}

    public void CreateInitialList()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            if(transform.GetChild(i).gameObject.activeSelf)
            {
                Enemies.Add(new EnemyListEntry(transform.GetChild(i).GetComponent<Scr_Enemy>()));
            }
        }

        Scr_UI_Score.inst.InitializeShroomCounter(Enemies.Count);
    }

    public void UpdateEnemyList()
    {
        for (int i = 0; i < Enemies.Count; i++)
        {
            //If the enemy is disabeled upon saving, it stays that way
            if (!Enemies[i].EnemyComponent.gameObject.activeSelf)
            {
                Enemies[i].Alive = false;
            }
        }
    }

    public void LoadEnemiesFromList()
    {
        for (int i = 0; i < Enemies.Count; i++)
        {
            //If the enemy has been destroyed but its state hasnt been saved, spawn another one
            if (Enemies[i].Alive)
            {
                Enemies[i].Reset();
            }
        }

        for (int i = 0; i < Enemies.Count; i++)
        {
            if(Enemies[i].EnemyComponent.GetComponent<Scr_Enemy_Movement>().AggroStateAlert)
            {
                //EnemyComponent.enemy_Movement.SetStatePassive(0);
                //Enemies[i].EnemyComponent.enemy_Movement.SetToPassiveNow = true;
            }
        }

        Scr_UI_Score.inst.UpdateShroomCounter();
    }

    [Serializable]
    public class EnemyListEntry
    {
        public Scr_Enemy EnemyComponent;
        public Vector3 SpawningPosition;
        public Vector3 SpawningScale;
        public Quaternion SpawningRotation;

        public bool Alive;


        public EnemyListEntry(Scr_Enemy _enemyComponent)
        {
            EnemyComponent = _enemyComponent;
            SpawningPosition = _enemyComponent.transform.position;
            SpawningScale = _enemyComponent.transform.localScale;
            SpawningRotation = _enemyComponent.transform.rotation;
            Alive = true;
        }

        public void Reset()
        {
            EnemyComponent.enemy_Movement.StopAllCoroutines();

            if (EnemyComponent.GetComponent<Rigidbody>() != null)
            {
                Destroy(EnemyComponent.GetComponent<Rigidbody>());
            }

            EnemyComponent.GetComponent<Scr_Enemy_Health>().Alive = true;

            EnemyComponent.gameObject.SetActive(true);

            EnemyComponent.GetComponent<Scr_Enemy_Movement>().enabled = true;

            EnemyComponent.GetComponent<BoxCollider>().enabled = true;

            //Reset Transform
            EnemyComponent.transform.position = SpawningPosition;
            EnemyComponent.transform.localScale = SpawningScale;
            EnemyComponent.transform.rotation = SpawningRotation;

            //Hopefully stop
            EnemyComponent.enemy_Movement.ChangeSpeed(0);
            EnemyComponent.enemy_Movement.Agent.SetDestination(EnemyComponent.transform.position);


            

            //Set state to passive
            EnemyComponent.enemy_Movement.SetStatePassive(0);

            //If aggro alert or passive alert of, Set to cancel
            //if(EnemyComponent.enemy_Movement.AggroStateAlert || EnemyComponent.enemy_Movement.DeAggroStateAlert)
            //{
            //EnemyComponent.enemy_Movement.CancelAggroDelays = true;
            //}

            

            //Reset health
            EnemyComponent.GetComponent<Scr_Enemy_Health>().Health = EnemyComponent.GetComponent<Scr_Enemy_Health>().BaseHealth;

            //Reset eyes
            for (int i = 0; i < EnemyComponent.GetComponent<Scr_Enemy_Health>().DestructableEyes.Count; i++)
            {
                EnemyComponent.GetComponent<Scr_Enemy_Health>().DestructableEyes[i].SetActive(true);
            }

            EnemyComponent.GetComponent<Scr_Enemy_Health>().eyeIndex = 0;

            

            //Reset patrol
            if (EnemyComponent.enemy_Movement.StartPatrolling)
            {
                EnemyComponent.enemy_Movement.FindClosestPathNode();
                EnemyComponent.GetComponent<Scr_EnemyAnimationVariables>().TriggerPatrolling();
            }
            else
            {
                EnemyComponent.GetComponent<Scr_EnemyAnimationVariables>().ResetAnimations();
            }
        }
    }
}
