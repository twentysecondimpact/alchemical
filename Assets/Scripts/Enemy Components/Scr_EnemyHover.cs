﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_EnemyHover : MonoBehaviour
{

    Rigidbody rb;

    float Dampening = 0;

	// Use this for initialization
	void Start ()
    {
        rb = GetComponent<Rigidbody>();
        if(rb == null)
        {
            rb = gameObject.AddComponent<Rigidbody>();
        }


    }
	
	// Update is called once per frame
	void Update ()
    {
        rb = GetComponent<Rigidbody>();
        if (rb == null)
        {
            rb = gameObject.AddComponent<Rigidbody>();
        }


        rb.velocity = Vector3.Lerp(rb.velocity, Vector3.one * 0.05f, Dampening * Time.deltaTime);
        rb.angularVelocity = Vector3.Lerp(rb.angularVelocity, Vector3.one * 0.05f, Dampening * Time.deltaTime);
    }

    public void KnockUp(float _KnockUpForce, Vector3 _KnockUpTorque, float _Dampening)
    {
        StartCoroutine(DelayedKnockUp(_KnockUpForce, _KnockUpTorque, _Dampening));

        //Dampening = _Dampening;

        //rb = gameObject.AddComponent<Rigidbody>();
        //rb.useGravity = false;
        //rb.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
        //rb = GetComponent<Rigidbody>();

        //rb.AddForce(new Vector3(0, _KnockUpForce, 0));
        //rb.AddTorque(new Vector3(Random.Range(-_KnockUpTorque.x, _KnockUpTorque.x), Random.Range(-_KnockUpTorque.y, _KnockUpTorque.y), Random.Range(-_KnockUpTorque.z, _KnockUpTorque.z)));
    }

    IEnumerator DelayedKnockUp(float _KnockUpForce, Vector3 _KnockUpTorque, float _Dampening)
    {
        Dampening = _Dampening;

        rb = gameObject.AddComponent<Rigidbody>();
        rb.useGravity = false;
        rb.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
        rb = GetComponent<Rigidbody>();

        yield return new WaitForSeconds(0.06f);

        rb.AddForce(new Vector3(0, _KnockUpForce, 0));
        rb.AddTorque(new Vector3(Random.Range(-_KnockUpTorque.x, _KnockUpTorque.x), Random.Range(-_KnockUpTorque.y, _KnockUpTorque.y), Random.Range(-_KnockUpTorque.z, _KnockUpTorque.z)));
    }

    private void OnDestroy()
    {
        //Debug.Log("Enemy Hover Being destroyer");
        if(rb != null)
        {
            //Debug.Log("Taking the RB with me");
            Destroy(rb);
        }
    }
}
