﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_Enemy_Attack_Contact : MonoBehaviour {

    public bool Ranged;

    public Scr_Enemy enemyController;

    public float Cooldown;
    public float CooldownTimer;
    public int HitDamage;

    public GameObject ExplosionEffect;

    public AudioEffectSO AESO_Explode;


    public float ProjectileRange;
    public float FollowRange;

    public float ProjectileGravity;


    public GameObject ProjectilePrefab;

    Vector3 BaseScale;
    public float CastDuration;
    public AnimationCurve CastCurve;
    public float CastCurveFactor;
    public float AttackCastTime;

    public GameObject EyeSphere;
    public GameObject EyeSocket;

    public AnimationCurve EyeGrowthCurve;
    public float EyeGrowDuration;

    public AnimationCurve EyeFadeCurve;
    //public AnimationCurve EmmisionCurve;

    public Color StartingColor;
    public Color GoalColor;

    [ColorUsageAttribute(true, true)]
    public Color StartingEmmission;
    [ColorUsageAttribute(true, true)]
    public Color GoalEmmission;

    [TextArea(3, 10)]
    public string[] ToolTipText;
    static bool TooltipActivated = false;

    private void Awake()
    {
        enemyController = GetComponent<Scr_Enemy>();
    }

    // Use this for initialization
    void Start () {
        BaseScale = transform.localScale;
        CooldownTimer = 1;

        //EyeSocket = GetComponent<Scr_Enemy_Health>().Eyes[0];

        if (Ranged)
        {

            StartingColor = new Color(EyeSphere.GetComponent<Renderer>().material.color.r, EyeSphere.GetComponent<Renderer>().material.color.b, EyeSphere.GetComponent<Renderer>().material.color.g, EyeSphere.GetComponent<Renderer>().material.color.a);
            GoalColor = new Color(ProjectilePrefab.GetComponent<Renderer>().sharedMaterial.color.r, ProjectilePrefab.GetComponent<Renderer>().sharedMaterial.color.b, ProjectilePrefab.GetComponent<Renderer>().sharedMaterial.color.g, ProjectilePrefab.GetComponent<Renderer>().sharedMaterial.color.a);

            

            StartingEmmission = new Color(EyeSphere.GetComponent<Renderer>().material.GetColor("_EmissionColor").r, EyeSphere.GetComponent<Renderer>().material.GetColor("_EmissionColor").b, EyeSphere.GetComponent<Renderer>().material.GetColor("_EmissionColor").g, EyeSphere.GetComponent<Renderer>().material.GetColor("_EmissionColor").a);
            GoalEmmission = new Color(ProjectilePrefab.GetComponent<Renderer>().sharedMaterial.GetColor("_EmissionColor").r, ProjectilePrefab.GetComponent<Renderer>().sharedMaterial.GetColor("_EmissionColor").b, ProjectilePrefab.GetComponent<Renderer>().sharedMaterial.GetColor("_EmissionColor").g, ProjectilePrefab.GetComponent<Renderer>().sharedMaterial.GetColor("_EmissionColor").a);

            

        }

    }

    // Update is called once per frame
    void Update ()
    {
        if(GetComponent<Scr_Enemy_Movement>().aggroState == Scr_Enemy_Movement.AggroState.Aggroed && GetComponent<Scr_Enemy>().enemyState == Scr_Enemy.EnemyState.Active)
        {
            UpdateCooldownTimer();
        }
        else
        {
            CooldownTimer = Cooldown;
        }
        
    }

    public void UpdateCooldownTimer()
    {
        if(CooldownTimer > 0)
        {
            CooldownTimer -= Time.deltaTime;
        }
        else
        {
            if (Ranged)
            {
                if(Vector3.Distance(transform.position, Scr_Character_Components.inst.transform.position) < ProjectileRange)
                {
                    RangedAttack();
                    CooldownTimer = Cooldown;
                }
                //If youre not in range when you try to attack, set to passive
                else
                {
                    GetComponent<Scr_Enemy_Movement>().SetStatePassive(0);
                }
            }
        }
    }

    public void RangedAttack()
    {
        StartCoroutine(RangedAttackCast(CastDuration));
    }

    public void Attack()
    {
        //If the cooldown is up, and your not in i
        //if(CooldownTimer <= 0)
        //{
            Scr_Character_Components.inst.character_Health.TakeDamage(HitDamage);
            CooldownTimer = Cooldown;
            Explode();
        //}
    }

    public void Explode()
    {
        //Create the particle effect
        AESO_Explode.Play();

        //Notify the game director of aggro stat change
        Scr_GameDirector.inst.UpdateEnemiesInCombat(true, enemyController);

        //Create the eplxosion effect
        GameObject explosionEffect = GameObject.Instantiate(ExplosionEffect, transform.position, Quaternion.identity);
        explosionEffect.transform.localScale = this.transform.localScale;

        //Create the explosion Pool
        if (GetComponent<Scr_Enemy_Health>().ExplodesOnDeath)
        {
            GameObject explosionPool = GameObject.Instantiate(GetComponent<Scr_Enemy_Health>().ExplosionPoolPrefab, this.transform.position, Quaternion.identity);
        }

        gameObject.SetActive(false);
    }

    private void OnTriggerStay(Collider other)
    {
        if(enemyController.enemyState == Scr_Enemy.EnemyState.Active)
        {
            if (other.gameObject.tag == "Player")
            {
                //if(!Ranged)
                //{
                    Attack();
                //}
            }
        }
        
    }

    IEnumerator RangedAttackCast(float _Duration)
    {
        bool HasAttacked = false;
       
        for (float i = 0; i < _Duration; i += Time.deltaTime)
        {

            float CastPercentage = i / AttackCastTime;
            

            if (i < AttackCastTime)
            {
                EyeSphere.GetComponent<Renderer>().material.color = Color.Lerp(StartingColor, GoalColor, EyeFadeCurve.Evaluate(i / AttackCastTime));
                EyeSphere.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.Lerp(StartingEmmission, GoalEmmission, EyeFadeCurve.Evaluate(i / AttackCastTime)));
                //Debug.Log(EyeFadeCurve.Evaluate(i / AttackCastTime));
            }
            else
            {
                EyeSphere.GetComponent<Renderer>().material.color = Color.Lerp(StartingColor, GoalColor, 0);
                EyeSphere.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.Lerp(StartingEmmission, GoalEmmission, 0));
            }
            




            transform.localScale = BaseScale * (CastCurve.Evaluate(i / _Duration) * CastCurveFactor);

            //If you reach the part in the cast you need to attack, do it
            if(i >= AttackCastTime && HasAttacked == false)
            {
                if (GetComponent<Scr_Enemy_Health>().Alive)
                {
                    //Debug.Log("Ranged Attack");
                    GameObject newProjectile = GameObject.Instantiate(ProjectilePrefab, EyeSocket.transform.position, transform.rotation);
                    newProjectile.transform.localScale = EyeSphere.transform.lossyScale;

                    newProjectile.GetComponent<Scr_EnemyProjectile>().Launch(Scr_Character_Components.inst.transform.position, ProjectileRange);

                    HasAttacked = true;

                    StartCoroutine(ReGrowEyeSphere(EyeGrowDuration));


                }
            }

            yield return null;
        }
        //Reset to proper scale after everything is done

        transform.localScale = BaseScale;
    }

    IEnumerator ReGrowEyeSphere(float _Duration)
    {
        Vector3 EyeBaseScale = EyeSphere.transform.localScale;
        EyeSphere.transform.localScale = Vector3.zero;

        

        for (float i = 0; i < _Duration; i += Time.deltaTime)
        {
            EyeSphere.transform.localScale = EyeBaseScale * EyeGrowthCurve.Evaluate((i/_Duration));

            yield return null;
        }
    }
}
