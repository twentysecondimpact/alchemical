﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scr_Flask : MonoBehaviour
{
    public Scr_Vessel vessel;
    public Scr_Chemical chemical;

    public bool GravityActive;
    public float Gravity;

    public bool Ultimate;

    public bool PerfectShot = false;

    public string name;
    [TextArea(3, 10)]
    public string info;
    [TextArea(3, 10)]
    public string perfectShotInfo;

    public float cooldown;
    
    public float maxRange;
    public float radius;
    public int MaxCharges;
    public Sprite flaskSprite;
    public Color AimReticleColor;
    public Vector3 Rotation;
    public GameObject ExplosionEffect;


    public float castTime;
    //The percentage through the cast where the bonus cast is in the middle of
    public float BonusCastPosition;
    //The time window the player has to his the bonus cast
    public float BonusCastDuration;
    //The % return on cooldown when you hit the perfect reload
    public float PerfectBonus;

    public bool HasPerfectShot;
    public bool StartsAtRadius0;

    public float RotationSpeed;

    public Quaternion TargetRotation;

    public float FlaskSpinLerpSpeed;

    public Vector3 FlaskSpin;

    public Scr_AimReticle aimReticle;

 

    private void Awake()
    {
        vessel = GetComponent<Scr_Vessel>();
        chemical = GetComponent<Scr_Chemical>();
        //Turns off the flask collider when its created, is turned on by the player when its thrown
        gameObject.GetComponent<Collider>().enabled = false;
    }

    // Use this for initialization
    void Start ()
    {
        
        UpdateArcColor();
        Gravity = Physics.gravity.y;
    }
	
	// Update is called once per frame
	void Update ()
    {
        transform.Rotate(Rotation * Time.deltaTime);

        UpdateArcColor();

        
        //if()
    }

    private void FixedUpdate()
    {
        UpdateGravity();
    }

    public void StartThrow(float _Gravity, Vector3 _TargetPosition)
    {
        Gravity = _Gravity;
        GravityActive = true;



        TargetRotation = Quaternion.LookRotation(transform.position - _TargetPosition);
        //transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles., 0, 0);
    }

    public void UpdateGravity()
    {
        if (GravityActive)
        {
            //Debug.Log(GetComponent<Rigidbody>().velocity);
            GetComponent<Rigidbody>().AddForce(new Vector3(0, Gravity, 0));
            //GetComponent<Rigidbody>().velocity += new Vector3(0, Gravity, 0);

            TargetRotation *= Quaternion.Euler(FlaskSpin * Time.deltaTime);


            transform.rotation = Quaternion.SlerpUnclamped(transform.rotation, TargetRotation, FlaskSpinLerpSpeed);
        }
    }

    public void UpdateArcColor()
    {

        aimReticle = Scr_Character_Components.inst.character_Throw.AimReticle.GetComponent<Scr_AimReticle>();

        //Debug.Log("Original Color from component: " + GetComponent<LineRenderer>().colorGradient.colorKeys[1].time);
        if (Scr_Character_Components.inst.character_Throw.PlayerThrowState == Scr_Character_Throw.ThrowState.Charging)
        {

            //Grabs the whole gradient from the line renderer
            Gradient grabbedColor = GetComponent<LineRenderer>().colorGradient;
            //Grabs the array of keys from it
            GradientColorKey[] grabbedGradientKeys = grabbedColor.colorKeys;
            //Debug.Log("Cast Time: " + castTime);
            //Debug.Log("Cast Timer: " + Scr_Character_Components.inst.character_Throw.CastTimer);

            //Sets the initial colors
            grabbedGradientKeys[0].color = Color.white;
            grabbedGradientKeys[1].color = Color.white;
            grabbedGradientKeys[2].color = AimReticleColor;
            grabbedGradientKeys[3].color = AimReticleColor;
            grabbedGradientKeys[4].color = AimReticleColor;

            //Set start is always 0
            grabbedGradientKeys[0].time = 0;

            float castTimer = Scr_Character_Components.inst.character_Throw.CastTimer;

            float bonusCastTime = castTime * (BonusCastPosition - BonusCastDuration/2);
            float bonusCastTimer = castTimer - (castTime - (castTime * (BonusCastPosition - BonusCastDuration/2)));
            //Change time based on position relative to a perfect shot
            //Debug.Log("Cast Time Max: " + castTime);
            //Debug.Log("Cast Timer: " + castTimer);


            float CastPercent = 1 - ((bonusCastTimer / bonusCastTime));// - (BonusCastPosition / castTime));

            //Debug.Log("Cast Timer: " + bonusCastTimer + " Divide by Bonus Cast Time: " + bonusCastTime + " = " + CastPercent);
            //Debug.Log(bonusCastTime);

            //Going from 0 to (castTime * BonusCastPosition)
            //float CastPercent = (castTimer / (castTime));

            //Debug.Log("Percent: " + CastPercent);

            //Debug.Log("Perfentage: " + (castTime - ((castTime / Scr_Character_Components.inst.character_Throw.CastTimer) - 0.01f)));
            grabbedGradientKeys[1].time = Mathf.Clamp01(CastPercent - 0.05f);
            grabbedGradientKeys[2].time = Mathf.Clamp01(CastPercent);
            grabbedGradientKeys[3].time = Mathf.Clamp01(CastPercent + 0.05f);

            //Set end is always 1
            grabbedGradientKeys[4].time = 1;

            //Debug.Log("Color from indervidual key: " + grabbedGradientKeys[1].time);
            grabbedColor.SetKeys(grabbedGradientKeys, grabbedColor.alphaKeys);
            //Debug.Log("Color from grabbed color: " + grabbedColor.colorKeys[1].time);
            GetComponent<LineRenderer>().colorGradient = grabbedColor;
            //grabbedGradient.colorKeys[1].time = 1;// castTime / Scr_Character_Components.inst.character_Throw.CastTimer;

            UpdateReticleColor(CastPercent);
        }
    }

    void UpdateReticleColor(float Percent)
    {
        //If the flask can have a perfect shot
        if (HasPerfectShot)
        {
            float CastTimer = Scr_Character_Components.inst.character_Throw.CastTimer;
            float PerfectTime = castTime * BonusCastPosition;
            float PerfectLeeway = (castTime * BonusCastDuration) / 2;

            float PerfectTimeMin = PerfectTime - PerfectLeeway - Scr_Character_Components.inst.character_Throw.PerfectThrowLeeway;
            float PerfectTimeMax = PerfectTime + PerfectLeeway + Scr_Character_Components.inst.character_Throw.PerfectThrowLeeway;


            //If you are on a perfect shot
            if ((castTime - CastTimer) >= PerfectTimeMin && (castTime - CastTimer) <= PerfectTimeMax)
            {
                //Perfect Shot
                Scr_Character_Components.inst.character_Throw.AimReticle.transform.GetChild(0).gameObject.GetComponent<MeshRenderer>().materials[1].color = Color.white;
                Scr_Character_Components.inst.character_Throw.AimReticle.transform.GetChild(0).gameObject.GetComponent<MeshRenderer>().materials[1].SetColor("_EmissionColor", Color.white);
            }
            //If you are not on a perfect shot
            else
            {
                Color ModifiredColor = AimReticleColor;

                ModifiredColor.a = AimReticleColor.a * aimReticle.FadeInCurve.Evaluate(aimReticle.FadePercent);

                //Missed Shot
                Scr_Character_Components.inst.character_Throw.AimReticle.transform.GetChild(0).gameObject.GetComponent<MeshRenderer>().materials[1].color = ModifiredColor;
                Scr_Character_Components.inst.character_Throw.AimReticle.transform.GetChild(0).gameObject.GetComponent<MeshRenderer>().materials[1].SetColor("_EmissionColor", ModifiredColor);

                #region Updating the fade of the arc
                //Grabs the whole gradient from the line renderer
                Gradient grabbedColor = GetComponent<LineRenderer>().colorGradient;

                GradientAlphaKey[] grabbedAlphaKeys = grabbedColor.alphaKeys;

                //Sets the initial colors
                for (int i = 0; i < grabbedAlphaKeys.Length; i++)
                {
                    grabbedAlphaKeys[i].alpha = GetComponent<LineRenderer>().colorGradient.colorKeys[i].color.a * aimReticle.FadeInCurve.Evaluate(aimReticle.FadePercent); ;
                }

                //Debug.Log("Color from indervidual key: " + grabbedGradientKeys[1].time);
                grabbedColor.SetKeys(grabbedColor.colorKeys, grabbedAlphaKeys);
                //Debug.Log("Color from grabbed color: " + grabbedColor.colorKeys[1].time);
                GetComponent<LineRenderer>().colorGradient = grabbedColor;

                #endregion


                //If you are outside of the perfect zone and the percentage is negative aka gone past
                if (Percent > 1)
                {

                    //Grabs the array of keys from it
                    GradientColorKey[] grabbedGradientKeys = grabbedColor.colorKeys;

                    //Sets the initial colors
                    for (int i = 0; i < grabbedGradientKeys.Length; i++)
                    {
                        grabbedGradientKeys[i].color = AimReticleColor;
                    }

                    //Debug.Log("Color from indervidual key: " + grabbedGradientKeys[1].time);
                    grabbedColor.SetKeys(grabbedGradientKeys, grabbedColor.alphaKeys);
                    //Debug.Log("Color from grabbed color: " + grabbedColor.colorKeys[1].time);
                    GetComponent<LineRenderer>().colorGradient = grabbedColor;
                    //grabbedGradient.colorKeys[1].time = 1;// castTime / Scr_Character_Components.inst.character_Throw.CastTimer;
                }


            }
        }
        //If it cant have a perfect shot
        else
        {
            Color ModifiredColor = AimReticleColor;

            ModifiredColor.a = AimReticleColor.a * aimReticle.FadeInCurve.Evaluate(aimReticle.FadePercent);

            //Missed Shot
            Scr_Character_Components.inst.character_Throw.AimReticle.transform.GetChild(0).gameObject.GetComponent<MeshRenderer>().materials[1].color = ModifiredColor;
            Scr_Character_Components.inst.character_Throw.AimReticle.transform.GetChild(0).gameObject.GetComponent<MeshRenderer>().materials[1].SetColor("_EmissionColor", ModifiredColor);

            #region Updating the fade of the arc
            //Grabs the whole gradient from the line renderer
            Gradient grabbedColor = GetComponent<LineRenderer>().colorGradient;

            GradientAlphaKey[] grabbedAlphaKeys = grabbedColor.alphaKeys;

            //Sets the initial colors
            for (int i = 0; i < grabbedAlphaKeys.Length; i++)
            {
                grabbedAlphaKeys[i].alpha = GetComponent<LineRenderer>().colorGradient.colorKeys[i].color.a * aimReticle.FadeInCurve.Evaluate(aimReticle.FadePercent);
            }

            //Debug.Log("Color from indervidual key: " + grabbedGradientKeys[1].time);
            grabbedColor.SetKeys(grabbedColor.colorKeys, grabbedAlphaKeys);
            //Debug.Log("Color from grabbed color: " + grabbedColor.colorKeys[1].time);
            GetComponent<LineRenderer>().colorGradient = grabbedColor;

            #endregion
        }
    }

    public void ActivatePerfectShot()
    {
        chemical.ActivatePerfectShot();
        vessel.ActivatePerfectShot();
    }

    public void ActivateCollider()
    {
        StartCoroutine(ActivateColliderDelay(0.2f));
    }

    IEnumerator ActivateColliderDelay(float _time)
    {
        yield return new WaitForSeconds(_time);

        GetComponent<Collider>().enabled = true;
    }
}
