﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Scr_FlaskTracker : MonoBehaviour {


    public List<FlaskListEntry> FlaskPickups;

    public static Scr_FlaskTracker inst;

    private void Awake()
    {
        inst = this;
    }

    // Use this for initialization
    void Start()
    {
        CreateInitialList();

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void CreateInitialList()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).gameObject.activeSelf)
            {
                FlaskPickups.Add(new FlaskListEntry(transform.GetChild(i).gameObject));
            }
        }
    }

    public void UpdateFlaskList()
    {
        for (int i = 0; i < FlaskPickups.Count; i++)
        {
            //If the enemy is disabeled upon saving, it stays that way
            if (!FlaskPickups[i].FlaskObject.activeSelf)
            {
                FlaskPickups[i].Alive = false;
            }
        }
    }

    public void LoadFlasksFromList()
    {
        for (int i = 0; i < FlaskPickups.Count; i++)
        {
            //If the enemy has been destroyed but its state hasnt been saved, spawn another one
            if (FlaskPickups[i].Alive && FlaskPickups[i].FlaskObject.activeSelf == false)
            {
                FlaskPickups[i].Reset();
            }
        }
    }

    [Serializable]
    public class FlaskListEntry
    {
        public GameObject FlaskObject;
        public Vector3 SpawningPosition;
        public Vector3 SpawningScale;
        public Quaternion SpawningRotation;

        public bool Alive;


        public FlaskListEntry(GameObject _flaskObject)
        {
            FlaskObject = _flaskObject;
            SpawningPosition = _flaskObject.transform.position;
            SpawningScale = _flaskObject.transform.localScale;
            SpawningRotation = _flaskObject.transform.rotation;
            Alive = true;
        }

        public void Reset()
        {
            FlaskObject.SetActive(true);
            FlaskObject.transform.position = SpawningPosition;
            FlaskObject.transform.localScale = SpawningScale;
            FlaskObject.transform.rotation = SpawningRotation;
        }
    }
}
