﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_Chemical : MonoBehaviour
{
    public Scr_Flask flask;
    public bool Active;
    public AudioEffectSO ChemicalAudio;

    private void Awake()
    {
        flask = gameObject.GetComponent<Scr_Flask>();
        Active = true;
    }

    // Use this for initialization
    protected virtual void Start ()
    {
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public virtual void Activate()
    {
        ChemicalAudio.Play();
    }

    public virtual void ActivatePerfectShot()
    {

    }
    

    private void OnTriggerEnter(Collider collision)
    {

        //Debug.Log("Flask hit: " + collision.gameObject.name);

        if(collision.gameObject.layer == 11)
        {
            //Debug.Log(collision.gameObject);
            //By setting an active flag, if it hits 2 object simultaniously only 1 of the activations works
            if (Active)
            {
                Activate();
                Active = false;
            }

            //Debug.Log("hit Something");
            //Destroy(this.gameObject);
        }

    }
}
