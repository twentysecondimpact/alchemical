﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_ChemicalEffect_Vortex : MonoBehaviour
{
    public bool perfect;

    public float duration;
    public float durationTimer;
    public float SpeedPercentReduction;
    public float Radius;
    public Projector projector;
    public float ExpandRate;
    public int EffectiveStacks;


    public float SuckStrength;

    public GameObject[] FlaskEffects;

    public float FadeTime;

    public Gradient NormalColor;
    public Gradient PerfectColor;
    public Gradient FadingColor;

	// Use this for initialization
	void Start ()
    {
        //Destroy after the time runs out

        durationTimer = duration;

        projector.orthographicSize = 0.001f;

        
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (durationTimer <= 0)
        {
            Destroy(this.gameObject);
        }
        else
        {
            durationTimer -= Time.deltaTime;
        }


        projector.orthographicSize = Mathf.Lerp(projector.orthographicSize, Radius, Time.deltaTime * ExpandRate);
        UpdateColor();
    }

    void UpdateColor()
    {
        ParticleSystem.MainModule grabbedMain = GetComponent<ParticleSystem>().main;
        ParticleSystem.TrailModule grabbedTrail = GetComponent<ParticleSystem>().trails;

        if (durationTimer < FadeTime)
        {
            //Debug.Log("Setting to Fade");
            grabbedMain.startColor = FadingColor;
            grabbedTrail.colorOverTrail = FadingColor;
        }
        else if (perfect)
        {
            //Debug.Log("Setting to Normal");
            grabbedMain.startColor = PerfectColor;
            grabbedTrail.colorOverTrail = PerfectColor;
        }
        else
        {
            //Debug.Log("Setting to Normal");
            grabbedMain.startColor = NormalColor;
            grabbedTrail.colorOverTrail = NormalColor;
        }
    }

    public void HitByFlask()
    {
        if(EffectiveStacks > 1)
        {
            EffectiveStacks -= 1;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }


    private void OnTriggerStay(Collider other)
    {
        
        if(other.gameObject.tag == "Enemy")
        {
            Vector3 SuckDirection = (transform.position - other.transform.position).normalized;

            other.transform.position += SuckDirection * SuckStrength * Time.deltaTime;

            if(!(other.GetComponent<Scr_Enemy_Movement>().DeAggroStateAlert || other.GetComponent<Scr_Enemy_Movement>().AggroStateAlert || other.GetComponent<Scr_Enemy_Movement>().aggroState == Scr_Enemy_Movement.AggroState.Aggroed))
            {
                other.GetComponent<Scr_Enemy_Movement>().SetStateAggro(Scr_GameDirector.inst.AggroDelay, Scr_GameDirector.inst.PassiveDelay, true);
            }
            
            ////Debug.Log("Enemy Trigger Stay");
            ////other.gameObject.GetComponent<Scr_Enemy_Movement>().Speed = other.gameObject.GetComponent<Scr_Enemy_Movement>().BaseSpeed * SpeedPercentReduction;

            //if(other.gameObject.GetComponent<Scr_Enemy_Movement>().aggroState == Scr_Enemy_Movement.AggroState.Aggroed)
            //{
            //    other.gameObject.GetComponent<Scr_Enemy_Movement>().ChangeSpeed(other.gameObject.GetComponent<Scr_Enemy_Movement>().AggroSpeed * SpeedPercentReduction);
            //}
            //else
            //{
            //    other.gameObject.GetComponent<Scr_Enemy_Movement>().ChangeSpeed(other.gameObject.GetComponent<Scr_Enemy_Movement>().PatrollingSpeed * SpeedPercentReduction);
            //    //other.gameObject.GetComponent<Scr_Enemy_Movement>().AggroPulse();
            //}
            
        }

        //if(other.gameObject.tag == "Player")
        //{
        //    //Debug.Log("Sapping player");
        //    other.gameObject.GetComponent<Scr_Character_Movement>().BaseRunSpeed = other.gameObject.GetComponent<Scr_Character_Movement>().OriginalBaseRunSpeed * SpeedPercentReduction;
        //}
    }

}
