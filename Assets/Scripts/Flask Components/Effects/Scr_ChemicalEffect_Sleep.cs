﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_ChemicalEffect_Sleep : MonoBehaviour
{

    public Color NormalColor;
    public Color PerfectColor;
    public Color FadingColor;

    public ParticleSystem particleSystem;
    public Scr_Enemy enemy;


    public float FadeTime;
    private void Awake()
    {
        particleSystem = GetComponent<ParticleSystem>();
        enemy = transform.parent.GetComponent<Scr_Enemy>();
    }

    // Use this for initialization
    void Start ()
    {
        
        
	}
	
	// Update is called once per frame
	void Update ()
    {
        ParticleSystem.MainModule grabbedMain = particleSystem.main;

        if(enemy.DisabledTimer < FadeTime)
        {
            //Debug.Log("Setting to Fade");
            grabbedMain.startColor = FadingColor;
        }
        else if (enemy.DisabledStacks > 1)
        {
            //Debug.Log("Setting to Perfect");
            grabbedMain.startColor = PerfectColor;
        }
        else
        {
            //Debug.Log("Setting to Normal");
            grabbedMain.startColor = NormalColor;
        }
	}
}
