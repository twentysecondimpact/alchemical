﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_ChemicalEffect_Sap : MonoBehaviour
{
    public float duration;
    public float durationTimer;
    public float SpeedPercentReduction;
    public float Radius;
    public Projector projector;
    public float ExpandRate;
    public int EffectiveStacks;

    public bool perfect;
    public Material NormalMaterial;
    public Material PerfectMaterial;
    public Material FadingMaterial;

    public GameObject[] FlaskEffects;

    public float FadeTime;

	// Use this for initialization
	void Start ()
    {
        durationTimer = duration;

        //Destroy after the time runs out
        Destroy(this.gameObject, duration);
        projector.orthographicSize = 0.001f;

    }
	
	// Update is called once per frame
	void Update ()
    {
        if (durationTimer <= 0)
        {
            Destroy(this.gameObject);
        }
        else
        {
            durationTimer -= Time.deltaTime;
        }

        projector.orthographicSize = Mathf.Lerp(projector.orthographicSize, Radius, Time.deltaTime * ExpandRate);

        UpdateColor();
    }

    public void HitByFlask()
    {
        if(EffectiveStacks > 1)
        {
            EffectiveStacks -= 1;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    void UpdateColor()
    {
        if(durationTimer <= FadeTime)
        {
            //Debug.Log("Changin to fading");
            //projector.material = FadingMaterial;
        }
        else if(EffectiveStacks > 1)
        {
            projector.material = PerfectMaterial;
        }
        else
        {
            projector.material = NormalMaterial;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        
        if(other.gameObject.tag == "Enemy")
        {
            //Debug.Log("Enemy Trigger Stay");
            //other.gameObject.GetComponent<Scr_Enemy_Movement>().Speed = other.gameObject.GetComponent<Scr_Enemy_Movement>().BaseSpeed * SpeedPercentReduction;

            if(other.gameObject.GetComponent<Scr_Enemy_Movement>().aggroState == Scr_Enemy_Movement.AggroState.Aggroed && other.gameObject.GetComponent<Scr_Enemy_Movement>().AggroStateAlert == false)
            {
                other.gameObject.GetComponent<Scr_Enemy_Movement>().ChangeSpeed(other.gameObject.GetComponent<Scr_Enemy_Movement>().AggroSpeed * SpeedPercentReduction);
            }
            else if (other.gameObject.GetComponent<Scr_Enemy_Movement>().AggroStateAlert == false)
            {
                other.gameObject.GetComponent<Scr_Enemy_Movement>().ChangeSpeed(other.gameObject.GetComponent<Scr_Enemy_Movement>().PatrollingSpeed * SpeedPercentReduction);
                //other.gameObject.GetComponent<Scr_Enemy_Movement>().AggroPulse();
            }
            
        }

        if(other.gameObject.tag == "Player")
        {
            //Debug.Log("Sapping player");
            other.gameObject.GetComponent<Scr_Character_Movement>().BaseRunSpeed = other.gameObject.GetComponent<Scr_Character_Movement>().OriginalBaseRunSpeed * SpeedPercentReduction;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            if (other.gameObject.GetComponent<Scr_Enemy_Movement>().aggroState == Scr_Enemy_Movement.AggroState.Aggroed)
            {
                other.gameObject.GetComponent<Scr_Enemy_Movement>().ChangeSpeed(other.gameObject.GetComponent<Scr_Enemy_Movement>().AggroSpeed);
            }
            else
            {
                other.gameObject.GetComponent<Scr_Enemy_Movement>().ChangeSpeed(other.gameObject.GetComponent<Scr_Enemy_Movement>().PatrollingSpeed);
                //other.gameObject.GetComponent<Scr_Enemy_Movement>().AggroPulse();
            }
        }

        if (other.gameObject.tag == "Player")
        {
            other.gameObject.GetComponent<Scr_Character_Movement>().BaseRunSpeed = other.gameObject.GetComponent<Scr_Character_Movement>().OriginalBaseRunSpeed;
        }
    }

    public void OnDestroy()
    {
        Collider[] hitColliders = Physics.OverlapSphere(this.transform.position, transform.localScale.x);

        //Scans all the objects in the radius, and sets em back to their original speeds.
        for (int i = 0; i < hitColliders.Length; i++)
        {
            if(hitColliders[i].gameObject.GetComponent<Scr_Enemy_Movement>() != null)
            {
                if(hitColliders[i].gameObject.GetComponent<Scr_Enemy_Movement>().aggroState == Scr_Enemy_Movement.AggroState.Aggroed)
                {
                    hitColliders[i].gameObject.GetComponent<Scr_Enemy_Movement>().ChangeSpeed(hitColliders[i].gameObject.GetComponent<Scr_Enemy_Movement>().AggroSpeed);
                }
                else
                {
                    hitColliders[i].gameObject.GetComponent<Scr_Enemy_Movement>().ChangeSpeed(hitColliders[i].gameObject.GetComponent<Scr_Enemy_Movement>().PatrollingSpeed);
                }
                
            }

            if (hitColliders[i].gameObject.GetComponent<Scr_Character_Movement>() != null)
            {
                hitColliders[i].gameObject.GetComponent<Scr_Character_Movement>().BaseRunSpeed = hitColliders[i].gameObject.GetComponent<Scr_Character_Movement>().OriginalBaseRunSpeed;
            }
        }
    }

}
