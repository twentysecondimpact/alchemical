﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_BrokenVessel : MonoBehaviour {

    public float Duration;

	// Use this for initialization
	void Start () {
        Destroy(this.gameObject, Duration);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
