﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_Vessel : MonoBehaviour
{
    public AudioEffectSO FlaskShatter;

    bool Exploded = false;

    public GameObject BrokenVessel;
    public Scr_Flask flask;

    public TooltipObject FirstUseToolTip;

    private void Awake()
    {
        flask = gameObject.GetComponent<Scr_Flask>();
    }

    // Use this for initialization
    protected virtual void Start ()
    {
    }

    public void Activate()
    {
        Debug.Log("Flask Activating");
        flask.chemical.Activate();
    }

    public virtual void ActivatePerfectShot()
    {

    }

    public void Explode()
    {
        if (Exploded == false)
        {
            //TODO come back and fix physics effects

            GameObject BrokenFlask = GameObject.Instantiate(BrokenVessel, this.transform.position, this.transform.rotation);

            //Carry over the physics from the original flash to the shattered shards
            for (int i = 0; i < BrokenFlask.transform.childCount; i++)
            {
                BrokenFlask.transform.GetChild(i).gameObject.GetComponent<Rigidbody>().velocity = this.GetComponent<Rigidbody>().velocity;
            }

            //Debug.Log("Playing Shatter nose");
            //Player flask shatter
            FlaskShatter.Play();

            //Create the eplxosion effect
            GameObject explosionEffect = GameObject.Instantiate(GetComponent<Scr_Flask>().ExplosionEffect, transform.position, Quaternion.identity);


            //Exploded used to stop it destroying multiple times while the physics engine does its thing overmultiple frames
            Exploded = true;
        }

        if(FirstUseToolTip != null)
        {
            FirstUseToolTip.Activate();
        }
        

        Destroy(this.gameObject);
    }
}
