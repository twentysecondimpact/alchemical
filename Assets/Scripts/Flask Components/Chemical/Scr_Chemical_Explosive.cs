﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_Chemical_Explosive : Scr_Chemical {

    public GameObject SapExplosionEffect;
    

    // Use this for initialization
    protected override void Start () {
        base.Start();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public override void Activate()
    {
        base.Activate();
        //Debug.Log("Deal damage function");
        //Deal damage to all enemies
        //Search out all enemies in a radius
        Collider[] collidersInRange = Physics.OverlapSphere(this.transform.position, GetComponent<Scr_Flask>().radius);


        bool LandedOnSap = false;
        float SapRadius = 0;
        Vector3 SapPosition = new Vector3();

        foreach (Collider colliderInRange in collidersInRange)
        {
            if (colliderInRange.GetComponent<Scr_ChemicalEffect_Sap>())
            {
                LandedOnSap = true;
                SapPosition = colliderInRange.GetComponent<Scr_ChemicalEffect_Sap>().transform.position;
                SapRadius = colliderInRange.GetComponent<Scr_ChemicalEffect_Sap>().Radius;
                colliderInRange.GetComponent<Scr_ChemicalEffect_Sap>().HitByFlask();
                break;
            }
        }

        if (LandedOnSap)
        {
            //Create the sap explosion effect

            GameObject SapExplosion = GameObject.Instantiate(SapExplosionEffect, SapPosition, Quaternion.identity);
            //Debug.Log("Landed in sap");
            collidersInRange = Physics.OverlapSphere(SapPosition, SapRadius);
        }
        else
        {
            //Debug.Log("Didnt Land in sap");
            collidersInRange = Physics.OverlapSphere(this.transform.position, GetComponent<Scr_Flask>().radius);
        }

        foreach (Collider colliderInRange in collidersInRange)
        {
            if(colliderInRange.tag == "Enemy")
            {
                colliderInRange.GetComponent<Scr_Enemy_Health>().TakeDamage(1);
            }

            if (colliderInRange.tag == "Player")
            {
                colliderInRange.GetComponent<Scr_Character_Health>().TakeDamage(1);
            }
        }


        flask.vessel.Explode();
    }
}
