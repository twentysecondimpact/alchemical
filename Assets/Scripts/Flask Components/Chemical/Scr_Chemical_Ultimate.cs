﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_Chemical_Ultimate : Scr_Chemical {

    public int EffectiveStacks;
    public int EffectiveStacksFromPerfect;
    public GameObject VortexPrefab;
    public float Duration;
    public float PerfectDuration;
    bool PerfectShot = false;

    float BaseRadius;

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        BaseRadius = flask.radius;

        float CastPercent = Scr_Character_Components.inst.character_Throw.CastTimer / flask.castTime;
        flask.radius = BaseRadius * (1 - CastPercent);

        Scr_Character_Components.inst.character_Throw.AimReticle.GetComponent<Scr_AimReticle>().EnableUltimate();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateRadiusWithCharge();
        if(Scr_Character_Components.inst.character_Throw.PlayerThrowState == Scr_Character_Throw.ThrowState.Neutral)
        {
            Activate();
        }
    }

    public void UpdateRadiusWithCharge()
    {
        if(Scr_Character_Components.inst.character_Throw.Flask != null)
        {
            if (Scr_Character_Components.inst.character_Throw.Flask.gameObject == this.gameObject)
            {
                //float CastPercent = Scr_Character_Components.inst.character_Throw.CastTimer / flask.castTime;
                //flask.radius = BaseRadius * (1 - CastPercent);

                ////Update the radius of the flask during the charge
                //Scr_Character_Components.inst.character_Throw.AimReticle.transform.localScale = Vector3.one * flask.radius;


                //Making up for the ultimate being on a plane
                flask.radius = BaseRadius;
                Scr_Character_Components.inst.character_Throw.AimReticle.transform.localScale = Vector3.one * flask.radius * 0.2f;
            }
        }
    }

    public override void Activate()
    {
        //Snap to aim reticle position;
        transform.position = Scr_Character_Components.inst.character_Throw.AimReticle.transform.position;
        transform.rotation = Scr_Character_Components.inst.character_Throw.AimReticle.transform.rotation;

        base.Activate();

        //Scr_Character_Components.inst.character_Throw.AimReticle.GetComponent<Scr_AimReticle>().EnableReady();

        for (int i = 0; i < GameObject.FindGameObjectsWithTag("Enemy").Length; i++)
        {
            float distance = Vector3.Distance(GameObject.FindGameObjectsWithTag("Enemy")[i].transform.position, this.transform.position);

            if(distance < GetComponent<Scr_Flask>().radius)
            {
                Scr_Character_Components.inst.character_Movement.Syl.GetComponent<Scr_Syl_Movement>().TargetEnemies.Add(GameObject.FindGameObjectsWithTag("Enemy")[i]);
            }

            
        }

        Scr_Character_Components.inst.character_Movement.Syl.GetComponent<Scr_Syl_Movement>().StartAttack();

        //Figure out a way to only active it if it its a floor or ramp
        //GameObject Vortex = GameObject.Instantiate(VortexPrefab, transform.position, Quaternion.identity);
        //Vortex.transform.localScale = new Vector3(GetComponent<Scr_Flask>().radius * 2, GetComponent<Scr_Flask>().radius * 2, GetComponent<Scr_Flask>().radius * 2);
        //Vortex.GetComponent<Scr_ChemicalEffect_Vortex>().duration = Duration;
        //Vortex.GetComponent<Scr_ChemicalEffect_Vortex>().Radius = GetComponent<Scr_Flask>().radius;
        //Vortex.GetComponent<Scr_ChemicalEffect_Vortex>().EffectiveStacks = EffectiveStacks;
        //Vortex.GetComponent<Scr_ChemicalEffect_Vortex>().perfect = PerfectShot;
        flask.vessel.Explode();
    }

    public override void ActivatePerfectShot()
    {
        base.ActivatePerfectShot();
        //Reduce Cooldown
        //EffectiveStacks = EffectiveStacksFromPerfect;
        //Duration = PerfectDuration;
        //PerfectShot = true;
    }
}
