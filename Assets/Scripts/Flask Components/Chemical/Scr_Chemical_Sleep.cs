﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_Chemical_Sleep : Scr_Chemical
{
    public int EffectiveStacks;
    public GameObject DisabledParticleEffect;
    public float DisableDuration;
    public int EffectiveStacksFromPerfectShot;
    public GameObject SapExplosionEffect;

    // Use this for initialization
    protected override void Start () {
        base.Start();
        //Start with the base stacks of 1

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public override void Activate()
    {
        base.Activate();

        Collider[] collidersInRange = Physics.OverlapSphere(this.transform.position, GetComponent<Scr_Flask>().radius);

        bool LandedOnSap = false;
        float SapRadius = 0;
        Vector3 SapPosition = new Vector3();

        foreach (Collider colliderInRange in collidersInRange)
        {
            if (colliderInRange.GetComponent<Scr_ChemicalEffect_Sap>())
            {
                LandedOnSap = true;
                SapPosition = colliderInRange.GetComponent<Scr_ChemicalEffect_Sap>().transform.position;
                SapRadius = colliderInRange.GetComponent<Scr_ChemicalEffect_Sap>().Radius;
                colliderInRange.GetComponent<Scr_ChemicalEffect_Sap>().HitByFlask();
                break;
            }
        }

        if (LandedOnSap)
        {
            GameObject SapExplosion = GameObject.Instantiate(SapExplosionEffect, SapPosition, Quaternion.identity);
            //Debug.Log("Landed in sap");
            collidersInRange = Physics.OverlapSphere(SapPosition, SapRadius);
        }
        else
        {
            //Debug.Log("Didnt Land in sap");
            collidersInRange = Physics.OverlapSphere(this.transform.position, GetComponent<Scr_Flask>().radius);
        }

        foreach (Collider colliderInRange in collidersInRange)
        {
            if (colliderInRange.tag == "Enemy")
            {
                colliderInRange.GetComponent<Scr_Enemy>().Disable(DisableDuration, EffectiveStacks);
                //Instantiate the particle object
                GameObject disabledParticleEffect = GameObject.Instantiate(DisabledParticleEffect, colliderInRange.gameObject.transform);
                //Position it about 1 unit up the enemy, this should be put in a variable
                disabledParticleEffect.transform.localPosition = new Vector3(0, 1, 0);
                //Kill the particle effect after the duration ends
                Destroy(disabledParticleEffect, DisableDuration);
            }
        }


        flask.vessel.Explode();
    }

    public override void ActivatePerfectShot()
    {
        base.ActivatePerfectShot();
        //Reduce Cooldown
        EffectiveStacks = EffectiveStacksFromPerfectShot;

    }
}
