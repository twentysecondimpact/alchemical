﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_Chemical_Syl : Scr_Chemical {

    // Use this for initialization
    protected override void Start () {
        base.Start();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public override void Activate()
    {
        base.Activate();

        //Deal damage to all enemies
        //Search out all runes in a radius
        Collider[] collidersInRange = Physics.OverlapSphere(this.transform.position, GetComponent<Scr_Flask>().radius);



        foreach (Collider colliderInRange in collidersInRange)
        {
            if (colliderInRange.tag == "Rune")
            {
                colliderInRange.GetComponent<Scr_ActivationRune>().Activate();
            }

            if(colliderInRange.tag == "Enemy")
            {
                colliderInRange.GetComponent<Scr_Enemy_Movement>().SetStateAggro(Scr_GameDirector.inst.AggroDelay, Scr_GameDirector.inst.PassiveDelay, true);
            }

        }

        flask.vessel.Explode();
    }
}
