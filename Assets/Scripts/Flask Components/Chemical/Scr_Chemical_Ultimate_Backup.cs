﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_Chemical_Ultimate_Backup : Scr_Chemical {

    public int EffectiveStacks;
    public int EffectiveStacksFromPerfect;
    public GameObject VortexPrefab;
    public float Duration;
    public float PerfectDuration;
    bool PerfectShot = false;

    float BaseRadius;

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        BaseRadius = flask.radius;

        float CastPercent = Scr_Character_Components.inst.character_Throw.CastTimer / flask.castTime;
        flask.radius = BaseRadius * (1 - CastPercent);


    }

    // Update is called once per frame
    void Update()
    {
        UpdateRadiusWithCharge();
    }

    public void UpdateRadiusWithCharge()
    {
        if(Scr_Character_Components.inst.character_Throw.Flask != null)
        {
            if (Scr_Character_Components.inst.character_Throw.Flask.gameObject == this.gameObject)
            {
                float CastPercent = Scr_Character_Components.inst.character_Throw.CastTimer / flask.castTime;
                flask.radius = BaseRadius * (1 - CastPercent);

                //Update the radius of the flask during the charge
                Scr_Character_Components.inst.character_Throw.AimReticle.transform.localScale = Vector3.one * flask.radius;
            }
        }
        
        
    }

    public override void Activate()
    {
        base.Activate();

        //Figure out a way to only active it if it its a floor or ramp
        GameObject Vortex = GameObject.Instantiate(VortexPrefab, transform.position, Quaternion.identity);
        Vortex.transform.localScale = new Vector3(GetComponent<Scr_Flask>().radius * 2, GetComponent<Scr_Flask>().radius * 2, GetComponent<Scr_Flask>().radius * 2);
        Vortex.GetComponent<Scr_ChemicalEffect_Vortex>().duration = Duration;
        Vortex.GetComponent<Scr_ChemicalEffect_Vortex>().Radius = GetComponent<Scr_Flask>().radius;
        Vortex.GetComponent<Scr_ChemicalEffect_Vortex>().EffectiveStacks = EffectiveStacks;
        Vortex.GetComponent<Scr_ChemicalEffect_Vortex>().perfect = PerfectShot;
        flask.vessel.Explode();
    }

    public override void ActivatePerfectShot()
    {
        base.ActivatePerfectShot();
        //Reduce Cooldown
        EffectiveStacks = EffectiveStacksFromPerfect;
        Duration = PerfectDuration;
        PerfectShot = true;
    }
}
