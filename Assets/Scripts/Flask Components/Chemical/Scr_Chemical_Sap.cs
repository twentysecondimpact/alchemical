﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_Chemical_Sap : Scr_Chemical
{
    public int EffectiveStacks;
    public int EffectiveStacksFromPerfect;
    public GameObject SapPoolPrefab;

    // Use this for initialization
    protected override void Start () {
        base.Start();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public override void Activate()
    {
        base.Activate();

        //Figure out a way to only active it if it its a floor or ramp
        GameObject SapPool = GameObject.Instantiate(SapPoolPrefab, transform.position, Quaternion.identity);
        SapPool.transform.localScale = new Vector3(GetComponent<Scr_Flask>().radius * 2, SapPool.transform.localScale.y, GetComponent<Scr_Flask>().radius * 2);
        SapPool.GetComponent<Scr_ChemicalEffect_Sap>().Radius = GetComponent<Scr_Flask>().radius;
        SapPool.GetComponent<Scr_ChemicalEffect_Sap>().EffectiveStacks = EffectiveStacks;
        flask.vessel.Explode();
    }

    public override void ActivatePerfectShot()
    {
        base.ActivatePerfectShot();
        //Reduce Cooldown
        EffectiveStacks = EffectiveStacksFromPerfect;

    }
}
