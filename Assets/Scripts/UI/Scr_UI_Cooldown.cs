﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Scr_UI_Cooldown : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    public int FlaskIndex;

    public Image FlaskSprite;
    public Image Progress;
    public Text Cooldown;
    public Text HotKey;
    public Text Charges;

    public Color UnusableCooldownColor;
    public Color UsableCooldownColor;

    public float CooldownPercent;
    public float ProgressBaseHeight;

    public GameObject ToolTip;

    //Tooltip Variables

    private void Awake()
    {
    }

    // Use this for initialization
    void Start ()
    {
        //Starts the proges fill at 0
        //Progress.fillAmount = 0;

        //Should take this out of update and just have it called when changed
        UpdateFlaskImage();
        ProgressBaseHeight = Progress.rectTransform.localScale.y;
        UpdateHotKey();
	}
	
	// Update is called once per frame
	void Update ()
    {
        UpdateFlaskImage();
        UpdateCharges();

        if (Scr_Character_Components.inst.character_Throw.FlaskPrefab[FlaskIndex] != null)
        {
            UpdateCooldown();
        }
        else
        {
            Cooldown.text = "";
            Progress.fillAmount = 1;
        }

        //If the game goes outside the game, reset cooldowns
        //if(Scr_GameDirector.inst)
        //{
        //    Progress.fillAmount = 0;
        //}
    }

    public void UpdateHotKey()
    {
        HotKey.text = Scr_Character_Components.inst.character_Throw.FlaskKeybinds[FlaskIndex].ToString();
    }

    void UpdateCharges()
    {
        if (Scr_Character_Components.inst.character_Throw.FlaskPrefab[FlaskIndex] != null)
        {
            if(Scr_Character_Components.inst.character_Throw.FlaskPrefab[FlaskIndex].GetComponent<Scr_Flask>().MaxCharges > 1)
            {
                if (Scr_Character_Components.inst.character_Throw.FlaskCharges[FlaskIndex] > 0)
                {
                    Charges.text = Scr_Character_Components.inst.character_Throw.FlaskCharges[FlaskIndex].ToString();
                }
                else
                {
                    Charges.text = "";
                }
            }
            else
            {
                Charges.text = "";
            }
        }
        else
        {
            Charges.text = "";
        }
    }

    public void UpdateFlaskImage()
    {
            if (Scr_Character_Components.inst.character_Throw.FlaskPrefab[FlaskIndex] != null)
            {
                FlaskSprite.enabled = true;
                FlaskSprite.sprite = Scr_Character_Components.inst.character_Throw.FlaskPrefab[FlaskIndex].GetComponent<Scr_Flask>().flaskSprite;
            }
            else
            {
                FlaskSprite.enabled = false;
                //FlaskSprite.sprite = null;
            } 
    }

    void UpdateCooldown()
    {
        if(Scr_Character_Components.inst.character_Throw.FlaskCooldowns[FlaskIndex] > 0)
        {
            //Cooldown.text = ((int)Scr_Character_Components.inst.character_Throw.FlaskCooldowns[FlaskIndex] + 1).ToString();

            CooldownPercent = Scr_Character_Components.inst.character_Throw.FlaskCooldowns[FlaskIndex] / Scr_Character_Components.inst.character_Throw.FlaskPrefab[FlaskIndex].GetComponent<Scr_Flask>().cooldown;
            //Progress.rectTransform.localScale = new Vector3(Progress.rectTransform.localScale.x, (ProgressBaseHeight * CooldownPercent), Progress.rectTransform.localScale.z);
            Progress.fillAmount = CooldownPercent;

            if(Scr_Character_Components.inst.character_Throw.FlaskCharges[FlaskIndex] == 0)
            {
                Progress.color = UnusableCooldownColor;
            }
            else
            {
                Progress.color = UsableCooldownColor;
            }
        }
        else
        {
            //Cooldown.text = "";

            CooldownPercent = 0;
            //Progress.rectTransform.localScale = new Vector3(Progress.rectTransform.localScale.x, 0, Progress.rectTransform.localScale.z);
            Progress.fillAmount = 0;
            //Set the color to usable
            Progress.color = UsableCooldownColor;
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (Scr_Character_Components.inst.character_Throw.FlaskPrefab[FlaskIndex] != null)
        {
            GetComponent<Scr_FlaskToolTip>().UpdateTooltip();
            ToolTip.SetActive(true);
        }
    }
    

    public void OnPointerExit(PointerEventData eventData)
    {
        if (Scr_Character_Components.inst.character_Throw.FlaskPrefab[FlaskIndex] != null)
        {
            GetComponent<Scr_FlaskToolTip>().UpdateTooltip();
            ToolTip.SetActive(false);
        }
    }
}
