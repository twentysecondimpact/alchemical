﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Scr_HintToolTip : MonoBehaviour
{

    public static Scr_HintToolTip inst;

    public TMP_Text TextMesh;
    public Image TextBacking;

    public TMP_Text ButtonText;
    public Image ButtonBacking;

    public float FadePercent;
    public float FadeInSpeed;
    public float FadeOutSpeed;

    public Image CharacterIcon;

    public bool Active;

    public bool PausedOnTooltip;

    public string[] CurrentToolTip;
    public int TooltipIndex;

    public float FullOpacity;

    public List<string> ShownOneshotTooltips;

    private void Awake()
    {
        inst = this;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {


        if (Active)
        {
            FadePercent = Mathf.Clamp(FadePercent + (FadeInSpeed * Time.unscaledDeltaTime), 0, FullOpacity);
        }
        else
        {
            FadePercent = Mathf.Clamp(FadePercent - (FadeOutSpeed * Time.unscaledDeltaTime), 0, FullOpacity);
        }

        //ToolTip Backing Color
        Color GrabbedColor = TextBacking.color;
        GrabbedColor.a = FadePercent;
        TextBacking.color = GrabbedColor;

        //Tooltip Text color
        GrabbedColor = TextMesh.color;
        GrabbedColor.a = FadePercent;
        TextMesh.color = GrabbedColor;

        //Button Backing Color
        GrabbedColor = ButtonBacking.color;
        GrabbedColor.a = FadePercent;
        ButtonBacking.color = GrabbedColor;

        //Button Text color
        GrabbedColor = ButtonText.color;
        GrabbedColor.a = FadePercent;
        ButtonText.color = GrabbedColor;

        //Fade Character Icon
        GrabbedColor = CharacterIcon.color;
        GrabbedColor.a = FadePercent;
        CharacterIcon.color = GrabbedColor;

        if (TooltipIndex == CurrentToolTip.Length - 1)
        {
            ButtonText.text = "Close";
        }
        else
        {
            ButtonText.text = "Next";
        }

        

    }

    private void LateUpdate()
    {
        if (Active)
        {
            UpdateToolTipState();
        }
    }

    public void Activate(TooltipObject _toolTip)
    {
        //checks if it isnt in the list of one shots that have already played
        if (!ShownOneshotTooltips.Contains(_toolTip.name))
        {
            //If it isnt then add it to the list 
            if (_toolTip.OneShot)
            {
                ShownOneshotTooltips.Add(_toolTip.name);
            }

            //Cancle throw if yo uare throwing and if time stops
            if (Scr_Character_Components.inst.character_Throw.PlayerThrowState == Scr_Character_Throw.ThrowState.Charging && _toolTip.PauseOnToolTip)
            {
                Scr_Character_Components.inst.character_Throw.CancelThrow();
            }

            TooltipIndex = 0;
            CurrentToolTip = _toolTip.ToolTipText;
            TextMesh.text = CurrentToolTip[TooltipIndex];
            Active = true;

            if (_toolTip.PauseOnToolTip)
            {
                PauseOnToolTip();
            }

            CharacterIcon.sprite = _toolTip.CharacterIcon;

        }

        
    }

    public void Deactive()
    {
        Active = false;
        Scr_GameDirector.inst.StartTime();
        
    }

    public void UpdateToolTipState()
    {
        //Removes the tooltip pausing
        
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ContinueToolTip();
            
        }
        
    }

    public void ContinueToolTip()
    {
        //If you have reached the end of the tooltip
        if (TooltipIndex >= CurrentToolTip.Length - 1)
        {
            if (PausedOnTooltip)
            {
                Scr_GameDirector.inst.gameState = Scr_GameDirector.GameState.InGame;

                //Scr_Character_Components.inst.character_Movement.UnpauseCharacterMovement();
            }
            Deactive();
        }
        else
        {
            //Go to the next tooltip
            TooltipIndex++;
            TextMesh.text = CurrentToolTip[TooltipIndex];
        }
    }

    public void PauseOnToolTip()
    {
        PausedOnTooltip = true;
        Scr_GameDirector.inst.gameState = Scr_GameDirector.GameState.Dialogue;

        Scr_Character_Components.inst.character_Movement.PauseCharacterMovement();


        Scr_GameDirector.inst.StopTime();
    }
}
