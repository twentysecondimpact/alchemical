﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scr_UI_Fader : MonoBehaviour
{
    public float FadeSpeed;
    public float FadeTime;
    public float FadePercent;

    public bool Active;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Active)
        {
            //Update Fade
            FadePercent = Mathf.Clamp01(FadePercent + ((Time.deltaTime / FadeTime) * FadeSpeed));

            Color grabbedColor = GetComponent<Image>().color;
            grabbedColor.a = FadePercent;
            GetComponent<Image>().color = grabbedColor;

            if(FadePercent == 0 || FadePercent == 1)
            {
                Active = false;
            }
        }


    }

    public void Activate(int _Speed)
    {
        //Debug.Log("Activating");
        Active = true;
        FadeSpeed = _Speed;
    }

    //IEnumerator FadeOut()
    //{
    //    for (float i = 0; i < FadeTime; i += Time.deltaTime)
    //    {
    //        FadePercent += Time.deltaTime / FadeTime;

    //        if (FadePercent >= 0)
    //        {
    //            break;
    //        }

    //        yield return null;
    //    }
    //}

    //IEnumerator FadeIn()
    //{
    //    for (float i = 0; i < FadeTime; i += Time.deltaTime)
    //    {
    //        FadePercent -= Time.deltaTime / FadeTime;

    //        if(FadePercent <= 0)
    //        {
    //            break;
    //        }

    //        yield return null;
    //    }
    //}
}
