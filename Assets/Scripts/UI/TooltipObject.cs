﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

[CreateAssetMenu()]
public class TooltipObject: ScriptableObject
{
    public string Name;
    [TextArea(3, 10)]
    public string[] ToolTipText;
    public bool PauseOnToolTip;
    public bool ShowToolTip;
    public bool OneShot;
    public Sprite CharacterIcon;
    

    public void Activate()
    {
        if(ShowToolTip)
        {
            Scr_HintToolTip.inst.Activate(this);
        }
    }
}
