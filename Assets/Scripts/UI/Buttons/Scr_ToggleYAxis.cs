﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scr_ToggleYAxis : MonoBehaviour
{

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void UpdateYInvert()
    {
        Scr_FollowCamera.inst.SetYInvert(GetComponent<Toggle>().isOn);

    }
}
