﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scr_ToggleKeyboardTurn : MonoBehaviour
{

	// Use this for initialization
	void Start () {
        //UpdateTurnWithCamera();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void UpdateTurnWithCamera()
    {
        Scr_FollowCamera.inst.UpdateTurnWithKeyboard(GetComponent<Toggle>().isOn);
        
    }
}
