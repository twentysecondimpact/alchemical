﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Scr_HintRune : MonoBehaviour
{
    public Scr_HintRune NextHintRune;

    public bool Active;

    public float FadePercent;
    public float FadeInSpeed;
    public float FadeOutSpeed;

    public GameObject TipGhost;

    public float TipGhostFloatNoise;

    public AudioEffectSO AESO_Activate;

    //If you have been stepped on once by the player and saved the game
    public bool Activated;
    public GameObject ActivatedVisual;
    public GameObject NotActivatedVisual;

    public bool Primed;

    public TooltipObject TTSO_Tombstones;
    public TooltipObject TTSO_Hint;


	// Use this for initialization
	void Start () {
        //Needs to go somewhere else but eh
        //Tooltip = Scr_GameDirector.inst.menuManager.HintToolTip;

        if (Primed)
        {
            NotActivatedVisual.SetActive(true);
        }

    }
	
	// Update is called once per frame
	void Update ()
    {
        UpdateFadePercent();

        //Bobbing
        TipGhost.transform.localPosition += new Vector3(Mathf.Sin(Time.time), Mathf.Sin(Time.time * 1.8f), 0) * TipGhostFloatNoise * Time.deltaTime;
    }

    public void UpdateFadePercent()
    {
        if (Active)
        {
            FadePercent = Mathf.Clamp01(FadePercent + (FadeInSpeed * Time.unscaledDeltaTime));
        }
        else
        {
            FadePercent = Mathf.Clamp01(FadePercent - (FadeOutSpeed * Time.unscaledDeltaTime));
        }

        Color GrabbedColor = TipGhost.GetComponent<MeshRenderer>().material.color;
        GrabbedColor.a = FadePercent;
        TipGhost.GetComponent<MeshRenderer>().material.color = GrabbedColor;

        
    }

    private void OnTriggerEnter(Collider other)
    {
        
        //if (Primed)
        //{
        if (other.tag == "Player")
        {
            //Set the tombstone to have been activated, changing its visual
            if (!Activated)
            {
                Activate();
            }
        Active = true;

        if (!Scr_HintToolTip.inst.ShownOneshotTooltips.Contains(TTSO_Tombstones.name))
        {
            TTSO_Tombstones.Activate();
        }
        else
        {
            TTSO_Hint.Activate();

        }
        Save();
        }

            
        //}

    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            //DisableText();
            Scr_HintToolTip.inst.Deactive();
            Active = false;
        }
    }

    public void EnableText()
    {
        if (Active == false)
        {
            AESO_Activate.Play();

            //Set the tombstone to be active
            Active = true;
        }
    }

    public void DisableText()
    {
        
        if (Active == true)
        {
            Active = false;
        }
    }

    public void Prime()
    {
        NotActivatedVisual.SetActive(true);
        Primed = true;

        //Scr_Character_Components.inst.character_Health.RestartCharacterHealth();
    }

    void Activate()
    {

        

        Activated = true;
        NotActivatedVisual.SetActive(false);
        ActivatedVisual.SetActive(true);

        if (NextHintRune != null)
        {
            NextHintRune.Prime();
        }
        
        
    }

    void Save()
    {

        
        Scr_GameDirector.inst.SaveGame();

    }
}
