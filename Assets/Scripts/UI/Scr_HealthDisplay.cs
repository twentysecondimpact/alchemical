﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Scr_HealthDisplay : MonoBehaviour {

    public static Scr_HealthDisplay inst;
    Scr_Character_Health charHealth;
    float FullBarWidth;
    public RectTransform BarMask;
    public TMP_Text HealthText;
    public float CurrentRegenBarPercentage;
    

    public float BarLerpSpeed;

    public RectTransform Heart;

    private void Awake()
    {
        inst = this;
    }

    // Use this for initialization
    void Start ()
    {
        //StartingScale = Heart.localScale;
        FullBarWidth = BarMask.rect.width;
        charHealth = Scr_Character_Components.inst.character_Health;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(charHealth.Lives > 0)
        {
            HealthText.text = (charHealth.Lives - 1).ToString();
        }
        else
        {
            HealthText.text = "";
        }
        



        
        BarMask.localScale = new Vector3(Mathf.Lerp(BarMask.localScale.x,CurrentRegenBarPercentage, BarLerpSpeed * Time.deltaTime), BarMask.localScale.y, BarMask.localScale.z);
    }

    public void UpdateHealth()
    {
        //Play heart beat thingo

        if ((float)charHealth.Health > 0)
        {
            CurrentRegenBarPercentage = (float)charHealth.Health / (float)charHealth.MaxHealth;
        }
        else
        {
            CurrentRegenBarPercentage = 0;
        }
    }
}
