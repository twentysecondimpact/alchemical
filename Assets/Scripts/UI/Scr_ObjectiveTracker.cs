﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Scr_ObjectiveTracker : MonoBehaviour
{

    public static Scr_ObjectiveTracker inst;

    public int CurrentObjective;
    public List<string> ObjectTexts;

    public TMP_Text ObjectTextElement;

    void Awake()
    {
        inst = this;
    }

	// Use this for initialization
	void Start () {
        CurrentObjective = 0;
        UpdateObjectText(0);
	}
	
	// Update is called once per frame
	void Update ()
    {

    }

    public void UpdateObjectText(int _Increment)
    {
        CurrentObjective += _Increment;
        ObjectTextElement.text = ObjectTexts[CurrentObjective];
    }
}
