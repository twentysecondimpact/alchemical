﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Scr_UI_LevelOverPanel : MonoBehaviour
{
    public Scr_Score Score;

    public Text ComboScore;
    public Text TimeScore;
    public Text EfficiencyScore;
    public Text TotalScore;

    public Image Star1;
    public Image Star2;
    public Image Star3;

    public Button SubmitButton;
    public TMP_InputField InputField;

    public TMP_Text Grade;

    public int FinalScore;

	// Use this for initialization
	void Start ()
    {
        //Score = Scr_GameDirector.inst.scoreSystem;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {
            //PlayerPrefs.DeleteAll();
            //PlayerPrefs.Save();
        }
	}

    public void CalculateLevelScore()
    {
        //Adds the current score with what ever combo you happen to have right now
        float FinalComboScore = Score.CurrentScore + (Score.ComboPool * Score.ComboMultiplier);
        ComboScore.text = ((int)FinalComboScore).ToString();

        float FinalTimeScore = Score.TimeBonusBase - (Scr_GameDirector.inst.LevelTimer * Score.TimeBonusFactor);
        TimeScore.text = ((int)FinalTimeScore).ToString();

        float FinalEffiencyScore = Score.EfficiencyBonusBase - (Scr_GameDirector.inst.FlasksUsed * Score.EfficiencyBonusFactor);
        EfficiencyScore.text = ((int)FinalEffiencyScore).ToString();

        float FinalTotalScore = FinalComboScore + FinalTimeScore + FinalEffiencyScore;
        FinalScore = (int)FinalTotalScore;
        TotalScore.text = ((int)FinalTotalScore).ToString();

        Grade.text = Scr_MainMenuController.ScoreGrade(FinalScore);
    }

    public void SubmitScore()
    {

        //PlayerPrefs.SetInt("DummyScore", 578);
        //PlayerPrefs.Save();

        //Debug.Log(PlayerPrefs.GetInt("DummyScore"));

        int ScoreIndex = 0;

        //Find how many entires exist 
        if(PlayerPrefs.HasKey("Scores") == true)
        {
            ScoreIndex = PlayerPrefs.GetInt("Scores");
            Debug.Log("Setting score index to: " + ScoreIndex);
        }
        //if no entires exist make one
        else
        {
            PlayerPrefs.SetInt("Scores", 0);
            //Saves the updated playerprefs
            PlayerPrefs.Save();
            Debug.Log("No Scores found, setting to 0");
        }

        //Set the current score index name and score
        PlayerPrefs.SetInt("ScoreNumber_" + ScoreIndex, FinalScore);
        PlayerPrefs.SetString("ScoreName_" + ScoreIndex, InputField.text);

        //Increment the number of saved scores;
        PlayerPrefs.SetInt("Scores", ScoreIndex + 1);

        //Saves the updated playerprefs
        PlayerPrefs.Save();

        for (int i = 0; i < PlayerPrefs.GetInt("Scores"); i++)
        {
            Debug.Log(PlayerPrefs.GetString("ScoreName_" + i.ToString()));
            Debug.Log(PlayerPrefs.GetInt("ScoreNumber_" + i.ToString()));
        }

        SubmitButton.transform.GetChild(0).GetComponent<Text>().text = "Score Saved";
        SubmitButton.enabled = false;
    }
}
