﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Scr_UI_TextPanel : MonoBehaviour
{
    public string DemoMessage;

	// Use this for initialization
	void Start ()
    {
        StartCoroutine(DisplayMessage(DemoMessage, 0.1f));
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    IEnumerator DisplayMessage(string _Message, float _WaitBetweenChar)
    {
        //Creates a char array of the size of the message
        //int CurrentIndex = 0;
        string DisplayedMessage = "";
        string NewLine = "\n";

        //Makes every chracter in it a blank character
        for (int i = 0; i < _Message.Length; i++)
        {
            if(_Message[i].ToString() == "@")
            {
                DisplayedMessage += NewLine;
            }
            else
            {
                DisplayedMessage += _Message[i];
            }

            GetComponent<TMP_Text>().text = DisplayedMessage;
            yield return new WaitForSeconds(_WaitBetweenChar);
        }
    }
}
