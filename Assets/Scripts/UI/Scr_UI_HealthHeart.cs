﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scr_UI_HealthHeart : MonoBehaviour
{
    public Sprite sprite_FullHeart;
    public Sprite sprite_HalfHeart;
    public Sprite sprite_EmptyHeart;

    public int FullHeartValue;
    public int HalfHeartValue;

    public void UpdateHealth()
    {
        //Debug.Log("Update Health Heart");
        if(Scr_Character_Components.inst.character_Health.Health >= FullHeartValue)
        {
            this.GetComponent<Image>().enabled = true;
            this.GetComponent<Image>().sprite = sprite_FullHeart;
        }
        else if(Scr_Character_Components.inst.character_Health.Health >= HalfHeartValue)
        {
            this.GetComponent<Image>().enabled = true;
            this.GetComponent<Image>().sprite = sprite_HalfHeart;
        }
        else
        {
            this.GetComponent<Image>().enabled = true;
            this.GetComponent<Image>().sprite = sprite_EmptyHeart;
        }
    }
}
