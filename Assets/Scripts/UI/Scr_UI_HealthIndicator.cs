﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_UI_HealthIndicator : MonoBehaviour
{
    public static Scr_UI_HealthIndicator inst;
    public List<Scr_UI_HealthHeart> HealthHearts;

    private void Awake()
    {
        inst = this;
    }

    // Use this for initialization
    void Start ()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            HealthHearts.Add(transform.GetChild(i).gameObject.GetComponent<Scr_UI_HealthHeart>());
        }

        //UpdateHealth();
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void UpdateHealth()
    {
        for (int i = 0; i < HealthHearts.Count; i++)
        {
            HealthHearts[i].UpdateHealth();
        }
    }
}
