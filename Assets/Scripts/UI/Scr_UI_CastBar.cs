﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scr_UI_CastBar : MonoBehaviour
{

    public float FullBarWidth;

    public RectTransform BarMask;
    public RectTransform BackingBar;

    public RectTransform BonusBar;

    public float CastPercentage;

	// Use this for initialization
	void Start ()
    {
        FullBarWidth = BarMask.rect.width;

    }
	
	// Update is called once per frame
	void Update ()
    {
        //If you are charging a throw and the throw has a perfect shot
        if(Scr_Character_Components.inst.character_Throw.PlayerThrowState == Scr_Character_Throw.ThrowState.Charging && Scr_Character_Components.inst.character_Throw.Flask.GetComponent<Scr_Flask>().HasPerfectShot)
        {
            //Ghetto display toggling
            BarMask.gameObject.SetActive(true);
            BackingBar.gameObject.SetActive(true);
            BonusBar.gameObject.SetActive(true);

            if (Scr_Character_Components.inst.character_Throw.Flask != null)
            {
                CastPercentage = Scr_Character_Components.inst.character_Throw.CastTimer / Scr_Character_Components.inst.character_Throw.Flask.GetComponent<Scr_Flask>().castTime;
                BarMask.localScale = new Vector3(1 - CastPercentage, BarMask.localScale.y, BarMask.localScale.z);

                ////Set the position of the bonus bar
                Vector3 grabbedPosition = BonusBar.localPosition;
                //Debug.Log("bonus Position: " + Scr_Character_Components.inst.character_Throw.Flask.GetComponent<Scr_Flask>().BonusCastPosition);
                //Debug.Log(FullBarWidth);
                grabbedPosition.x = (FullBarWidth * Scr_Character_Components.inst.character_Throw.Flask.GetComponent<Scr_Flask>().BonusCastPosition) - (FullBarWidth / 2);
                BonusBar.localPosition = grabbedPosition;

                BonusBar.localScale = new Vector3(Scr_Character_Components.inst.character_Throw.Flask.GetComponent<Scr_Flask>().BonusCastDuration, 1, 1);
            }
        }
        else
        {
            //Ghetto display toggling
            BarMask.gameObject.SetActive(false);
            BackingBar.gameObject.SetActive(false);
            BonusBar.gameObject.SetActive(false);

            BarMask.localScale = new Vector3(0, BarMask.localScale.y, BarMask.localScale.z);// new Vector3(CastPercentage, BarMask.localScale.y, BarMask.localScale.z);
        }
	}
}
