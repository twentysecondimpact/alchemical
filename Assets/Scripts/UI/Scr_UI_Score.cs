﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;


public class Scr_UI_Score : MonoBehaviour
{

    public static Scr_UI_Score inst;

    public TMP_Text CurrentScore;
    public TMP_Text ComboPool;
    public TMP_Text ComboMultiplier;
    public TMP_Text ShroomCounter;

    private void Awake()
    {
        inst = this;
    }

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        CurrentScore.text = Scr_GameDirector.inst.scoreSystem.CurrentScore.ToString();
        ComboPool.text = Scr_GameDirector.inst.scoreSystem.ComboPool.ToString();

        if(Scr_GameDirector.inst.scoreSystem.ComboMultiplier > 0)
        {
            ComboMultiplier.text = "x" + Scr_GameDirector.inst.scoreSystem.ComboMultiplier.ToString();
        }
        else
        {
            ComboMultiplier.text = "";
        }
        
    }

    public void InitializeShroomCounter(int _NumOfShrooms)
    {
        ShroomCounter.text = _NumOfShrooms + " / " + _NumOfShrooms;

    }

    public void UpdateShroomCounter()
    {
        int NumberKilled = 0;

        for (int i = 0; i < Scr_EnemyTracker.inst.Enemies.Count; i++)
        {
            if(Scr_EnemyTracker.inst.Enemies[i].EnemyComponent.GetComponent<Scr_Enemy_Health>().Alive)
            {
                NumberKilled++;
            }
        }

        ShroomCounter.text = NumberKilled + " / " + Scr_EnemyTracker.inst.Enemies.Count.ToString();
    }
}
