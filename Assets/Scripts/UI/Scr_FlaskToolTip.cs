﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Scr_FlaskToolTip : MonoBehaviour
{
    public Scr_UI_Cooldown CooldownUI;
    //public Scr_Flask Flask;
    public GameObject ToolTipPanel;

    public TMP_Text Name;
    public TMP_Text Info;
    public TMP_Text Range;
    public TMP_Text Cooldown;
    public TMP_Text PerfectShotInfo;

    // Use this for initialization
    void Start()
    {
        

    }
	
	// Update is called once per frame
	void Update ()
    {
        //UpdateTooltip();
    }

    public void UpdateTooltip()
    {
        if (Scr_Character_Components.inst.character_Throw.FlaskPrefab[CooldownUI.FlaskIndex] != null)
        {
            Scr_Flask flask = Scr_Character_Components.inst.character_Throw.FlaskPrefab[CooldownUI.FlaskIndex].GetComponent<Scr_Flask>();

            //Name.text = flask.name;
            //Info.text = flask.info;
            Range.text = "Range: " + flask.maxRange.ToString();
            Cooldown.text = "Cooldown: " + flask.cooldown.ToString();
            //PerfectShotInfo.text = flask.perfectShotInfo;
        }


    }
}
