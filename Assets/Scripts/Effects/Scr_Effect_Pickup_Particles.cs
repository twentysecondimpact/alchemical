﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_Effect_Pickup_Particles : MonoBehaviour
{

    ParticleSystem.Particle[] particles = new ParticleSystem.Particle[0];
    ParticleSystem ps;

    public AnimationCurve SuckCurve;

    public Vector3 CharacterSuckOffset;
    public float ParticleSuckStrength;
    public float StartingSuckStrength;
    float currentSuckStrength;

    public float LerpStrength;

	// Use this for initialization
	void Start () {
        currentSuckStrength = StartingSuckStrength;
	}
	
	// Update is called once per frame
	void LateUpdate ()
    {
        InitializeIfNeeded();

        VelocityChangeMethod();
    }

    void VelocityChangeMethod()
    {
        // GetParticles is allocation free because we reuse the m_Particles buffer between updates
        int numParticlesAlive = ps.GetParticles(particles);

        // Change only the particles that are alive
        for (int i = 0; i < numParticlesAlive; i++)
        {
            //particles[i].position = Vector3.LerpUnclamped(particles[i].position, (Scr_Character_Components.inst.transform.position + CharacterSuckOffset), (1 - (particles[i].remainingLifetime / particles[i].startLifetime)));

            Vector3 TargetPosition = (Scr_Character_Components.inst.character_Movement.Syl.transform.position + CharacterSuckOffset);

            currentSuckStrength += ParticleSuckStrength * Time.deltaTime;
            Vector3 Direction = (TargetPosition - particles[i].position) * currentSuckStrength;
            if (i == 0)
            {
                //Debug.Log("Particle Posiiton: " + particles[i].position);
                //Debug.Log("Character Position: " + Scr_Character_Components.inst.transform.position);
                //Debug.Log(Direction);
            }
            particles[i].velocity += Direction;

        }

        // Apply the particle changes to the particle system
        ps.SetParticles(particles, numParticlesAlive);
    }

    void InitializeIfNeeded()
    {
        if (ps == null)
            ps = GetComponent<ParticleSystem>();

        if (ps == null || particles.Length < ps.main.maxParticles)
            particles = new ParticleSystem.Particle[ps.main.maxParticles];
    }
}
