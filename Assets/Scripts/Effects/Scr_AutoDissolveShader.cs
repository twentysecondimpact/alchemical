﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_AutoDissolveShader : MonoBehaviour {

    Material mat;
    public float Duration;
    float durationTimer;

    //public float Delay;

    private void Start()
    {
        mat = GetComponent<Renderer>().material;
        durationTimer = Duration;
    }

    private void Update()
    {
        durationTimer -= Time.deltaTime;
        mat.SetFloat("_DissolveAmount", 1 - (durationTimer / Duration));

        if(durationTimer <= 0)
        {
            Destroy(this.gameObject);
        }
    }
}
