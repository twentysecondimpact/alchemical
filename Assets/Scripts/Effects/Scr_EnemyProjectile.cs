﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_EnemyProjectile : MonoBehaviour {

    public bool GravityActive;
    float AppliedGravity;
    public float gravity;
    public Rigidbody rb;

    public float ArkHeight;
    public float ArkHeightDistanceFactor;
    public float MinArkHeight;
    public float BaseArcHeightIncrease;

    public bool Active;

    public GameObject ExplosionEffect;
    public GameObject AimReticlePrefab;
    public GameObject AimReticle;

    public float DamageRadius;

    public float ParticleSizeModifier;

    public int ProjectileDamage;

    // Use this for initialization
    void Start () {
        Active = true;
	}
	
	// Update is called once per frame
	void Update () {
        
    }

    public void Launch(Vector3 _TargetPosition, float _MaxRange)
    {
        rb = GetComponent<Rigidbody>();

        //Sebastion Throw Code
        AppliedGravity = gravity;
        GravityActive = true;

        DetermineArcHeight(_TargetPosition, _MaxRange);


        //Sets the initial velocity for the flask when thrown
        rb.velocity = CalculateLaunchData(_TargetPosition, transform.position).initialVelocity;

        //Spawn the reticle
        AimReticle = GameObject.Instantiate(AimReticle, _TargetPosition, Quaternion.identity);
        //Scale the reticle
        AimReticle.transform.GetChild(0).GetComponent<Projector>().orthographicSize = DamageRadius;


    }

    public void DetermineArcHeight(Vector3 _TargetPosition, float _MaxRange)
    {
        float PercentDistance = Vector3.Distance(_TargetPosition, transform.position) / _MaxRange;

        //Start off with the arc being at player height
        float CalculatedArkHeight = transform.position.y;

        //if the
        if (_TargetPosition.y > CalculatedArkHeight)
        {
            CalculatedArkHeight = _TargetPosition.y;
        }

        ArkHeight = Mathf.Clamp((CalculatedArkHeight) + (ArkHeightDistanceFactor * PercentDistance) - (transform.position.y - 1), MinArkHeight, CalculatedArkHeight + ArkHeightDistanceFactor);
        ArkHeight += BaseArcHeightIncrease;
    }

    private void FixedUpdate()
    {
        if (GravityActive)
        {
            GetComponent<Rigidbody>().AddForce(new Vector3(0, AppliedGravity, 0));
        }
    }

    LaunchData CalculateLaunchData(Vector3 _targetPosition, Vector3 _flaskPosition)
    {
        float displacementY = _targetPosition.y - _flaskPosition.y;

        Vector3 displacementXZ = new Vector3(_targetPosition.x - _flaskPosition.x, 0, _targetPosition.z - _flaskPosition.z);
        float time = Mathf.Sqrt(-2 * ArkHeight / gravity) + Mathf.Sqrt(2 * (displacementY + -ArkHeight) / gravity);
        Vector3 velocityY = Vector3.up * Mathf.Sqrt(-2 * gravity * ArkHeight);
        Vector3 velocityXZ = displacementXZ / time;

        return new LaunchData(velocityXZ + velocityY * -Mathf.Sign(gravity), time);
    }

    struct LaunchData
    {
        public readonly Vector3 initialVelocity;
        public readonly float timeToTarget;

        public LaunchData(Vector3 initialVelocity, float timeToTarget)
        {
            this.initialVelocity = initialVelocity;
            this.timeToTarget = timeToTarget;
        }

    }

    public void Activate()
    {

        GameObject explosionEffect = GameObject.Instantiate(ExplosionEffect, transform.position, transform.rotation);
        explosionEffect.transform.localScale = Vector3.one *  DamageRadius * ParticleSizeModifier;

        Collider[] collidersInRange = Physics.OverlapSphere(this.transform.position, DamageRadius);

        foreach (Collider colliderInRange in collidersInRange)
        {
            if (colliderInRange.GetComponent<Scr_ChemicalEffect_Sap>())
            {
                //colliderInRange.GetComponent<Scr_ChemicalEffect_Sap>().HitByFlask();
                break;
            }
        }

        foreach (Collider colliderInRange in collidersInRange)
        {
            if (colliderInRange.tag == "Player")
            {
                colliderInRange.GetComponent<Scr_Character_Health>().TakeDamage(ProjectileDamage);
            }
        }

        if(AimReticle != null)
        {
            Destroy(AimReticle.gameObject);
        }

        Destroy(this.gameObject);
    }

    private void OnTriggerEnter(Collider collision)
    {

        //Debug.Log("Flask hit: " + collision.gameObject.name);

        if (collision.gameObject.layer == 11)
        {
            //Debug.Log(collision.gameObject);
            //By setting an active flag, if it hits 2 object simultaniously only 1 of the activations works
            if (Active)
            {
                Activate();
                Active = false;
            }

            //Debug.Log("hit Something");
            //Destroy(this.gameObject);
        }

    }
}
