﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class Scr_FollowCamera : MonoBehaviour
{
    public static Scr_FollowCamera inst;

    //The external reference to the camera itself
    public GameObject PlayerCamera;

    //The target the camera will follow
    public Transform TargetTransform;

    //Used to track the initial offset of the camera
    public Vector3 TargetOffset;

    //Used to track if the player is dragging the camera
    public bool DraggingCamera = false;

    //Use to track if left or right click is being used ot drag, for character movement
    public bool RightClickDrag = false;
    //public bool LeftClickDrag = false;

    //The difference in mouse position through delta time
    public Vector3 MouseDragDelta;

    //Use to compare mouse positoin differences
    public Vector3 PreviousMousePosition;

    //Camera rotation sensitivity
    public float CameraLookSensitivity;

    public float CameraZoomSensitivity;

    public Transform VerticalAnchor;

    public int InvertY = 1;

    bool ResetCamera = false;

    public float MouseMovementDelta;

    public float PlayerZoomDistance;
    public float ClampedZoomDistance;

    public float MinZoomDistance;
    public float MaxZoomDistance;

    public float CameraColliderBuffer;

    public Vector3 TargetPosition;
    public float CameraLerpSpeed;

    public bool OnCollider;

    public Quaternion StartingHoritzonalRotation;
    public Quaternion StartingVerticalRotation;
    public Quaternion StartingVerticalLocalRotation;

    public LayerMask CameraColliderLayerMask;

    public Collider cameraCollider;

    public float CameraColliderRadius;

    public float MinimumAngle;
    public float MaximumAngle;

    public bool TurnWithKeyboard;
    public float KeyboardTurnSensitivity;

    public bool LeftClickDrag;

    public bool ActiveDrag;

    [StructLayout(LayoutKind.Sequential)]
    public struct Point
    {
        public int X;
        public int Y;
        public static implicit operator Vector2(Point p)
        {
            return new Vector2(p.X, p.Y);
        }
    }

    public Point SavedMousePosition;
    

    private void Awake()
    {
        inst = this;
        StartingHoritzonalRotation = this.transform.rotation;
        StartingVerticalRotation = VerticalAnchor.rotation;
        StartingVerticalLocalRotation = VerticalAnchor.localRotation;
    }

    // Use this for initialization
    void Start ()
    {
        //Snapshots the distance between the camera and target object
        TargetOffset = transform.position - TargetTransform.position;
        //nparents the camera if it is parented to anything
        this.transform.parent = null;

        PreviousMousePosition = Input.mousePosition;


    }
	
	// Update is called once per frame
	void Update ()
    {
        MouseMovementDelta = (Input.mousePosition - PreviousMousePosition).magnitude;

        UpdateResettingCamera();

        if (Scr_GameDirector.inst.gameState == Scr_GameDirector.GameState.InGame)
        {
            UpdateInput();
            //UpdatePosition();
            UpdateRotation();
        }
    }

    void LateUpdate()
    {
        if (Scr_GameDirector.inst.gameState == Scr_GameDirector.GameState.InGame)
        {
            //    UpdateInput();
            //  Position needs to be updated last, so the player has enough time to update their position for the camera to get an accurate reading at the end of frame
            UpdatePosition();
            //UpdateCollisionPosition();
            UpdateSphereCastPosition();
            //    UpdateRotation();
        }

    PreviousMousePosition = Input.mousePosition;

    }

    void UpdateResettingCamera()
    {
        if (ResetCamera == true)
        {
            ResetMousePosition();
            //Keep trying to reset the mouse until it is reset
            Point currentMousePos;
            GetCursorPos(out currentMousePos);
            if (currentMousePos.X == SavedMousePosition.X && currentMousePos.Y == SavedMousePosition.Y)
            {
                ResetCamera = false;
                Cursor.visible = true;
            }
        }
    }

    public void InstantResetCamera()
    {
        ResetMousePosition();
        //Keep trying to reset the mouse until it is reset
        Point currentMousePos;
        GetCursorPos(out currentMousePos);
        if (currentMousePos.X == SavedMousePosition.X && currentMousePos.Y == SavedMousePosition.Y)
        {
            ResetCamera = false;
            Cursor.visible = true;
        }
    }

    void UpdateInput()
    {
        //if (Input.GetKeyDown(KeyCode.Backspace))
        //{
        //    ResetMousePosition();
        //}

        //Checing what is down

        if(TurnWithKeyboard)
        {
            MouseDragDelta.x = (Input.GetAxis("Horizontal") * KeyboardTurnSensitivity);
            MouseDragDelta.y = 0;
        }
        else if(ActiveDrag)
        {
            if(Scr_Character_Components.inst.character_Throw.PlayerThrowState == Scr_Character_Throw.ThrowState.Neutral)
            {
                if(DraggingCamera == false)
                {
                    RightClickDrag = true;
                    StartDrag();
                }
            }
            else
            {
                if(DraggingCamera == true)
                {
                    RightClickDrag = false;
                    StopDrag();
                }
            }

            MouseDragDelta.x = (Input.GetAxis("Mouse X"));
            MouseDragDelta.y = (Input.GetAxis("Mouse Y"));
        }
        else
        {
            if (Input.GetMouseButton(1) && Scr_Character_Components.inst.character_Throw.PlayerThrowState == Scr_Character_Throw.ThrowState.Neutral)
            {
                if (DraggingCamera == false)
                {
                    //Debug.Log("Dragging Camera");
                    RightClickDrag = true;
                    StartDrag();
                }
            }
            //Left click dragging
            else if (Input.GetMouseButton(0) && Scr_Character_Components.inst.character_Throw.PlayerThrowState == Scr_Character_Throw.ThrowState.Neutral)
            {
                if (LeftClickDrag)
                {
                    if (DraggingCamera == false)
                    {
                        //Debug.Log("Dragging Camera");
                        RightClickDrag = false;
                        StartDrag();
                    }
                }
                
            }

            //Checkign what isnt down
            if (Input.GetMouseButton(1) == false && Input.GetMouseButton(0) == false)
            {
                if (DraggingCamera == true)
                {
                    RightClickDrag = false;
                    StopDrag();
                }
            }
            else if (Input.GetMouseButton(1) == false)
            {
                if (DraggingCamera == true)
                {
                    RightClickDrag = false;
                }
            }
            else if (Input.GetMouseButton(0) == false)
            {
                if (LeftClickDrag)
                {
                    if (DraggingCamera == true)
                    {
                        RightClickDrag = true;
                    }
                }
                
            }

            //Janky override to check if both are down
            if (Input.GetMouseButton(1) && Input.GetMouseButton(0))
            {
                RightClickDrag = true;
            }





            //MouseDragDelta = (Input.mousePosition - PreviousMousePosition) * Time.deltaTime;
            MouseDragDelta.x = (Input.GetAxis("Mouse X"));
            MouseDragDelta.y = (Input.GetAxis("Mouse Y"));
            //Input.mous
        }



    }

    [DllImport("user32.dll")]
    static extern bool SetCursorPos(int X, int Y);

    [DllImport("user32.dll")]
    static extern bool GetCursorPos(out Point pos);


    public void StartDrag()
    {
        DraggingCamera = true;
        //Lock the cursor
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        if(!ActiveDrag)
        {
            SaveMousePosition();
        }
        
        //Debug.Log(SavedMousePosition.X);
        //Debug.Log(SavedMousePosition.Y);
    }

    public void StopDrag()
    {
        DraggingCamera = false;
        //Lock the cursor
        Cursor.lockState = CursorLockMode.None;
        //ResetMousePosition();

        if (!ActiveDrag)
        {
            ResetCamera = true;
        }
        
    }


    void SaveMousePosition()
    {
        GetCursorPos(out SavedMousePosition);
    }

    public void ResetMousePosition()
    {
        SetCursorPos(SavedMousePosition.X, SavedMousePosition.Y);
    }

    void UpdatePosition()
    {
        //Retains the difference in position to the target object
        //transform.position = Vector3.LerpUnclamped(transform.position, TargetTransform.position +  TargetOffset, 6 * Time.deltaTime);
        transform.position = TargetTransform.position + TargetOffset;

        PlayerZoomDistance = Mathf.Clamp(PlayerZoomDistance + (CameraZoomSensitivity * Input.mouseScrollDelta.y), -MaxZoomDistance, -MinZoomDistance);
        //Zoom in and out based on the scroll
        if (Input.mouseScrollDelta.y != 0)
        {

            
            //Get the vector between the camera and anchor
            //Vector3 Direction = (this.transform.position - PlayerCamera.transform.position).normalized;
            //PlayerCamera.transform.localPosition += (Direction * CameraZoomSensitivity);

            

            //PlayerCamera.transform.position += (this.transform.position - PlayerCamera.transform.position).normalized * CameraZoomSensitivity * Input.mouseScrollDelta.y;

            //Debug.Log(Input.mouseScrollDelta);
        }
    }

    void UpdateRotation()
    {
        if (DraggingCamera || (TurnWithKeyboard))
        {
            //Rotate the camera
            this.gameObject.transform.Rotate(0, MouseDragDelta.x * CameraLookSensitivity, 0);


            float VerticleRotationDelta = Mathf.Clamp( MouseDragDelta.y * CameraLookSensitivity * InvertY, -10,10);
            float CurrentRotation = VerticalAnchor.localRotation.eulerAngles.x;
            float PredictedRotation = VerticalAnchor.localRotation.eulerAngles.x + VerticleRotationDelta;
            if(PredictedRotation < 0)
            {
                PredictedRotation = 360 + PredictedRotation;
            }
            //Debug.Log("Rotation Delta: " + VerticleRotationDelta);
            //Debug.Log("Predicted Rotation: " + PredictedRotation);
            //Debug.Log("Current Local Euler Angles: " + VerticalAnchor.transform.localEulerAngles);
            //Debug.Log("Quaternion Euler: " + VerticalAnchor.transform.localRotation.eulerAngles);
            //Debug.Log(MouseDragDelta);
            
            //TODO still can lock in middle randomly and not elegent solution

            //Middle Transition
            if((CurrentRotation >= 0 && CurrentRotation < 60) || (CurrentRotation <= 360 && CurrentRotation > 300) || (CurrentRotation == 0))
            {
                //Debug.Log("In Transition");
                //Move regardless
                VerticalAnchor.Rotate(VerticleRotationDelta, 0, 0);
            }
            else
            {
                //Lower quadrent clamping
                if (CurrentRotation <= 360 && CurrentRotation >= 270)
                {
                    if(VerticleRotationDelta > 0)
                    {
                        VerticalAnchor.Rotate(VerticleRotationDelta, 0, 0);
                    }
                    else
                    {
                        //If your predicted rotation is within the bounds
                        //Debug.Log("Lower");
                        if (PredictedRotation > 360 + MinimumAngle)
                        {
                            VerticalAnchor.Rotate(VerticleRotationDelta, 0, 0);
                        }
                        //Otherwise deermine how much left there is to rotate and rotate that much
                        else
                        {
                            //VerticalAnchor.Rotate((360 + MinimumAngle) - CurrentRotation, 0, 0);
                        }
                    }
                    
                }
                //Upper quadrent
                else if (CurrentRotation >= 0 && CurrentRotation <= 90)
                {
                    if (VerticleRotationDelta < 0)
                    {
                        VerticalAnchor.Rotate(VerticleRotationDelta, 0, 0);
                    }
                    else
                    {
                        //If your predicted rotation is within the bounds
                        //Debug.Log("Upper");
                        if (PredictedRotation < MaximumAngle)
                        {
                            VerticalAnchor.Rotate(VerticleRotationDelta, 0, 0);
                        }
                        //Otherwise deermine how much left there is to rotate and rotate that much
                        else
                        {
                            //VerticalAnchor.Rotate(MaximumAngle - CurrentRotation, 0, 0);
                        }
                    }
                    
                }
            }
            
            //If you ever get caught on 0 then snap out of it
            if(VerticalAnchor.localRotation.x == 0)
            {
                Debug.Log("Unhinging");
                VerticalAnchor.Rotate(0.1f, 0, 0);
            }

            //VerticalAnchor.Rotate(VerticleRotationDelta, 0, 0);

            //Debug.Log(VerticalAnchor.transform.localRotation.eulerAngles.x);

            //Clampign Rotation
            //if (VerticalAnchor.transform.localRotation.eulerAngles.x >= MaximumAngle)
            //{
            //Debug.Log("Over Rotated");
            //VerticalAnchor.transform.localRotation = Quaternion.Euler(MaximumAngle, VerticalAnchor.transform.localEulerAngles.y, VerticalAnchor.transform.localEulerAngles.z);
            //}



            //Debug.Log("1# Camera Rotation: " + this.transform.rotation.eulerAngles);
            //Debug.Log("2# Character ROtation: " + Scr_Character_Components.inst.character_Movement.transform.rotation.eulerAngles);
        }
    }

    void UpdateSphereCastPosition()
    {
        Vector3 direction = (PlayerCamera.transform.position - transform.position).normalized;
        Ray ray = new Ray(transform.position, direction);
        RaycastHit hit = new RaycastHit();
        if (Physics.SphereCast(ray, CameraColliderRadius, out hit, 9999, CameraColliderLayerMask))
        {
            if (hit.collider.gameObject.layer == 11 || hit.collider.gameObject.layer == 14)
            {
                //PlayerCamera.transform.position = hit.point;
                //Debug.Log(hit.collider.gameObject.name);

                ClampedZoomDistance = hit.distance - CameraColliderBuffer;

                float CurrentDistance = (PlayerCamera.transform.position - transform.position).magnitude;

                //if the clamped distcne it shorter than the current zoom
                if (Mathf.Abs(ClampedZoomDistance) < Mathf.Abs(PlayerZoomDistance))
                {
                    //Set the camera instantly here so you dont lerp through the walls
                    //PlayerCamera.transform.position = this.transform.position - (PlayerCamera.transform.forward * ClampedZoomDistance);
                    TargetPosition = this.transform.position - (PlayerCamera.transform.forward * ClampedZoomDistance);
                    OnCollider = true;
                }
                else
                {
                    TargetPosition = this.transform.position + (PlayerCamera.transform.forward * PlayerZoomDistance);
                    OnCollider = false;
                }
            }
            else
            {
                TargetPosition = this.transform.position + (PlayerCamera.transform.forward * PlayerZoomDistance);
                OnCollider = false;
            }
        }
        else
        {
            TargetPosition = this.transform.position + (PlayerCamera.transform.forward * PlayerZoomDistance);
            OnCollider = false;
        }

        PlayerCamera.transform.position = Vector3.Lerp(PlayerCamera.transform.position, TargetPosition, CameraLerpSpeed * Time.deltaTime);


        Debug.DrawLine(ray.origin, ray.origin + direction * 10, Color.red);
    }

    public void UpdateTurnWithKeyboard(bool _State)
    {
        TurnWithKeyboard = _State;
        VerticalAnchor.transform.localRotation = StartingVerticalLocalRotation;
    }

    public void SetYInvert(bool _YInverted)
    {
        if (_YInverted)
        {
            InvertY = 1;
        }
        else
        {
            InvertY = -1;
        }
    }

    void UpdateCollisionPosition()
    {
        //Dray a ray from the camera to the character, if there is anything in the way, adjust the zoom until there isnt
        //public Vector3 direction = Player
        Vector3 direction = (PlayerCamera.transform.position - transform.position).normalized;
        Ray ray = new Ray(transform.position, direction);
        RaycastHit hit = new RaycastHit();
        if(Physics.Raycast(ray, out hit, CameraColliderLayerMask))
        {
            if (hit.collider.gameObject.layer == 11 || hit.collider.gameObject.layer == 14)
            {
                //PlayerCamera.transform.position = hit.point;
                Debug.Log(hit.collider.gameObject.name);

                ClampedZoomDistance = (hit.point - this.transform.position).magnitude - CameraColliderBuffer;

                float CurrentDistance = (PlayerCamera.transform.position - transform.position).magnitude;

                //if the clamped distcne it shorter than the current zoom
                if (Mathf.Abs(ClampedZoomDistance) < Mathf.Abs(PlayerZoomDistance))
                {
                    //Set the camera instantly here so you dont lerp through the walls
                    //PlayerCamera.transform.position = this.transform.position - (PlayerCamera.transform.forward * ClampedZoomDistance);
                    TargetPosition = this.transform.position - (PlayerCamera.transform.forward * ClampedZoomDistance);
                    OnCollider = true;
                }
                else
                {
                    TargetPosition = this.transform.position + (PlayerCamera.transform.forward * PlayerZoomDistance);
                    OnCollider = false;
                }
            }
            else
            {
                TargetPosition = this.transform.position + (PlayerCamera.transform.forward * PlayerZoomDistance);
                OnCollider = false;
            }
        }
        else
        {
            TargetPosition = this.transform.position + (PlayerCamera.transform.forward * PlayerZoomDistance);
            OnCollider = false;
        }

        PlayerCamera.transform.position = Vector3.Lerp(PlayerCamera.transform.position, TargetPosition, CameraLerpSpeed * Time.deltaTime);


        Debug.DrawLine(ray.origin, ray.origin + direction * 10, Color.red);
    }

    public void ResetCameraRotation()
    {
        this.transform.rotation = StartingHoritzonalRotation;
        //VerticalAnchor.rotation = StartingVerticalRotation;
        VerticalAnchor.localRotation = StartingVerticalLocalRotation;
    }
}
