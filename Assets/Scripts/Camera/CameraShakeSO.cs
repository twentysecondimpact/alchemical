﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class CameraShakeSO : ScriptableObject 
{

    public float Magnitude;
    public float Roughness;

    public float FadeInTime;
    public float FadeOutTime;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Shakeonce(float _MagMultiplier, float _RoughMultiplier, float _FadeInMultiplier, float _FadeOutMultiplier)
    {
        //Screen shake
        EZCameraShake.CameraShaker.Instance.ShakeOnce(Magnitude * _MagMultiplier, Roughness * _RoughMultiplier, FadeInTime * _FadeInMultiplier, FadeOutTime * _FadeOutMultiplier);
    }
}
