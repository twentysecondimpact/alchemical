﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_LevelManager : MonoBehaviour
{
    public string OpeningLevelScene;

    public Transform PlayerSpawn;

    public GameObject[] PlayerFlaskPrefab;
    public GameObject PlayerObjects;

    public int CheckPointObjective;
    public int CheckPointCrystals;

    // Use this for initialization
    void Start ()
    {
        Scr_GameDirector.inst.currentLevel = this;

        SetUpLevel();

        //CLoses any menus uised to get to the level
        Scr_GameDirector.inst.menuManager.UnloadMenu();

        //Change the game state to have started
        Scr_GameDirector.inst.gameState = Scr_GameDirector.GameState.InGame;

	}
	
	// Update is called once per frame
	void Update ()
    {
	}

    public void SetUpLevel()
    {
        Scr_Character_Components character = Scr_Character_Components.inst;

        //Debug.Log("Resetting player positions and shit");
        //Reset the character position
        PlayerObjects.transform.position = PlayerSpawn.position;
        //Reset the character rotation
        PlayerObjects.transform.rotation = PlayerSpawn.rotation;

        PlayerObjects.GetComponent<Scr_PlayerObjects>().ResetCharacterObjects();

        //Reset character model rotation
        character.character_Movement.MouseRotationAngle = character.transform.rotation.eulerAngles;
        character.character_Movement.CharacterModelTransformMouse.rotation = character.transform.rotation;
        character.character_Movement.CharacterModelTransformKeyboard.localRotation = Quaternion.identity;
        character.character_Movement.KeyboardRotationAngle = 0;
        character.character_Movement.TargetRotation = PlayerSpawn.rotation;

        //Reset the camera rotation
        Scr_FollowCamera.inst.ResetCameraRotation();
        //Sets the camera rotation to face the sam eway as the character now, its all veyr confuising
        Scr_FollowCamera.inst.transform.rotation = PlayerSpawn.rotation;

        //Reset the current collected crystals,
        Scr_GameDirector.inst.CrystalsCollected = CheckPointCrystals;

        //Reset the objective marker
        //Scr_ObjectiveTracker.inst.CurrentObjective = CheckPointObjective;

        //Scr_ObjectiveTracker.inst.UpdateObjectText(0);


        //Only reset flasks if its a proper checkpoint
        if(Scr_GameDirector.inst.CurrentCheckpointLevel != "")
        {
            //Removes all the flasks
            for (int i = 0; i < Scr_Character_Components.inst.character_Throw.FlaskPrefab.Length; i++)
            {
                Scr_Character_Components.inst.character_Throw.FlaskPrefab[i] = null;
            }

            //Unlocks all the flasks for the level
            for (int i = 0; i < PlayerFlaskPrefab.Length; i++)
            {
                if (PlayerFlaskPrefab[i] != null)
                {
                    Scr_Character_Components.inst.character_Throw.UnlockFlask(i, PlayerFlaskPrefab[i]);
                }
            }

            //Reset flask cooldowns
            Scr_Character_Components.inst.character_Throw.ResetCooldowns();

            //Reset cooldown bar visuals
            for (int i = 0; i < Scr_CooldownBar.inst.cooldowns.Count; i++)
            {
                Scr_CooldownBar.inst.cooldowns[i].Progress.fillAmount = 0;
            }
        }


        //Remove all objects made by the player
        foreach (GameObject playerMadeObject in GameObject.FindGameObjectsWithTag("PlayerMade"))
        {
            Destroy(playerMadeObject);
        }


        //Refresh HP visual 
        //if(Scr_HealthDisplay.inst.)


        Scr_HealthDisplay.inst.UpdateHealth();
        
        
        //Scr_UI_HealthIndicator.inst.UpdateHealth();

        Scr_GameDirector.inst.fader.Activate(-1);
    }
}
