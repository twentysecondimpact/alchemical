﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Linq;
using UnityEngine.UI;
using TMPro;

public class Scr_MainMenuController : MonoBehaviour
{

    public Texture2D MouseUpCursor;
    public Texture2D MouseDownCursor;

    public GameObject CreditsMenu;
    public GameObject HighScores;

    public GameObject HighScoreEntry;
    public GameObject HighScoreList;

    public bool populatedScoreTable;

    public int AScore;
    public int BScore;
    public int CScore;
    public int DScore;
    public int EScore;

    public Scr_UI_Fader fader;
    public float FadeInDelay;

    public AudioEffectSO AESO_ButtonClick;

	// Use this for initialization
	void Start () {

        SetScoresToPrefs();
        //Set the cursor image
        Cursor.SetCursor(MouseUpCursor, new Vector2(0, 0), CursorMode.Auto);

        StartCoroutine(DelayFadeIn());
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0))
        {
            //Debug.Log("Change cursor to down");
            //Set the cursor image
            Cursor.SetCursor(MouseDownCursor, new Vector2(0, 0), CursorMode.Auto);
        }

        if (Input.GetMouseButtonUp(0))
        {
            //Debug.Log("Change cursor to up");
            //Set the cursor image
            Cursor.SetCursor(MouseUpCursor, new Vector2(0, 0), CursorMode.Auto);
        }
    }

    public void SetScoresToPrefs()
    {
        PlayerPrefs.SetInt("AScore", AScore);
        PlayerPrefs.SetInt("BScore", BScore);
        PlayerPrefs.SetInt("CScore", CScore);
        PlayerPrefs.SetInt("DScore", DScore);
        PlayerPrefs.SetInt("EScore", EScore);
    }

    public void StartGame()
    {
        //if (!fader.Active)
        //{
            StartCoroutine(FadeOutStartGame());
        //}
    }

    public void ActivateCreditsMenu()
    {
        if (CreditsMenu.activeSelf)
        {
            CreditsMenu.SetActive(false);
        }
        else
        {
            CreditsMenu.SetActive(true);
        }
        
        HighScores.SetActive(false);
    }

    public void ResetHighScores()
    {
        if (PlayerPrefs.HasKey("Scores"))
        {
            for (int i = 0; i < PlayerPrefs.GetInt("Scores"); i++)
            {
                PlayerPrefs.DeleteKey("ScoreName_" + i.ToString());
                PlayerPrefs.DeleteKey("ScoreNumber_" + i.ToString());
            }
            PlayerPrefs.SetInt("Scores", 0);

            PlayerPrefs.Save();
        }

        //Destroy all the entries
        if(HighScoreList.transform.childCount > 0)
        {
            for (int i = 0; i < HighScoreList.transform.childCount; i++)
            {
                Destroy(HighScoreList.transform.GetChild(i).gameObject);
            }
        }

        populatedScoreTable = false;


    }

    public void ActivateHighScores()
    {
        //for (int i = 0; i < HighScoreList.transform.childCount; i++)
        //{
        //    Destroy(HighScoreList.transform.GetChild(i).gameObject);
        //}

        if(HighScores.activeSelf)
        {
            HighScores.SetActive(false);
        }
        else
        {
            HighScores.SetActive(true);
        }
        
        CreditsMenu.SetActive(false);

        if (!populatedScoreTable)
        {
            List<PlayerScore> PlayerScores = new List<PlayerScore>();

            if (PlayerPrefs.HasKey("Scores"))
            {
                for (int i = 0; i < PlayerPrefs.GetInt("Scores"); i++)
                {
                    //Populates the list
                    PlayerScores.Add(new PlayerScore(PlayerPrefs.GetString("ScoreName_" + i.ToString()), PlayerPrefs.GetInt("ScoreNumber_" + i.ToString())));
                }

                PlayerScores = PlayerScores.OrderBy(w => w.Score).ToList();
                PlayerScores.Reverse();

                for (int i = 0; i < PlayerScores.Count; i++)
                {
                    if(i == 10)
                    {
                        break;
                    }

                    GameObject scoreEntry = GameObject.Instantiate(HighScoreEntry, HighScoreList.transform);
                    scoreEntry.transform.GetChild(0).gameObject.GetComponent<TMP_Text>().text = PlayerScores[i].Name;
                    scoreEntry.transform.GetChild(1).gameObject.GetComponent<TMP_Text>().text = PlayerScores[i].Score.ToString();
                    scoreEntry.transform.GetChild(3).gameObject.GetComponent<TMP_Text>().text = ScoreGrade(PlayerScores[i].Score);
                }
            }

            populatedScoreTable = true;
        }

        

        
        
    }

    public struct PlayerScore
    {
        public string Name;
        public int Score;

        //IComparer<int> comparer;

        public PlayerScore(string _Name, int _Score)
        {
            Name = _Name;
            Score = _Score;
        }

        
    }

    public int[] OrderedScoreIndex()
    {
        return null;
    }

    public static string ScoreGrade(int _Score)
    {
        Debug.Log(_Score);
        if(_Score >= PlayerPrefs.GetInt("AScore"))
        {
            Debug.Log("Better or equal to A Score");
            return "A";
        }
        else if (_Score >= PlayerPrefs.GetInt("BScore"))
        {
            return "B";
        }
        else if (_Score >= PlayerPrefs.GetInt("CScore"))
        {
            return "C";
        }
        else if (_Score >= PlayerPrefs.GetInt("DScore"))
        {
            return "D";
        }
        else 
        {
            return "E";
        }
    }

    public void PlayButtonClick()
    {
        AESO_ButtonClick.Play();
    }

    IEnumerator DelayFadeIn()
    {
        //Initial delay after the object loads in
        for (float i = 0; i < FadeInDelay; i += Time.deltaTime)
        {
            yield return null;
        }


        fader.Activate(-1);
    }

    IEnumerator FadeOutStartGame()
    {
        //Set the fader to fade out
        fader.Activate(1);

        for (int i = 0; i < 10000; i++)
        {
            //Wait till the fader has hit 1
            if(fader.Active == false)
            {
                break;
            }
            yield return null;
        }

        //elay after full fade
        for (float i = 0; i < 0.2f; i += Time.deltaTime)
        {
            yield return null;
        }
        //Load the game
        SceneManager.LoadScene("GameplayScene");
    }
}
