﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_AnimEvents_Character : MonoBehaviour
{

    public Scr_Character_AudioController audioController;

    public ParticleSystem GrassFootSteps_Particles;
    public ParticleSystem StoneFootSteps_Particles;

    public ParticleSystem GrassFootSlide_Particles;
    public ParticleSystem StoneFootSlide_Particles;


    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        CheckSlidingParticles();
	}
    #region Howlend Events

    void CheckSlidingParticles()
    {
        if(Scr_Character_Components.inst.character_Throw.PlayerThrowState == Scr_Character_Throw.ThrowState.Neutral)
        {
            //If on grass
            if (Scr_Character_Components.inst.character_AnimEvents.GrassFootSlide_Particles.isPlaying)
            {
                //Debug.Log("Disabling grass slide");
                Scr_Character_Components.inst.character_AnimEvents.GrassFootSlide_Particles.Stop();
            }

            //If on stone
            if (Scr_Character_Components.inst.character_AnimEvents.StoneFootSlide_Particles.isPlaying)
            {
                Scr_Character_Components.inst.character_AnimEvents.StoneFootSlide_Particles.Stop();
            }
        }
    }

    void FootStep()
    {
        audioController.PlayHowlendFootStep();

        if(Scr_Character_Components.inst.character_Movement.currentGroundType == Scr_Character_Movement.GroundType.Grass)
        {
            //Plays the grass steps
            GrassFootSteps_Particles.Play();
        }
        else
        {
            StoneFootSteps_Particles.Play();
        }
        
    }

    void ThrowFlask()
    {
        
    }

    void Jump()
    {

    }

    void Land()
    {

    }

    void Slide()
    {

    }

    #endregion
}
