﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_AnimEvents_Enemy : MonoBehaviour
{
    public AudioSource audioSource;

    public AudioEffectSO AESO_WalkFootStep;
    public AudioEffectSO AESO_RunFootStep;

    public AudioEffectSO AESO_PopOut;


    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        transform.parent.GetComponent<Scr_Enemy>().audioSource = audioSource;
    }

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    void PlayWalkFootStep()
    {
        AESO_WalkFootStep.Play(audioSource, false);
    }

    void PlayRunFootStep()
    {
        AESO_RunFootStep.Play(audioSource, false);
    }

    void PlayPopOut()
    {
        AESO_PopOut.Play(audioSource, false);
    }
}
