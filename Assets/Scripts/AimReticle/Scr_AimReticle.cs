﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_AimReticle : MonoBehaviour
{

    public GameObject ReticleReady;
    public GameObject ReticleUltimate;

    public float FadePercent;
    public float FadeSpeed;

    public AnimationCurve FadeInCurve;
    public AnimationCurve FadeOutCurve;

    public float UltiFadePercent;
    public float UltiFadeInSpeed;
    public float UltiFadeOutSpeed;

    public AnimationCurve UltiFadeInCurve;
    public AnimationCurve UltiFadeOutCurve;

    bool Ready = false;

    bool UltimateActive;

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        //if(Scr_Character_Components.inst.character_Throw.PlayerThrowState == Scr_Character_Throw.ThrowState.Charging)
        //{
        //    if (Scr_Character_Components.inst.character_Throw.castState == Scr_Character_Throw.CastState.Ready)
        //    {
        //        EnableReady();
        //    }
        //    else if (Scr_Character_Components.inst.character_Throw.castState == Scr_Character_Throw.CastState.NotReady)
        //    {
        //        EnableNotReady();
        //    }

        //    //HighlightTargets();
        //}
        //else
        //{
        //    DisableReticle();
        //}

        UpdateFadePercent();

    }

    public void EnableReady()
    {
        //Debug.Log("enable");
        //FadePercent = 0;
        ReticleReady.GetComponent<MeshRenderer>().enabled = true;
        //ReticleUltimate.transform.GetChild(0).gameObject.SetActive(false);
        //ReticleNotReady.GetComponent<MeshRenderer>().enabled = false;
        UltimateActive = false;
    }

    public void EnableUltimate()
    {
        //ReticleUltimate.transform.GetChild(0).gameObject.SetActive(true);
        ReticleReady.GetComponent<MeshRenderer>().enabled = false;
        UltimateActive = true;
    }

    public void EnableNotReady()
    {
        //Debug.Log("disable");
        //FadePercent = 0;
        //ReticleNotReady.GetComponent<MeshRenderer>().enabled = true;
        ReticleReady.GetComponent<MeshRenderer>().enabled = false;
    }

    public void DisableReticle()
    {
        ReticleReady.GetComponent<MeshRenderer>().enabled = false;
        //ReticleUltimate.transform.GetChild(0).gameObject.SetActive(false);
        UltimateActive = false;
    }

    public void HighlightTargets()
    {
        Vector3 center = transform.position;
        float radius = GetComponent<SphereCollider>().radius * transform.localScale.x;

        //Debug.Log("Scannign for enemies");

        Collider[] allOverlappingColliders = Physics.OverlapSphere(center, radius);
        //Debug.Log(radius);
        

        for (int i = 0; i < allOverlappingColliders.Length; i++)
        {
            if(allOverlappingColliders[i].gameObject.tag == "Enemy")
            {
                //Trigger a highlight shader thingo
                //allOverlappingColliders[i].gameObject.GetComponent<Scr_Enemy_AggroDisplay>().triggered = true;
                //allOverlappingColliders[i].transform.GetChild(0).GetComponent<MeshRenderer>().materials[1].shader = Shader.Find("Self-Illumin/Outlined Diffuse");
                //allOverlappingColliders[i].transform.GetChild(0).GetComponent<MeshRenderer>().materials[1].color = Color.white;
            }
        }
    }

    public void UpdateFadePercent()
    {
        if (Scr_Character_Components.inst.character_Throw.PlayerThrowState == Scr_Character_Throw.ThrowState.Charging)
        {
            //Debug.Log("Update Color Fade");
            FadePercent = Mathf.Clamp01(FadePercent + (Time.deltaTime * FadeSpeed));

            if (UltimateActive)
            {
                UltiFadePercent = Mathf.Clamp01(UltiFadePercent + (Time.deltaTime * UltiFadeInSpeed));
            }
            
        }
        else
        {
            //Debug.Log("Update Color Fade");
            FadePercent = Mathf.Clamp01(FadePercent - (Time.deltaTime * FadeSpeed));

            UltiFadePercent = Mathf.Clamp01(UltiFadePercent - (Time.deltaTime * UltiFadeOutSpeed));
        }



        Vector3 GrabbedPosition = ReticleUltimate.transform.GetChild(0).localPosition;

        if (UltimateActive)
        {
            GrabbedPosition.y = UltiFadeInCurve.Evaluate(UltiFadePercent) * 0.25f;
        }
        else
        {
            GrabbedPosition.y = UltiFadeOutCurve.Evaluate(UltiFadePercent) * 0.25f;
        }
        


        ReticleUltimate.transform.GetChild(0).localPosition = GrabbedPosition;

        Color GrabbedColor = ReticleUltimate.transform.GetChild(0).GetComponent<MeshRenderer>().material.color;
        if (UltimateActive)
        {
            GrabbedColor.a = UltiFadeInCurve.Evaluate(UltiFadePercent) * 0.25f;
        }
        else
        {
            GrabbedColor.a = UltiFadeOutCurve.Evaluate(UltiFadePercent) * 0.25f;
        }
        
        ReticleUltimate.transform.GetChild(0).GetComponent<MeshRenderer>().material.color = GrabbedColor;

        //GrabbedColor = Color.red;
        //GrabbedColor.a = FadePercent;

        //ReticleReady.GetComponent<MeshRenderer>().materials[1].color = GrabbedColor;
        //Debug.Log("Setting ready reticle to alpha to: " + GrabbedColor);

    }
}
